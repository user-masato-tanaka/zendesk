﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;
using Zendesk.Common;
using Zendesk.Internal.Models.Common;
using Zendesk.Internal.Models.Core;
using Zendesk.Internal.Models.HelpCenter;
using Zendesk.Internal.Models.Support;
#if UNITY_EDITOR
using UnityEditor;

#endif

namespace Zendesk.UI
{
    public class ZendeskSupportUI : MonoBehaviour
    {
        #region Public Properties

        public GameObject attachmentPrefab;
        public GameObject ticketResponseAttachmentCountContainer;
        public Text ticketResponseAttachmentCountText;
        public GameObject createRequestAttachmentCountContainer;
        public Text createRequestAttachmentCountText;
        public GameObject attachmentsView;
        public GameObject attachmentButton;
        public GameObject sendCommentButton;
        public GameObject attachmentsViewNonConversational;
        public GameObject addCommentButtonImage;
        public GameObject agentResponsePrefab;
        public GameObject answerBotResponsePrefab;
        public GameObject ticketConfirmationMessagePrefab;
        public GameObject backButtonContainer;
        public Button createRequestFooterPanelSendButton;
        public Button createRequestFooterPanelAttachmentButton;
        public GameObject createRequestPanel;
        public GameObject createRequestPanelFieldContainer;
        public GameObject createRequestPanelAttachmentLine;
        public GameObject createRequestPanelEmailValidation;
        public GameObject createRequestPanelEmailContainer;
        public GameObject createRequestPanelFeedbackToast;
        public GameObject createRequestPanelFeedbackPanel;
        public Text createRequestPanelFeedbackPanelTitle;
        public Text createRequestPanelFeedbackPanelDescription;
        public Text createRequestPanelFeedbackPanelButtonText;
        public InputField createRequestPanelInputEmail;
        public InputField createRequestPanelInputMessage;
        public Text createRequestPanelNameText;
        public Text createRequestPanelInvalidNameText;
        public Text createRequestPanelEmailText;
        public Text createRequestPanelInvalidEmailText;
        public Text createRequestPanelMessageText;
        public Text typeMessageText;
        public Text createRequestPanelInvalidMessageText;
        public InputField createRequestPanelInputName;
        public GameObject createRequestPanelMessageValidation;
        public GameObject createRequestPanelNameValidation;
        public GameObject createRequestAttachmentLine;
        public GameObject createRequestPanelNameContainer;
        public GameObject CustomTextFieldPrefab;
        public GameObject CustomTextMultilineFieldPrefab;
        public GameObject CustomCheckboxFieldPrefab;
        public GameObject CustomNumericFieldPrefab;
        public GameObject CustomDecimalFieldPrefab;
        public TextAsset customFieldTranslations;
        public TextAsset customFieldsFile;
        public GameObject endOfConversationPrefab;
        public GameObject endUserResponsePrefab;
        public GameObject footerClosedStatus;
        public GameObject footerLineMain;
        public Text headerTitle;
        public GameObject messageDatePrefab;
        public GameObject myTicketsFooter;
        public Text myTicketsFooterNeedHelpText;
        public Text myTicketsFooterContactUsButtonText;
        public Text ticketResponseFooterContactUsButtonText;
        public Text conversationalFooterNeedHelpText;
        public GameObject myTicketsPanel;
        public GameObject newCommentEndUser;
        public GameObject noTicketsFooter;
        public GameObject noTicketsPanel;
        public Text noTicketsPanelGeneralInfoText;
        public Text noTicketsPanelStartConversationText;
        public RequestForm requestForm;
        public GameObject satisfactionRatingPrefab;
        public GameObject satisfactionRatingCommentPrefab;
        public GameObject solvedConversationPrefab;
        public GameObject ticketContainerPrefab;
        internal string ticketId;
        public GameObject ticketListView;
        public List<GameObject> ticketResponseContainerList = new List<GameObject>();
        public GameObject ticketResponseListView;
        public GameObject ticketResponseScrollView;
        public GameObject ticketResponsePanel;
        public GameObject createRequestPanelMessageContainer;
        public GameObject createRequestScrollView;
        public Dictionary<long, Texture2D> ticketContentImageIds = new Dictionary<long, Texture2D>();
        public List<Coroutine> ticketConfirmationMessageCoroutines = new List<Coroutine>();
        [HideInInspector] public ZendeskAuthType zendeskAuthType = ZendeskAuthType.Anonymous;
        public GameObject keyboardBackground;
        public Color keyboardBackgroundColor;
        public GameObject headerPanel;
        public string customFieldsWarning
        {
            get
            {
                return "Please refer to the documentation before adding, or making changes to custom fields.";
            }
        }

        #endregion Public Properties
        
        #region Private Properties
        
        private readonly List<string> tags = new List<string>();
        private string authScreenErrorTitle;
        private string authScreenErrorBody;
        private string jwtIntegratorError;
        private bool isCsatEnabled = false;
        private string ticketConfirmationMessageString = "";
        private bool isConversationsEnabled = false;
        private ZendeskErrorUI zendeskErrorUI;
        private ZendeskMain zendeskMain;
        private ZendeskUI zendeskUI;
        private ZendeskLocalizationHandler zendeskLocalizationHandler;
        private List<string> attachmentPaths = new List<string>();
        private Dictionary<GameObject, FieldType> customFieldsPrefabs = new Dictionary<GameObject, FieldType>();
        private bool emailValid = true;
        private bool customFieldValid = true;
        private DateTime lastCommentDatetime;
        private bool messageValid = true;
        private bool nameValid = true;
        private DateTime prevDate = DateTime.MinValue;
        private Color redOutline;
        private Request request;
        private Color greyOutline;
        private GameObject satisfactionRatingPrefabInstatiated;
        private List<GameObject> ticketContainersList = new List<GameObject>();
        private bool isUserAuthenticatedBefore;
        private long maxAttachmentSize;
        private ZendeskLocalizationHandler zendeskCustomFieldLocalizationHandler;
        private string labelKey = "usdk_cf_{cf-id}_label";
        private string validationMessageKey = "usdk_cf_{cf-id}_validation_message";
        private List<CustomField> customFields;
        private GameObject zendeskCanvas;
        
        #endregion Private Properties

        public void InitWithErrorHandler(ZendeskMain zendeskMain, ZendeskUI zendeskUI, ZendeskErrorUI zendeskErrorUI)
        {
            this.zendeskMain = zendeskMain;
            this.zendeskUI = zendeskUI;
            this.zendeskErrorUI = zendeskErrorUI;
            this.zendeskCanvas = this.zendeskUI.zendeskCanvas;
        }

        public void Init(ZendeskMain zendeskMain, ZendeskUI zendeskUI, ZendeskSettings zendeskSettings,
            ZendeskErrorUI zendeskErrorUI,
            string commonTags = "", Dictionary<long, string> invisibleCustomFields = null)
        {
            try
            {
                this.zendeskMain = zendeskMain;
                this.zendeskLocalizationHandler = zendeskMain.GetComponent<ZendeskLocalizationHandler>();
                this.zendeskUI = zendeskUI;
                this.zendeskCanvas = this.zendeskUI.zendeskCanvas;
                this.zendeskErrorUI = zendeskErrorUI;
                SetSupportStrings();
                maxAttachmentSize = zendeskSettings.Support.Attachments.MaxAttachmentSize;
                isCsatEnabled = zendeskSettings.Support.Csat.Enabled;
                isConversationsEnabled = zendeskSettings.Support.Conversations.Enabled;
                ticketConfirmationMessageString = zendeskSettings.Support.SystemMessage;
                if (zendeskSettings.Core.Authentication == ZendeskAuthType.Jwt.Value)
                {
                    zendeskAuthType = ZendeskAuthType.Jwt;
                }

                if (!string.IsNullOrEmpty(commonTags))
                {
                    tags.AddRange(commonTags.Split(',').ToList());
                }

                if (invisibleCustomFields != null)
                {
                    SetInvisibleCustomFields(invisibleCustomFields);
                }

                if (isConversationsEnabled)
                {
                    newCommentEndUser.GetComponent<InputField>().onValueChanged.AddListener(delegate
                    {
                        CheckAvailabilityOfSendButton(null);
                    });
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void SetSupportStrings()
        {
            myTicketsFooterNeedHelpText.text =
                zendeskMain.zendeskLocalizationHandler.translationGameObjects["usdk_need_more_help_label"];
            conversationalFooterNeedHelpText.text =
                zendeskMain.zendeskLocalizationHandler.translationGameObjects["usdk_need_more_help_label"];
            myTicketsFooterContactUsButtonText.text =
                zendeskMain.zendeskLocalizationHandler.translationGameObjects["usdk_get_in_touch_label_saddle_bar"];
            ticketResponseFooterContactUsButtonText.text =
                zendeskMain.zendeskLocalizationHandler.translationGameObjects["usdk_get_in_touch_label_saddle_bar"];
            noTicketsPanelGeneralInfoText.text =
                zendeskMain.zendeskLocalizationHandler.translationGameObjects["usdk_help_center_description"];
            noTicketsPanelStartConversationText.text =
                zendeskMain.zendeskLocalizationHandler.translationGameObjects[
                    "usdk_get_in_touch_label_saddle_bar"];
            createRequestPanelNameText.text =
                zendeskMain.zendeskLocalizationHandler.translationGameObjects["usdk_label_name"];
            createRequestPanelInvalidNameText.text =
                zendeskMain.zendeskLocalizationHandler.translationGameObjects["usdk_required_field_label"];
            createRequestPanelEmailText.text =
                zendeskMain.zendeskLocalizationHandler.translationGameObjects["usdk_label_email"];
            createRequestPanelInvalidEmailText.text =
                zendeskMain.zendeskLocalizationHandler.translationGameObjects["usdk_enter_valid_email"];
            createRequestPanelMessageText.text =
                zendeskMain.zendeskLocalizationHandler.translationGameObjects["usdk_label_message"];
            createRequestPanelInvalidMessageText.text =
                zendeskMain.zendeskLocalizationHandler.translationGameObjects["usdk_required_field_label"];
            typeMessageText.text = zendeskMain.zendeskLocalizationHandler.translationGameObjects["usdk_type_a_message_label_without_dots"];
            createRequestPanelFeedbackPanelTitle.text =
                zendeskMain.zendeskLocalizationHandler.translationGameObjects["usdk_got_the_message_label"];
            createRequestPanelFeedbackPanelDescription.text = string.IsNullOrEmpty(ticketConfirmationMessageString)
                ? zendeskMain.zendeskLocalizationHandler.translationGameObjects["usdk_create_request_feedback_message_label"]
                : ticketConfirmationMessageString;
            createRequestPanelFeedbackPanelButtonText.text =
                zendeskMain.zendeskLocalizationHandler.translationGameObjects["usdk_back_button_label"];
            authScreenErrorTitle = zendeskLocalizationHandler.translationGameObjects["usdk_jwt_error_message"];
            authScreenErrorBody = zendeskLocalizationHandler.translationGameObjects["usdk_error_page_message_new_design"];
            jwtIntegratorError = zendeskLocalizationHandler.translationGameObjects["usdk_jwt_token_integrator_error"];
            //            createRequestPanelFeedbackToastText.text = zendeskMain.zendeskLocalizationHandler.translationGameObjects["usdk_error_msg_unable_to_send"];
        }

        public void OpenSupportPanel(string requestTags = "")
        {
            try
            {
                if (!zendeskCanvas.activeSelf)
                {
                    zendeskCanvas.SetActive(true);
                }
                gameObject.SetActive(true);
                ShowMyTicketsWindow();
                tags.AddRange(requestTags.Split(',').ToList());
            }
            catch
            {
                zendeskErrorUI.NavigateError(null, true);
            }
        }

        private void InitCreateRequestPanel()
        {
            try
            {
                isUserAuthenticatedBefore = this.zendeskMain.IsUserAuthenticatedBefore();
                createRequestFooterPanelSendButton.interactable = true;
                createRequestFooterPanelAttachmentButton.interactable = true;
                createRequestPanelFeedbackToast.SetActive(false);
                createRequestPanelFeedbackPanel.SetActive(false);
                createRequestPanelInputEmail.interactable = true;
                createRequestPanelInputName.interactable = true;
                createRequestPanelInputMessage.interactable = true;
                ColorUtility.TryParseHtmlString("#D8DCDE", out greyOutline);
                ColorUtility.TryParseHtmlString("#CC3340", out redOutline);
                createRequestPanelInputMessage.GetComponentInParent<Image>().color = greyOutline;
                createRequestPanelInputMessage.text = "";
                createRequestPanelMessageValidation.SetActive(false);
                float heightOfMessageContainer = 0f;
                if (zendeskAuthType.Value == ZendeskAuthType.Jwt.Value || isUserAuthenticatedBefore)
                {
                    createRequestPanelNameContainer.SetActive(false);
                    createRequestPanelEmailContainer.SetActive(false);
                }
                else
                {
                    createRequestPanelNameContainer.SetActive(true);
                    createRequestPanelEmailContainer.SetActive(true);
                    createRequestPanelInputEmail.GetComponentInParent<Image>().color = greyOutline;
                    createRequestPanelInputEmail.text = "";
                    createRequestPanelInputName.GetComponentInParent<Image>().color = greyOutline;
                    createRequestPanelInputName.text = "";
                    createRequestPanelEmailValidation.SetActive(false);
                    createRequestPanelNameValidation.SetActive(false);
                    heightOfMessageContainer +=
                        createRequestPanelNameContainer.GetComponent<RectTransform>().rect.height +
                        createRequestPanelEmailContainer.GetComponent<RectTransform>().rect.height +
                        (createRequestPanelFieldContainer.GetComponent<VerticalLayoutGroup>().spacing * 2);
                }

                heightOfMessageContainer +=
                    createRequestPanelFieldContainer.GetComponent<VerticalLayoutGroup>().padding.bottom +
                    createRequestPanelFieldContainer.GetComponent<VerticalLayoutGroup>().padding.top;
                heightOfMessageContainer = createRequestScrollView.GetComponent<RectTransform>().rect.height -
                                           heightOfMessageContainer;
                if ((requestForm == null || requestForm.Fields == null || requestForm.Fields.Count == 0) &&
                    createRequestPanelMessageContainer.GetComponent<LayoutElement>().minHeight <
                    heightOfMessageContainer)
                {
                    createRequestPanelMessageContainer.GetComponent<LayoutElement>().minHeight =
                        heightOfMessageContainer;
                }

                AddCustomFieldsUi();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void InitLocalizationHandlerCustomFields()
        {
            try
            {
                if (customFieldTranslations != null && zendeskCustomFieldLocalizationHandler == null)
                {
                    zendeskCustomFieldLocalizationHandler = gameObject.AddComponent<ZendeskLocalizationHandler>();
                    zendeskCustomFieldLocalizationHandler.translationsFile = customFieldTranslations;
                    zendeskCustomFieldLocalizationHandler.SetLocaleISO((int) zendeskMain.locale);
                    zendeskCustomFieldLocalizationHandler.ReadData("", false);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private string GetTranslatedCustomField(string key)
        {
            string textTranslated = "";
            try
            {
                textTranslated = zendeskCustomFieldLocalizationHandler.translationGameObjects[key];
            }
            catch
            {
                // throw e;
            }

            return textTranslated;
        }

        private void AddTags(string requestTags)
        {
            try
            {
                tags.AddRange(requestTags.Split(',').ToList());
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void SendRequest()
        {
            if (customFields == null) customFields = new List<CustomField>();
            customFields.AddRange(GetCustomFieldsValues());
            try
            {
                if (ValidateRequestForm())
                {
                    zendeskUI.StartLoading(headerPanel);
                    createRequestFooterPanelSendButton.interactable = false;
                    createRequestFooterPanelAttachmentButton.interactable = false;
                    createRequestPanelInputEmail.interactable = false;
                    createRequestPanelInputName.interactable = false;
                    createRequestPanelInputMessage.interactable = false;
                    var wrapper = CreateRequestWrapperObject(createRequestPanelInputName.text,
                        createRequestPanelInputEmail.text, createRequestPanelInputMessage.text, customFields);
                    zendeskMain.supportProvider.CreateRequest(CreateRequestCallback, wrapper, attachmentPaths);
                }
            }
            catch
            {
                zendeskErrorUI.NavigateError(null, true);
                zendeskUI.FinishLoading();
            }
        }

        private CreateRequestWrapper CreateRequestWrapperObject(string userName, string email, string message,
            List<CustomField> customFields = null)
        {
            try
            {
                var req = new CreateRequest();
                req.Requester = new Requester();
                if (zendeskAuthType.Value == ZendeskAuthType.Anonymous.Value)
                {
                    req.Requester.Name = userName;
                    req.Requester.Email = email;
                }

                req.Comment = new Comment();
                req.CustomFields = customFields;
                req.Comment.Body = message;
                req.Tags = tags;
                req.Subject = message;
                var wrapper = new CreateRequestWrapper();
                wrapper.Request = req;
                return wrapper;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void CreateRequestCallback(ZendeskResponse<RequestResponse> requestResponse)
        {
            try
            {
                createRequestFooterPanelAttachmentButton.interactable = true;
                if (requestResponse.IsError)
                {
                    if (requestResponse.ErrorResponse.IsAuthError && requestResponse.ErrorResponse.Status == 401)
                    {
                        if (this.zendeskAuthType.Value == ZendeskAuthType.Jwt.Value)
                        {
                            Debug.Log(jwtIntegratorError);
                        }

                        zendeskErrorUI.NavigateError(authScreenErrorBody, true, false, authScreenErrorTitle);
                        ClearCreateRequestPage();
                    }
                    else
                    {
                        ShowCreateTicketFormFeedback(false);
                    }

                    return;
                }

                RemoveAllAttachments();
                if (!isConversationsEnabled)
                    ShowCreateTicketFormFeedback(true, requestResponse);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                zendeskUI.FinishLoading();
            }
        }

        private void CreateRequestConversationalCallback(ZendeskResponse<RequestResponse> requestResponse)
        {
            try
            {
                if (ticketResponseListView != null && ticketResponseListView.activeSelf &&
                    ticketResponseListView.activeInHierarchy && string.IsNullOrEmpty(ticketId))
                {
                    attachmentButton.GetComponent<Button>().interactable = true;
                    newCommentEndUser.GetComponent<InputField>().interactable = true;
                    sendCommentButton.GetComponent<Button>().interactable = true;
                    if (requestResponse.IsError)
                    {
                        if (!zendeskErrorUI.IfAuthError(requestResponse.ErrorResponse, zendeskAuthType))
                        {
                            zendeskErrorUI.NavigateError(
                                zendeskLocalizationHandler.translationGameObjects["usdk_label_failed"]);
                        }

                        CheckAvailabilityOfSendButton(true);
                        return;
                    }

                    ClearConversationalFooter();
                    ticketId = requestResponse.Result.Request.Id;
                    CheckAvailabilityOfSendButton(null);
                    zendeskUI.backStateGO[zendeskUI.backStateGO.Count - 1].Parameter = requestResponse.Result.Request;
                    prevDate = DateTime.Today;
                    zendeskMain.supportProvider.GetComments(GetCommentsCreateRequestConversationalCallback, ticketId);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void GetCommentsCreateRequestConversationalCallback(ZendeskResponse<CommentsResponse> commentsResponse)
        {
            try
            {
                if (string.IsNullOrEmpty(ticketId))
                    return;
                prevDate = DateTime.MinValue;
                if (commentsResponse.IsError)
                {
                    if (!zendeskErrorUI.IfAuthError(commentsResponse.ErrorResponse, zendeskAuthType))
                    {
                        zendeskErrorUI.NavigateError(commentsResponse.ErrorResponse.Reason);
                    }

                    return;
                }

                var response = commentsResponse.Result;
                long previousAuthorId = -1;
                foreach (var comment in response.Comments)
                {
                    GameObject g = null;
                    if (comment.CreatedAt.HasValue && !comment.CreatedAt.Value.Date.Equals(prevDate.Date))
                    {
                        prevDate = comment.CreatedAt.Value;
                        previousAuthorId = -1;
                    }

                    if (comment.Agent)
                    {
                        g = Instantiate(agentResponsePrefab, ticketResponseListView.transform);
                    }
                    else
                    {
                        g = Instantiate(endUserResponsePrefab, ticketResponseListView.transform);
                        previousAuthorId = -1;
                    }

                    var isPreviousAuthor = previousAuthorId == comment.AuthorId;
                    var author = response.Users.FirstOrDefault(u => u.Id == comment.AuthorId);
                    if (author != null)
                    {
                        if (isPreviousAuthor)
                        {
                            DeleteAgentImageFromPreviousResponse(
                                ticketResponseContainerList[ticketResponseContainerList.Count - 1]);
                        }

                        g.GetComponent<ZendeskTicketResponse>().Init(comment, zendeskMain.supportProvider, this,
                            zendeskErrorUI, author, isPreviousAuthor,
                            response.Users.Where(u => u.Id == comment.AuthorId).Select(u => u.Photo).First(), null,
                            zendeskMain.zendeskHtmlHandler, zendeskLocalizationHandler);
                    }
                    else
                    {
                        g.GetComponent<ZendeskTicketResponse>().Init(comment, zendeskMain.supportProvider, this,
                            zendeskErrorUI, null, isPreviousAuthor, null, null, zendeskMain.zendeskHtmlHandler,
                            zendeskLocalizationHandler);
                    }

                    if (!comment.Agent)
                    {
                        g.GetComponent<ZendeskTicketResponse>().MessageSentStatus(true);
                    }

                    previousAuthorId = comment.AuthorId;
                    ClearConversationalFooter();
                    ticketResponseContainerList.Add(g);
                }

                createRequestFooterPanelSendButton.interactable = false;
                if (ticketResponseListView != null && ticketResponseListView.activeSelf &&
                    ticketResponseListView.activeInHierarchy)
                    ScrollDownTicketResponse(true);
                if (!string.IsNullOrEmpty(ticketConfirmationMessageString))
                {
                    StartCoroutine(AddConfirmationMessageConversational(ticketConfirmationMessageString, 2));
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void ClearCreateRequestPage()
        {
            createRequestFooterPanelSendButton.interactable = true;
            createRequestPanelInputEmail.interactable = true;
            createRequestPanelInputName.interactable = true;
            createRequestPanelInputMessage.interactable = true;
        }

        private void ShowCreateTicketFormFeedback(bool success, ZendeskResponse<RequestResponse> requestResponse = null)
        {
            try
            {
                if (success)
                {
                    createRequestPanelFeedbackPanel.SetActive(true);
                }
                else
                {
                    zendeskErrorUI.NavigateError(
                        zendeskLocalizationHandler.translationGameObjects["usdk_label_failed"]);
                    ClearCreateRequestPage();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void LoadTickets()
        {
            try
            {
                zendeskUI.StartLoading(headerPanel);
                ClearExistingTickets();
                zendeskMain.supportProvider.GetAllRequests(AllRequestsCallback, "");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void AllRequestsCallback(ZendeskResponse<RequestsResponse> requestsResponse)
        {
            try
            {
                if (requestsResponse.IsError)
                {
                    if (requestsResponse.ErrorResponse.IsNetworkError)
                    {
                    }

                    if (!zendeskErrorUI.IfAuthError(requestsResponse.ErrorResponse, zendeskAuthType))
                    {
                        zendeskErrorUI.NavigateError(null, true);
                    }

                    return;
                }

                var requests = requestsResponse.Result;
                if (requests != null && requests.Requests != null && requests.Requests.Count > 0)
                {
                    myTicketsFooter.SetActive(true);
                    noTicketsPanel.SetActive(false);
                    noTicketsFooter.SetActive(false);
                    ClearExistingTickets();
                    foreach (var req in requests.Requests)
                    {
                        var g = Instantiate(ticketContainerPrefab, ticketListView.transform);
                        g.GetComponent<ZendeskTicketContentOverview>().Init(req, zendeskMain.supportProvider, this,
                            zendeskErrorUI, zendeskMain.zendeskHtmlHandler, zendeskLocalizationHandler);
                        ticketContainersList.Add(g);
                    }
                }
                else
                {
                    myTicketsFooter.SetActive(false);
                    noTicketsPanel.SetActive(true);
                    noTicketsFooter.SetActive(true);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                zendeskUI.FinishLoading();
            }
        }

        private void ClearExistingTickets()
        {
            try
            {
                foreach (var g in ticketContainersList)
                {
                    var textFields = g.GetComponentsInChildren<Text>();
                    foreach (var txt in textFields)
                    {
                        txt.text = "";
                    }

                    Destroy(g);
                }

                ticketContainersList.Clear();
                ticketContainersList = new List<GameObject>();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private bool ValidateRequestForm()
        {
            try
            {
                if (zendeskAuthType.Value == ZendeskAuthType.Anonymous.Value && !isUserAuthenticatedBefore)
                {
                    ValidateEmail();
                    ValidateName();
                }
                else
                {
                    emailValid = true;
                    nameValid = true;
                }

                ValidateMessage();
                ValidateCustomFields();
                return emailValid & nameValid & messageValid && customFieldValid;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void ValidateCustomFields()
        {
            try
            {
                customFieldValid = true;
                foreach (var item in customFieldsPrefabs)
                {
                    if (item.Value == FieldType.Text || item.Value == FieldType.Multiline ||
                        item.Value == FieldType.Numeric || item.Value == FieldType.Decimal)
                    {
                        var prefabScript = item.Key.GetComponent<ZendeskCustomTextFieldScript>();
                        if (prefabScript.required)
                        {
                            var customFieldValue = prefabScript.inputField.text.Trim();
                            if (string.IsNullOrEmpty(customFieldValue) && customFieldValue.Length == 0)
                            {
                                prefabScript.inputField.GetComponentInParent<Image>().color = redOutline;
                                prefabScript.validationPanel.SetActive(true);
                                customFieldValid = false;
                            }
                            else
                            {
                                prefabScript.inputField.GetComponentInParent<Image>().color = greyOutline;
                                prefabScript.validationPanel.SetActive(false);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void ValidateEmail()
        {
            try
            {
                var email = createRequestPanelInputEmail.text.Trim();
                emailValid = true;

                #region Email validation

                if (!email.Contains("@"))
                {
                    createRequestPanelInputEmail.GetComponentInParent<Image>().color = redOutline;
                    if ((requestForm == null || requestForm.Fields == null || requestForm.Fields.Count == 0) &&
                        !createRequestPanelEmailValidation.activeSelf)
                    {
                        createRequestPanelMessageContainer.GetComponent<LayoutElement>().minHeight -=
                            createRequestPanelEmailValidation.GetComponent<RectTransform>().rect.height +
                            createRequestPanelEmailContainer.GetComponent<VerticalLayoutGroup>().spacing;
                    }

                    createRequestPanelEmailValidation.SetActive(true);
                    emailValid = false;
                }
                else if (email.Contains("@"))
                {
                    createRequestPanelInputEmail.GetComponentInParent<Image>().color = greyOutline;
                    if ((requestForm == null || requestForm.Fields == null || requestForm.Fields.Count == 0) &&
                        createRequestPanelEmailValidation.activeSelf)
                    {
                        createRequestPanelMessageContainer.GetComponent<LayoutElement>().minHeight +=
                            createRequestPanelEmailValidation.GetComponent<RectTransform>().rect.height +
                            createRequestPanelEmailContainer.GetComponent<VerticalLayoutGroup>().spacing;
                    }

                    createRequestPanelEmailValidation.SetActive(false);
                }

                #endregion
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void ValidateName()
        {
            try
            {
                var nameInputFieldText = createRequestPanelInputName.text.Trim();
                nameValid = true;

                #region Name validation

                if (string.IsNullOrEmpty(nameInputFieldText) && nameInputFieldText.Length == 0)
                {
                    createRequestPanelInputName.GetComponentInParent<Image>().color = redOutline;
                    if ((requestForm == null || requestForm.Fields == null || requestForm.Fields.Count == 0) &&
                        !createRequestPanelNameValidation.activeSelf)
                    {
                        createRequestPanelMessageContainer.GetComponent<LayoutElement>().minHeight -=
                            createRequestPanelNameValidation.GetComponent<RectTransform>().rect.height +
                            createRequestPanelNameContainer.GetComponent<VerticalLayoutGroup>().spacing;
                    }

                    createRequestPanelNameValidation.SetActive(true);
                    nameValid = false;
                }
                else if (!string.IsNullOrEmpty(nameInputFieldText) && nameInputFieldText.Length != 0)
                {
                    createRequestPanelInputName.GetComponentInParent<Image>().color = greyOutline;
                    if ((requestForm == null || requestForm.Fields == null || requestForm.Fields.Count == 0) &&
                        createRequestPanelNameValidation.activeSelf)
                    {
                        createRequestPanelMessageContainer.GetComponent<LayoutElement>().minHeight +=
                            createRequestPanelNameValidation.GetComponent<RectTransform>().rect.height +
                            createRequestPanelNameContainer.GetComponent<VerticalLayoutGroup>().spacing;
                    }

                    createRequestPanelNameValidation.SetActive(false);
                }

                #endregion
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void ValidateMessage()
        {
            try
            {
                var message = createRequestPanelInputMessage.text.Trim();
                messageValid = true;

                #region Message validation

                if (string.IsNullOrEmpty(message) && message.Length == 0)
                {
                    createRequestPanelInputMessage.GetComponentInParent<Image>().color = redOutline;
                    createRequestPanelMessageValidation.SetActive(true);
                    messageValid = false;
                }
                else
                {
                    createRequestPanelInputMessage.GetComponentInParent<Image>().color = greyOutline;
                    createRequestPanelMessageValidation.SetActive(false);
                }

                #endregion
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #region Comments/Response

        private void GetRequestComments(string id, bool refreshFooter)
        {
            try
            {
                ticketId = id;
                zendeskMain.supportProvider.GetComments(
                    commentsResponse => { GetRequestCommentsCallback(commentsResponse, refreshFooter); },
                    id);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void RefreshComments(string id)
        {
            try
            {
                ticketId = id;
                zendeskMain.supportProvider.GetComments(RefreshCommentsCallback, id, lastCommentDatetime);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private IEnumerator AddConfirmationMessageConversational(string message, float delay = 0)
        {
            Coroutine ticketConfirmationMessageCoroutine = null;
            if (delay > 0)
            {
                ticketConfirmationMessageCoroutine = StartCoroutine(WaitForRealSeconds(delay));
                ticketConfirmationMessageCoroutines.Add(ticketConfirmationMessageCoroutine);
                yield return ticketConfirmationMessageCoroutine;
                var g = Instantiate(ticketConfirmationMessagePrefab, ticketResponseListView.transform);
                g.GetComponent<ZendeskTicketConfirmationMessage>().Init(message);
                ticketResponseContainerList.Add(g);
                ScrollDownTicketResponse();
                ticketConfirmationMessageCoroutines.Remove(ticketConfirmationMessageCoroutine);
            }
            else
            {
                try
                {
                    if (delay == 0 || (ticketConfirmationMessageCoroutines.Count > 0 &&
                                       ticketConfirmationMessageCoroutines.Contains(ticketConfirmationMessageCoroutine))
                    )
                    {
                        var g = Instantiate(ticketConfirmationMessagePrefab, ticketResponseListView.transform);
                        g.GetComponent<ZendeskTicketConfirmationMessage>().Init(message);
                        ticketResponseContainerList.Add(g);
                        ScrollDownTicketResponse();
                        ticketConfirmationMessageCoroutines.Remove(ticketConfirmationMessageCoroutine);
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        private void AddConfirmationMessageConversationalNoFade(string message)
        {
            try
            {
                var g = Instantiate(ticketConfirmationMessagePrefab, ticketResponseListView.transform);
                g.GetComponent<ZendeskTicketConfirmationMessage>().Init(message);
                ticketResponseContainerList.Add(g);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public GameObject AddAnswerBotCSAT(string message)
        {
            try
            {
                var g = Instantiate(answerBotResponsePrefab, ticketResponseListView.transform);
                g.GetComponent<ZendeskAnswerBotTicketResponse>()
                 .Init(message, zendeskErrorUI, zendeskLocalizationHandler, false);
                ticketResponseContainerList.Add(g);
                return g;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private IEnumerator WaitForRealSeconds(float time)
        {
            float start = Time.realtimeSinceStartup;
            while (Time.realtimeSinceStartup < start + time)
            {
                yield return null;
            }
        }

        private void GetRequestCommentsCallback(ZendeskResponse<CommentsResponse> commentsResponse, bool refreshFooter)
        {
            try
            {
                bool confirmationMessageAdded = false;
                prevDate = DateTime.MinValue;
                if (commentsResponse.IsError)
                {
                    if (!zendeskErrorUI.IfAuthError(commentsResponse.ErrorResponse, zendeskAuthType))
                    {
                        zendeskErrorUI.NavigateError(null, true);
                    }

                    return;
                }

                var response = commentsResponse.Result;
                ClearTicketResponse();
                RetrieveTicketResponseData();
                if (refreshFooter)
                {
                    ClearConversationalFooter();
                }

                long previousAuthorId = -1;
                foreach (var comment in response.Comments)
                {
                    if (comment.RequestId != this.ticketId)
                    {
                        return;
                    }

                    GameObject g = null;
                    if (comment.CreatedAt.HasValue && !comment.CreatedAt.Value.Date.Equals(prevDate.Date))
                    {
                        prevDate = comment.CreatedAt.Value;
                        previousAuthorId = -1;
                        AddMessageDatePrefab();
                    }

                    if (comment.Agent)
                    {
                        g = Instantiate(agentResponsePrefab, ticketResponseListView.transform);
                    }
                    else
                    {
                        g = Instantiate(endUserResponsePrefab, ticketResponseListView.transform);
                    }

                    if (!string.IsNullOrEmpty(ticketConfirmationMessageString) && !confirmationMessageAdded)
                    {
                        AddConfirmationMessageConversationalNoFade(ticketConfirmationMessageString);
                        confirmationMessageAdded = true;
                    }

                    var isPreviousAuthor = previousAuthorId == comment.AuthorId;
                    var author = response.Users.FirstOrDefault(u => u.Id == comment.AuthorId);
                    if (author != null)
                    {
                        if (isPreviousAuthor)
                        {
                            if (comment.Agent)
                                DeleteAgentImageFromPreviousResponse(
                                    ticketResponseContainerList[ticketResponseContainerList.Count - 1]);
                        }
                        else
                        {
                            g.GetComponent<HorizontalLayoutGroup>().padding.top = 24;
                        }

                        g.GetComponent<ZendeskTicketResponse>().Init(comment, zendeskMain.supportProvider, this,
                            zendeskErrorUI, author, isPreviousAuthor,
                            response.Users.Where(u => u.Id == comment.AuthorId).Select(u => u.Photo).First(), null,
                            zendeskMain.zendeskHtmlHandler, zendeskLocalizationHandler);
                    }
                    else
                    {
                        g.GetComponent<ZendeskTicketResponse>().Init(comment, zendeskMain.supportProvider, this,
                            zendeskErrorUI, null, isPreviousAuthor, null, null, zendeskMain.zendeskHtmlHandler,
                            zendeskLocalizationHandler);
                    }

                    if (!comment.Agent)
                    {
                        g.GetComponent<ZendeskTicketResponse>().MessageSentStatus(true);
                    }

                    previousAuthorId = comment.AuthorId;
                    ticketResponseContainerList.Add(g);
                }

                if (request.StatusEnum == RequestStatus.Closed)
                {
                    var go = Instantiate(endOfConversationPrefab, ticketResponseListView.transform);
                    go.GetComponent<ZendeskTextContentScript>().Init(new[]
                        {zendeskLocalizationHandler.translationGameObjects["usdk_conversation_ended"]});
                    ticketResponseContainerList.Add(go);
                    this.createRequestAttachmentLine.SetActive(false);
                    this.footerLineMain.SetActive(false);
                    this.footerClosedStatus.SetActive(true);
                }
                else if (request.StatusEnum == RequestStatus.Solved)
                {
                    if (isCsatEnabled)
                    {
                        satisfactionRatingPrefabInstatiated =
                            Instantiate(satisfactionRatingPrefab, ticketResponseListView.transform);
                        satisfactionRatingPrefabInstatiated.SetActive(true);
                        ticketResponseContainerList.Add(satisfactionRatingPrefabInstatiated);
                        satisfactionRatingPrefabInstatiated.GetComponent<ZendeskCustomerSatisfactionRating>()
                                                           .Init(request, zendeskMain.supportProvider, zendeskMain,
                                                               ticketResponseListView, zendeskErrorUI,
                                                               zendeskLocalizationHandler);
                    }
                }

                createRequestFooterPanelSendButton.interactable = false;
                ScrollDownTicketResponse(true);

                //update local storage ticket to reflect comment count
                var localRequest = ZendeskLocalStorage.GetSupportRequest(request.Id);
                ZendeskLocalStorage.SaveSupportRequest(request.Id, response.Comments.Count(), true, localRequest.score,
                    localRequest.ratingComment);
                lastCommentDatetime = response.Comments.LastOrDefault().CreatedAt.Value.AddSeconds(1);
            }
            catch
            {
                zendeskErrorUI.NavigateError(null, true);
            }
        }

        private void Update()
        {
            #if UNITY_IOS || UNITY_ANDROID
            if (TouchScreenKeyboard.visible)
            {
                keyboardBackground.SetActive(true);
            }
            else
            {
                keyboardBackground.SetActive(false);
            }
            #endif
        }

        public void ScrollDownTicketResponse(bool forceScroll = false)
        {
            try
            {
                if (ticketResponseListView != null && ticketResponseListView.activeSelf)
                {
                    if (ticketResponseListView.transform.childCount > 0)
                    {
                        if (forceScroll)
                            ticketResponseListView.GetComponentInParent<RectTransform>().anchoredPosition =
                                new Vector2(0, 10000000);
                        else
                        {
                            float componentsHeighSum = 0;
                            ticketResponseContainerList.ForEach(a =>
                                componentsHeighSum += a.GetComponent<RectTransform>().rect.height);
                            if (componentsHeighSum >=
                                (ticketResponseScrollView.GetComponent<RectTransform>().rect.height - 100))
                            {
                                if (ticketResponseListView != null && ticketResponseListView.activeSelf)
                                    ticketResponseListView.GetComponentInParent<RectTransform>().anchoredPosition =
                                        new Vector2(0, 10000000);
                            }
                            else
                            {
                                if (ticketResponseListView != null && ticketResponseListView.activeSelf)
                                    ticketResponseListView
                                        .GetComponentInParent<ScrollRect>().verticalNormalizedPosition = -1;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                // ignored
            }
        }

        private void DeleteAgentImageFromPreviousResponse(GameObject previousAgentResponse)
        {
            try
            {
                var response = previousAgentResponse.GetComponent<ZendeskTicketResponse>();
                response.agentPhotoContainerGO.gameObject.SetActive(false);
            }
            catch
            {
            }
        }

        private void RefreshCommentsCallback(ZendeskResponse<CommentsResponse> commentsResponse)
        {
            try
            {
                var localSupport = ZendeskLocalStorage.GetSupportRequest(request.Id);
                if (commentsResponse.IsError)
                {
                    if (!zendeskErrorUI.IfAuthError(commentsResponse.ErrorResponse, zendeskAuthType))
                    {
                        zendeskErrorUI.NavigateError(commentsResponse.ErrorResponse.Reason);
                    }

                    return;
                }

                var response = commentsResponse.Result;
                long previousAuthorId = -1;
                foreach (var comment in response.Comments)
                {
                    GameObject g = null;
                    if (comment.CreatedAt.HasValue && !comment.CreatedAt.Value.Date.Equals(prevDate.Date))
                    {
                        prevDate = comment.CreatedAt.Value;
                        AddMessageDatePrefab();
                    }

                    if (comment.Agent)
                    {
                        g = Instantiate(agentResponsePrefab, ticketResponseListView.transform);
                    }
                    else
                    {
                        g = Instantiate(endUserResponsePrefab, ticketResponseListView.transform);
                        previousAuthorId = -1;
                    }

                    var isPreviousAuthor = previousAuthorId == comment.AuthorId;
                    var author = response.Users.FirstOrDefault(u => u.Id == comment.AuthorId);
                    if (author != null)
                    {
                        if (isPreviousAuthor)
                        {
                            DeleteAgentImageFromPreviousResponse(
                                ticketResponseContainerList[ticketResponseContainerList.Count - 1]);
                        }

                        g.GetComponent<ZendeskTicketResponse>().Init(comment, zendeskMain.supportProvider, this,
                            zendeskErrorUI, author, isPreviousAuthor,
                            response.Users.Where(u => u.Id == comment.AuthorId).Select(u => u.Photo).First(), null,
                            zendeskMain.zendeskHtmlHandler, zendeskLocalizationHandler);
                    }
                    else
                    {
                        g.GetComponent<ZendeskTicketResponse>().Init(comment, zendeskMain.supportProvider, this,
                            zendeskErrorUI, null, isPreviousAuthor, null, null,
                            zendeskMain.zendeskHtmlHandler, zendeskLocalizationHandler);
                    }

                    previousAuthorId = comment.AuthorId;
                    ticketResponseContainerList.Add(g);
                    if (!comment.Agent)
                    {
                        g.GetComponent<ZendeskTicketResponse>().MessageSentStatus(true);
                    }
                }

                ScrollDownTicketResponse(true);
                ZendeskLocalStorage.SaveSupportRequest(request.Id,
                    localSupport.commentCount + response.Comments.Count(),
                    true, localSupport.score, localSupport.ratingComment);
                lastCommentDatetime = DateTime.Now;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void ClearTicketResponse()
        {
            try
            {
                foreach (var g in ticketResponseContainerList)
                {
                    if (g != null)
                    {
                        var textFields = g.GetComponentsInChildren<Text>();
                        foreach (var txt in textFields)
                        {
                            txt.text = "";
                        }

                        Destroy(g);
                    }
                }

                ticketResponseContainerList.Clear();
                ticketResponseContainerList = new List<GameObject>();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void AddComment()
        {
            try
            {
                if (satisfactionRatingPrefabInstatiated != null)
                {
                    satisfactionRatingPrefabInstatiated.GetComponent<ZendeskCustomerSatisfactionRating>().ClearCSAT();
                    GameObject.Destroy(satisfactionRatingPrefabInstatiated);
                }

                CheckAvailabilityOfSendButton(false);
                attachmentButton.GetComponent<Button>().interactable = false;
                newCommentEndUser.GetComponent<InputField>().interactable = false;
                sendCommentButton.GetComponent<Button>().interactable = false;
                var newCommentIF = newCommentEndUser.GetComponent<InputField>();
                var text = "";
                if (attachmentPaths != null && attachmentPaths.Count > 0 && string.IsNullOrEmpty(newCommentIF.text))
                {
                    text = "[" + String.Join(",", attachmentPaths.ToArray()) + "]";
                }
                else if (!string.IsNullOrEmpty(newCommentIF.text))
                {
                    text = newCommentIF.text;
                }

                if ((string.IsNullOrEmpty(ticketId) && string.IsNullOrEmpty(text)) ||
                    (!string.IsNullOrEmpty(ticketId) && string.IsNullOrEmpty(text) &&
                     (attachmentPaths == null || attachmentPaths.Count == 0)))
                {
                    var error = new ZendeskResponse<object>();
                    ZendeskUtils.CheckErrorAndAssign(null,
                        zendeskMain.zendeskLocalizationHandler.translationGameObjects["usdk_required_field_label"],
                        error);
                    zendeskErrorUI.NavigateError(error.ErrorResponse.Reason);
                    return;
                }

                if (string.IsNullOrEmpty(ticketId))
                {
                    var wrapper = CreateRequestWrapperObject(zendeskMain.GetUserName(), zendeskMain.GetUserEmail(),
                        text, customFields);
                    zendeskMain.supportProvider.CreateRequest(CreateRequestConversationalCallback, wrapper,
                        attachmentPaths);
                }
                else
                {
                    zendeskMain.supportProvider.AddComment(AddCommentCallback, ticketId, text, attachmentPaths);
                }

                if (attachmentPaths.Count > 0)
                {
                    zendeskErrorUI.NavigateError(
                        zendeskLocalizationHandler.translationGameObjects["usdk_sending_label"]);
                }
            }
            catch
            {
                zendeskErrorUI.NavigateError(null, true);
            }
        }

        private void CheckAvailabilityOfSendButton(bool? forceButton)
        {
            try
            {
                var newCommentIF = newCommentEndUser.GetComponent<InputField>();
                if (!string.IsNullOrEmpty(ticketId))
                {
                    attachmentButton.GetComponent<Button>().interactable = true;
                    attachmentButton.GetComponentInChildren<RawImage>().color = new Color32(33, 43, 53, 255);
                }

                var isSendButtonActive = !string.IsNullOrEmpty(newCommentIF.text) ||
                                         (attachmentPaths.Count != 0 && !string.IsNullOrEmpty(ticketId));
                if (forceButton.HasValue)
                {
                    isSendButtonActive = forceButton.Value;
                }

                if (addCommentButtonImage != null && addCommentButtonImage.gameObject != null &&
                    addCommentButtonImage.gameObject.GetComponentInParent<Button>() != null)
                {
                    addCommentButtonImage.gameObject.GetComponentInParent<Button>().interactable = isSendButtonActive;
                    if (isSendButtonActive)
                    {
                        addCommentButtonImage.gameObject.SetActive(true);
                        addCommentButtonImage.GetComponent<RawImage>().color =
                            new Color32(23, 73, 77, 255);
                        attachmentButton.GetComponent<Button>().interactable = true;
                        attachmentButton.GetComponentInChildren<RawImage>().color = new Color32(33, 43, 53, 255);
                    }
                    else
                    {
                        addCommentButtonImage.gameObject.SetActive(false);
                        addCommentButtonImage.GetComponent<RawImage>().color =
                            new Color32(194, 194, 194,
                                255);
                        if (string.IsNullOrEmpty(ticketId))
                        {
                            attachmentButton.GetComponent<Button>().interactable = false;
                            attachmentButton.GetComponentInChildren<RawImage>().color = new Color32(104, 115, 125, 69);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void ClearConversationalFooter()
        {
            try
            {
                typeMessageText.text =
                    zendeskMain.zendeskLocalizationHandler.translationGameObjects["usdk_type_a_message_label_without_dots"];
                this.newCommentEndUser.GetComponent<InputField>().text = "";
                this.createRequestAttachmentLine.SetActive(false);
                this.footerLineMain.SetActive(true);
                this.footerClosedStatus.SetActive(false);
                RemoveAllAttachments();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void RemoveAllAttachments()
        {
            try
            {
                attachmentPaths.Clear();
                Transform attachments;
                if (isConversationsEnabled)
                {
                    this.createRequestAttachmentLine.SetActive(false);
                    this.ticketResponseAttachmentCountContainer.SetActive(false);
                    this.ticketResponseAttachmentCountText.text = String.Empty;
                    attachments = createRequestAttachmentLine.transform.GetChild(0).GetChild(0).transform;
                }
                else
                {
                    this.createRequestPanelAttachmentLine.SetActive(false);
                    this.createRequestAttachmentCountContainer.SetActive(false);
                    this.createRequestAttachmentCountText.text = string.Empty;
                    attachments = createRequestPanelAttachmentLine.transform.GetChild(0).GetChild(0).transform;
                }

                foreach (Transform child in attachments)
                {
                    Destroy(child.gameObject);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void AddCommentCallback(ZendeskResponse<Comment> commentResponse)
        {
            try
            {
                attachmentButton.GetComponent<Button>().interactable = true;
                newCommentEndUser.GetComponent<InputField>().interactable = true;
                sendCommentButton.GetComponent<Button>().interactable = true;
                if (commentResponse.IsError)
                {
                    if (!zendeskErrorUI.IfAuthError(commentResponse.ErrorResponse, zendeskAuthType))
                    {
                        zendeskErrorUI.NavigateError(
                            zendeskLocalizationHandler.translationGameObjects["usdk_label_failed"]);
                    }

                    CheckAvailabilityOfSendButton(true);
                    return;
                }

                if (!prevDate.Date.Equals(DateTime.Today.Date))
                {
                    prevDate = DateTime.Today;
                    AddMessageDatePrefab();
                }

                ClearConversationalFooter();
                newCommentEndUser.GetComponent<InputField>().text = "";
                CheckAvailabilityOfSendButton(false);
                lastCommentDatetime = commentResponse.Result.CreatedAt.Value.AddSeconds(1);
                var g = Instantiate(endUserResponsePrefab, ticketResponseListView.transform);
                g.GetComponent<ZendeskTicketResponse>()
                 .Init(commentResponse.Result, this, zendeskMain.supportProvider, zendeskErrorUI,
                     zendeskLocalizationHandler);
                g.GetComponent<ZendeskTicketResponse>().MessageSentStatus(true);
                ticketResponseContainerList.Add(g);
                ScrollDownTicketResponse(true);
            }
            catch
            {
                zendeskErrorUI.NavigateError(null, true);
            }
        }

        public void AddAttachment()
        {
            try
            {
                #if UNITY_EDITOR
                string path = EditorUtility.OpenFilePanelWithFilters("", "",
                    new string[] {"All Files", "txt", "Image Files", "png,jpeg,jpg"});
                if (path.Length != 0)
                {
                    ImageSavedToTemporaryPath(path);
                }
                #elif UNITY_IOS || UNITY_ANDROID
                FileBrowser.OpenFileBrowser();
                #endif
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #endregion

        #region Show Panels

        private void RetrieveTicketResponseData()
        {
            try
            {
                zendeskMain.zendeskPauseHandler.PauseApplicationInternal();
                myTicketsPanel.SetActive(false);
                createRequestPanel.SetActive(false);
                ticketResponsePanel.SetActive(true);
                zendeskUI.ShowBackButton(backButtonContainer);
                headerTitle.text =
                    zendeskMain.zendeskLocalizationHandler.translationGameObjects["usdk_get_in_touch_label_conversation_header"];
                CheckAvailabilityOfSendButton(null);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void ShowMyTicketsWindow()
        {
            try
            {
                zendeskUI.AddScreenBackState(myTicketsPanel, ZendeskScreen.MyTickets);
                zendeskMain.zendeskPauseHandler.PauseApplicationInternal();
                myTicketsPanel.SetActive(true);
                createRequestPanel.SetActive(false);
                ticketResponsePanel.SetActive(false);
                zendeskUI.ShowBackButton(backButtonContainer);
                headerTitle.text =
                    zendeskLocalizationHandler.translationGameObjects["usdk_conversations_title"];
                if (isConversationsEnabled)
                {
                    LoadTickets();
                }
                else
                {
                    zendeskErrorUI.NavigateError(
                        zendeskLocalizationHandler.translationGameObjects["usdk_conversations_are_not_enabled"], true,
                        false, "hide");
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void SubmitNewRequest(string requestTags = "")
        {
            zendeskUI.FinishLoading();
            try
            {
                if (!zendeskCanvas.activeSelf)
                {
                    zendeskCanvas.SetActive(true);
                }
                if (zendeskMain.supportProvider != null)
                {
                    lastCommentDatetime = DateTime.MinValue;
                    prevDate = DateTime.Now;
                    zendeskMain.zendeskPauseHandler.PauseApplicationInternal();
                    gameObject.SetActive(true);
                    zendeskErrorUI.DestroyZendeskErrorToast();
                    myTicketsPanel.SetActive(false);
                    AddTags(requestTags);
                    if (isConversationsEnabled)
                    {
                        ticketId = null;
                        newCommentEndUser.GetComponent<InputField>().interactable = true;
                        ClearTicketResponse();
                        zendeskUI.AddScreenBackState(ticketResponsePanel, ZendeskScreen.TicketResponse, null);
                        ticketResponsePanel.SetActive(true);
                        createRequestPanel.SetActive(false);
                        ClearConversationalFooter();
                        CheckAvailabilityOfSendButton(null);
                        AddMessageDatePrefab();
                    }
                    else
                    {
                        RemoveAllAttachments();
                        createRequestScrollView.GetComponent<ScrollRect>().verticalNormalizedPosition = 1;
                        zendeskUI.AddScreenBackState(createRequestPanel, ZendeskScreen.CreateTicket, requestTags);
                        ticketResponsePanel.SetActive(false);
                        createRequestPanel.SetActive(true);
                        InitCreateRequestPanel();
                    }
                }
                else
                {
                    zendeskUI.AddScreenBackState(createRequestPanel, ZendeskScreen.CreateTicket);
                    throw new Exception();
                }

                headerTitle.text =
                    zendeskMain.zendeskLocalizationHandler.translationGameObjects["usdk_get_in_touch_label_conversation_header"];
                zendeskUI.ShowBackButton(backButtonContainer);
            }
            catch
            {
                zendeskErrorUI.NavigateError(null, true);
            }
        }

        public void RetrieveTicketResponseData(Request requestParam, bool clearLastCommentDatetime = false,
            bool refreshFooter = true)
        {
            try
            {
                zendeskMain.zendeskPauseHandler.PauseApplicationInternal();

                if (clearLastCommentDatetime)
                    lastCommentDatetime = DateTime.MinValue;
                if (requestParam != null)
                {
                    zendeskUI.StartLoading(headerPanel);
                    zendeskMain.supportProvider.GetRequest(
                        requestResponse => { RetrieveRequestCallback(requestResponse, refreshFooter); },
                        requestParam.Id);
                }
                else
                {
                    if (refreshFooter)
                    {
                        ClearConversationalFooter();
                    }

                    zendeskUI.AddScreenBackState(ticketResponsePanel, ZendeskScreen.TicketResponse, null);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void RetrieveRequestCallback(ZendeskResponse<RequestResponse> requestResponse, bool refreshFooter)
        {
            if (requestResponse.IsError)
            {
                if (!zendeskErrorUI.IfAuthError(requestResponse.ErrorResponse, zendeskAuthType))
                {
                    zendeskErrorUI.NavigateError(null, true);
                }

                zendeskUI.FinishLoading();
                return;
            }

            var requestParam = requestResponse.Result.Request;
            if (lastCommentDatetime == DateTime.MinValue)
            {
                if (zendeskUI.backStateGO.Count > 1 &&
                    zendeskUI.backStateGO[zendeskUI.backStateGO.Count - 1].ScreenGO == ticketResponsePanel)
                {
                    zendeskUI.backStateGO[zendeskUI.backStateGO.Count - 1].Parameter = requestParam.Id;
                }
                else
                {
                    zendeskUI.AddScreenBackState(ticketResponsePanel, ZendeskScreen.TicketResponse,
                        requestParam);
                }

                if (request == null || request.Id != requestParam.Id || request.Status != requestParam.Status)
                {
                    request = requestParam;
                }

                GetRequestComments(request.Id, refreshFooter);
            }
            else
            {
                zendeskUI.AddScreenBackState(ticketResponsePanel, ZendeskScreen.TicketResponse, requestParam);
                if (refreshFooter)
                {
                    ClearConversationalFooter();
                }

                RefreshComments(request.Id);
            }

            zendeskUI.FinishLoading();
        }

        public void Close()
        {
            try
            {
                lastCommentDatetime = DateTime.MinValue;
                zendeskUI.CloseButton();
                zendeskMain.zendeskPauseHandler.ResumeApplicationInternal();
            }
            catch
            {
                zendeskErrorUI.NavigateError(null, true);
            }
        }

        public void CloseSupportUI()
        {
            try
            {
                gameObject.SetActive(false);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #endregion

        #region Custom Fields

        private void ClearCustomFields()
        {
            try
            {
                foreach (var cf in customFieldsPrefabs)
                {
                    GameObject.Destroy(cf.Key);
                }

                customFieldsPrefabs.Clear();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void AddCustomFieldsUi()
        {
            try
            {
                ClearCustomFields();
                InitLocalizationHandlerCustomFields();
                foreach (var item in requestForm.Fields)
                {
                    if (CheckCustomFieldConfiguration(item))
                    {
                        var prefab = Instantiate(item.UIComponent, createRequestPanelFieldContainer.transform);
                        string labelKeyTranslated = labelKey.Replace("{cf-id}", item.Id.ToString());
                        string validationMessageKeyTranslated =
                            validationMessageKey.Replace("{cf-id}", item.Id.ToString());
                        string labelTranslated = this.GetTranslatedCustomField(labelKeyTranslated);
                        labelTranslated = string.IsNullOrEmpty(labelTranslated) ? item.Label : labelTranslated;
                        string validationMessageTranslated =
                            this.GetTranslatedCustomField(validationMessageKeyTranslated);
                        validationMessageTranslated = string.IsNullOrEmpty(validationMessageTranslated)
                            ? item.ValidationMessage
                            : validationMessageTranslated;
                        if (item.FieldType == FieldType.Text ||
                            item.FieldType == FieldType.Numeric ||
                            item.FieldType == FieldType.Decimal ||
                            item.FieldType == FieldType.Multiline)
                        {
                            var prefabScript = prefab.GetComponent<ZendeskCustomTextFieldScript>();
                            prefabScript.init(item.Id, labelTranslated, validationMessageTranslated, item.Placeholder,
                                item.Required,
                                zendeskLocalizationHandler.translationGameObjects["usdk_required_field_label"]);
                        }
                        else if (item.FieldType == FieldType.Checkbox)
                        {
                            var prefabScript = prefab.GetComponent<ZendeskCustomCheckboxFieldScript>();
                            prefabScript.init(item.Id, labelTranslated);
                        }

                        customFieldsPrefabs.Add(prefab, item.FieldType);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private bool CheckCustomFieldConfiguration(RequestFormField item)
        {
            try
            {
                if (item == null)
                {
                    Debug.Log(zendeskLocalizationHandler.translationGameObjects["usdk_custom_field_conf_success"]);
                    return false;
                }
                else
                {
                    if (item.Id == 0)
                    {
                        Debug.Log(zendeskLocalizationHandler.translationGameObjects["usdk_custom_field_conf_id_fail"]);
                        return false;
                    }

                    if (item.UIComponent == null)
                    {
                        Debug.Log(zendeskLocalizationHandler.translationGameObjects[
                            "usdk_custom_field_conf_ui_component_fail"]);
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return true;
        }

        public void LoadCustomFieldsFromCSV()
        {
            if (customFieldsFile == null)
            {
                Debug.Log(zendeskLocalizationHandler.translationGameObjects[
                    "usdk_custom_field_conf_csv_not_found_error"]);
                return;
            }

            List<RequestFormField> rfFields = new List<RequestFormField>();
            requestForm = new RequestForm();
            List<string> cf = new List<string>();
            string[] records = customFieldsFile.text.Split('\n');
            var isFirstLine = true;
            foreach (string record in records)
            {
                if (!String.IsNullOrEmpty(record))
                {
                    Regex CSVParser = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
                    string[] fields = CSVParser.Split(record);
                    if (isFirstLine)
                    {
                        foreach (var item in fields)
                        {
                            cf.Add(item);
                        }

                        isFirstLine = false;
                    }
                    else
                    {
                        int i = 0;
                        RequestFormField ff = new RequestFormField();
                        foreach (var field in fields)
                        {
                            if (cf[i].ToUpper() == "TITLE")
                            {
                                if (field.ToUpper().Equals("SUBJECT") ||
                                    field.ToUpper().Equals("DESCRIPTION") ||
                                    field.ToUpper().Equals("STATUS") ||
                                    field.ToUpper().Equals("TYPE") ||
                                    field.ToUpper().Equals("PRIORITY") ||
                                    field.ToUpper().Equals("GROUP") ||
                                    field.ToUpper().Equals("ASSIGNEE"))
                                {
                                    ff = null;
                                    break;
                                }

                                ff.Label = field;
                            }
                            else if (cf[i].ToUpper().StartsWith("FIELD ID"))
                            {
                                ff.Id = long.Parse(field);
                            }
                            else if (cf[i].ToUpper().StartsWith("REQUIRED FOR END USERS"))
                            {
                                ff.Required = bool.Parse(field);
                            }
                            else if (cf[i].ToUpper().Equals("TYPE"))
                            {
                                string fieldEnum = field;
                                if (fieldEnum.ToUpper().StartsWith("MULTI-LINE"))
                                {
                                    fieldEnum = "Multiline";
                                }
                                else if (fieldEnum.ToUpper().StartsWith("DROP-DOWN"))
                                {
                                    fieldEnum = "Dropdown";
                                }

                                try
                                {
                                    ff.FieldType = (FieldType) Enum.Parse(typeof(FieldType), fieldEnum, true);
                                }
                                catch
                                {
                                    ff = null;
                                    break;
                                }

                                if (ff.FieldType == FieldType.Text)
                                {
                                    ff.UIComponent = this.CustomTextFieldPrefab;
                                }
                                else if (ff.FieldType == FieldType.Multiline)
                                {
                                    ff.UIComponent = this.CustomTextMultilineFieldPrefab;
                                }

                                if (ff.FieldType == FieldType.Checkbox)
                                {
                                    ff.UIComponent = this.CustomCheckboxFieldPrefab;
                                }
                                else if (ff.FieldType == FieldType.Decimal)
                                {
                                    ff.UIComponent = this.CustomDecimalFieldPrefab;
                                }
                                else if (ff.FieldType == FieldType.Numeric)
                                {
                                    ff.UIComponent = this.CustomNumericFieldPrefab;
                                }
                            }
                            else if (cf[i].ToUpper() == "EDITABLE FOR END USERS")
                            {
                                if (bool.Parse(field))
                                {
                                    i++;
                                    continue;
                                }

                                ff = null;
                                break;
                            }

                            i++;
                        }

                        if (ff != null)
                        {
                            rfFields.Add(ff);
                        }
                    }
                }
            }

            requestForm.Fields = rfFields;
        }

        private List<CustomField> GetCustomFieldsValues()
        {
            List<CustomField> customFieldsResponse = new List<CustomField>();
            foreach (var item in customFieldsPrefabs)
            {
                if (item.Value == FieldType.Text ||
                    item.Value == FieldType.Numeric ||
                    item.Value == FieldType.Decimal ||
                    item.Value == FieldType.Multiline)
                {
                    var prefabScript = item.Key.GetComponent<ZendeskCustomTextFieldScript>();
                    customFieldsResponse.Add(new CustomField()
                    {
                        Id = prefabScript.idCustomField,
                        Value = prefabScript.inputField.text
                    });
                }
                else if (item.Value == FieldType.Checkbox)
                {
                    var prefabScript = item.Key.GetComponent<ZendeskCustomCheckboxFieldScript>();
                    customFieldsResponse.Add(new CustomField()
                    {
                        Id = prefabScript.idCustomField,
                        Value = prefabScript.inputField.isOn.ToString().ToLower()
                    });
                }
            }

            return customFieldsResponse;
        }

        /// <summary>
        /// Add/update new invisible custom fields dictionary to existing custom fields list
        /// </summary>
        /// <param name="customFieldsDictionary">Dictionary with custom field ID and value</param>
        public void SetInvisibleCustomFields(Dictionary<long, string> customFieldsDictionary)
        {
            try
            {
                if (customFields == null)
                {
                    customFields = new List<CustomField>();
                }

                foreach (var item in customFieldsDictionary)
                {
                    if (ValidateInvisibleCustomFieldItem(item.Key, item.Value))
                    {
                        if (customFields.Select(a => a.Id).Contains(item.Key))
                        {
                            customFields.FirstOrDefault(a => a.Id == item.Key).Value = item.Value;
                        }
                        else
                        {
                            customFields.Add(new CustomField()
                            {
                                Id = item.Key,
                                Value = item.Value
                            });
                        }
                    }
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// Add/update new invisible custom field to existing custom fields list
        /// </summary>
        /// <param name="id">Custom field ID</param>
        /// <param name="value">Custom field value</param>
        public void AddInvisibleCustomField(long id, string value)
        {
            try
            {
                if (customFields == null)
                {
                    customFields = new List<CustomField>();
                }

                if (ValidateInvisibleCustomFieldItem(id, value))
                {
                    if (customFields.Select(a => a.Id).Contains(id))
                    {
                        customFields.FirstOrDefault(a => a.Id == id).Value = value;
                    }
                    else
                    {
                        customFields.Add(new CustomField()
                        {
                            Id = id,
                            Value = value
                        });
                    }
                }
            }
            catch
            {
            }
        }

        private bool ValidateInvisibleCustomFieldItem(long id, string value)
        {
            bool isValid = true;
            if (id < 1)
            {
                Debug.Log(zendeskLocalizationHandler.translationGameObjects["usdk_custom_field_conf_id_fail"]);
                return false;
            }

            return isValid;
        }

        #endregion CustomFields

        public void BackToMyTicketsScreen()
        {
            try
            {
                lastCommentDatetime = DateTime.MinValue;
                prevDate = DateTime.MinValue;
                ShowMyTicketsWindow();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void AttachmentDeleteButtonClick(GameObject attachment)
        {
            if (attachmentPaths.Contains(attachment.GetComponent<ZendeskTicketAttachmentThumbnail>().GetPath()))
            {
                attachmentPaths = attachmentPaths
                                  .Where(val =>
                                      val != attachment.GetComponent<ZendeskTicketAttachmentThumbnail>().GetPath())
                                  .ToList();
            }

            Destroy(attachment);
            if (isConversationsEnabled)
            {
                this.ticketResponseAttachmentCountText.text = attachmentPaths.Count.ToString();
                if (attachmentPaths.Count == 0)
                {
                    this.ticketResponseAttachmentCountContainer.SetActive(false);
                    ticketResponseAttachmentCountText.text = String.Empty;
                    this.createRequestAttachmentLine.SetActive(false);
                    CheckAvailabilityOfSendButton(null);
                }
            }
            else
            {
                this.createRequestAttachmentCountText.text = attachmentPaths.Count.ToString();
                if (attachmentPaths.Count == 0)
                {
                    this.createRequestAttachmentCountContainer.SetActive(false);
                    this.createRequestAttachmentCountText.text = string.Empty;
                    this.createRequestPanelAttachmentLine.SetActive(false);
                }
            }
        }

        public void ImageSavedToTemporaryPath(string path)
        {
            GameObject g = null;
            try
            {
                if (!string.IsNullOrEmpty(path))
                {
                    if (path.Equals("error"))
                    {
                        #if UNITY_ANDROID
                        if (ZendeskLocalStorage.IsRequestDeniedForAndroidBefore())
                        {
                            zendeskErrorUI.NavigateError(
                                zendeskLocalizationHandler.translationGameObjects["usdk_file_permission_denied_generic"]);
                        }
                        else
                        {
                            zendeskErrorUI.NavigateError(
                                zendeskLocalizationHandler.translationGameObjects["usdk_first_time_permission_deny"]);
                        }
                        #else
                        zendeskErrorUI.NavigateError(
                            zendeskLocalizationHandler.translationGameObjects["usdk_file_permission_denied_generic"]);
                        #endif
                        return;
                    }

                    if (path.Equals("error_res"))
                    {
                        zendeskErrorUI.NavigateError(
                            zendeskLocalizationHandler.translationGameObjects["usdk_file_exceeds_max_resolution"]);
                        return;
                    }

                    if (attachmentPaths.Count == 0 || !attachmentPaths.Contains(path))
                    {
                        if (isConversationsEnabled)
                        {
                            g = Instantiate(attachmentPrefab, attachmentsView.transform);
                            if (string.IsNullOrEmpty(ticketId))
                            {
                                CheckAvailabilityOfSendButton(null);
                            }
                            else
                            {
                                CheckAvailabilityOfSendButton(true);
                            }
                        }
                        else
                        {
                            g = Instantiate(attachmentPrefab, attachmentsViewNonConversational.transform);
                        }

                        g.transform.SetAsFirstSibling();
                        g.GetComponent<ZendeskTicketAttachmentThumbnail>()
                         .Init(path, maxAttachmentSize, this,
                             zendeskLocalizationHandler.translationGameObjects["usdk_file_exceeds_max_size_20_mb"],
                             zendeskLocalizationHandler.translationGameObjects["usdk_file_exceeds_max_resolution"]);
                        attachmentPaths.Add(path);
                        if (isConversationsEnabled)
                        {
                            this.ticketResponseAttachmentCountText.text = attachmentPaths.Count.ToString();
                        }
                        else
                        {
                            this.createRequestAttachmentCountText.text = attachmentPaths.Count.ToString();
                        }
                    }
                    else
                    {
                        zendeskErrorUI.NavigateError(
                            zendeskLocalizationHandler.translationGameObjects["usdk_attachment_is_already_selected"]);
                        return;
                    }

                    if (attachmentPaths.Count == 1)
                    {
                        this.createRequestAttachmentLine.SetActive(true);
                        if (isConversationsEnabled)
                        {
                            this.ticketResponseAttachmentCountContainer.SetActive(true);
                            this.createRequestAttachmentLine.SetActive(true);
                        }
                        else
                        {
                            this.createRequestAttachmentCountContainer.SetActive(true);
                            this.createRequestPanelAttachmentLine.SetActive(true);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                if (g != null)
                {
                    Destroy(g);
                }

                zendeskErrorUI.NavigateError(e.Message);
                CheckAvailabilityOfSendButton(null);
            }
        }

        private void AddMessageDatePrefab()
        {
            var g = Instantiate(messageDatePrefab, ticketResponseListView.transform);
            if (prevDate.Date.Equals(DateTime.Today.Date))
            {
                g.GetComponentInChildren<Text>().text = zendeskMain
                                                        .zendeskLocalizationHandler
                                                        .translationGameObjects["usdk_today"];
            }
            else if (prevDate.Date.Equals(DateTime.Today.AddDays(-1).Date))
            {
                g.GetComponentInChildren<Text>().text = zendeskMain
                                                        .zendeskLocalizationHandler.translationGameObjects
                                                            ["usdk_yesterday"];
            }
            else
            {
                g.GetComponentInChildren<Text>().text =
                    prevDate.Day + " " + prevDate.ToString("MMMM",
                        System.Globalization.CultureInfo.CreateSpecificCulture(zendeskMain
                                                                               .zendeskLocalizationHandler
                                                                               .Locale));
                if (prevDate.Date.Year != DateTime.Today.Year)
                {
                    g.GetComponentInChildren<Text>().text += " " + prevDate.Date.Year;
                }
            }

            ticketResponseContainerList.Add(g);
        }
    }
}
