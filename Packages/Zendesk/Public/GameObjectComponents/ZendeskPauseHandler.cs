﻿using System;
using UnityEngine;

namespace Zendesk.UI
{
    public class ZendeskPauseHandler : MonoBehaviour
    {
        private static ZendeskMain zendeskMain;
        
        public void init(ZendeskMain main)
        {
            zendeskMain = main;
        }
        
        public void PauseApplication()
        {
            if (zendeskMain.autoPause)
            {
                Time.timeScale = 0;
            }
        }
        
        public void ResumeApplication()
        {
            if (zendeskMain.autoPause)
            {
                Time.timeScale = 1;
            }
        }
        
        internal void PauseApplicationInternal()
        {
            try
            {
                if (zendeskMain.autoPause)
                {
                    zendeskMain.pauseFunctionality.Invoke();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        
        internal void ResumeApplicationInternal()
        {
            try
            {
                if (zendeskMain.autoPause)
                {
                    zendeskMain.resumeFunctionality.Invoke();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}