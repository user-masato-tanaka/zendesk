using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Zendesk.UI
{
    [RequireComponent(typeof(Canvas))]
    public class CanvasHelper : MonoBehaviour
    {
        private static List<CanvasHelper> helpers = new List<CanvasHelper>();

        public static UnityEvent OnResolutionOrOrientationChanged = new UnityEvent();

        private static bool screenChangeVarsInitialized = false;
        private static ScreenOrientation lastOrientation = ScreenOrientation.Landscape;
        private static Vector2 lastResolution = Vector2.zero;
        private static Rect lastSafeArea = Rect.zero;

        private Canvas canvas;
        private RectTransform rectTransform;
        private RectTransform safeAreaTransform;

        void Awake()
        {
            if (!helpers.Contains(this))
                helpers.Add(this);
            canvas = GetComponent<Canvas>();
            rectTransform = GetComponent<RectTransform>();
            safeAreaTransform = transform.Find("SafeArea") as RectTransform;
            SetCanvasScalerMatchValue();
            OnResolutionOrOrientationChanged.AddListener(SetCanvasScalerMatchValue);
            if (!screenChangeVarsInitialized)
            {
                lastOrientation = UnityEngine.Screen.orientation;
                lastResolution.x = UnityEngine.Screen.width;
                lastResolution.y = UnityEngine.Screen.height;
                lastSafeArea = UnityEngine.Screen.safeArea;
                screenChangeVarsInitialized = true;
            }

            ApplySafeArea();
        }

        void Update()
        {
            if (helpers[0] != this)
                return;
            if (Application.isMobilePlatform && UnityEngine.Screen.orientation != lastOrientation)
                OrientationChanged();
            if (UnityEngine.Screen.safeArea != lastSafeArea)
                SafeAreaChanged();
            if (UnityEngine.Screen.width != lastResolution.x || UnityEngine.Screen.height != lastResolution.y)
                ResolutionChanged();
        }

        private void SetCanvasScalerMatchValue()
        {
            switch (UnityEngine.Screen.orientation)
            {
                case ScreenOrientation.Portrait:
                case ScreenOrientation.PortraitUpsideDown:
                    this.transform.parent.gameObject.GetComponentsInChildren<CanvasScaler>(true).ToList()
                        .ForEach(scaler => scaler.matchWidthOrHeight = 1.0f);
                    break;
                case ScreenOrientation.LandscapeLeft:
                case ScreenOrientation.LandscapeRight:
                    this.transform.parent.gameObject.GetComponentsInChildren<CanvasScaler>(true).ToList()
                        .ForEach(scaler => scaler.matchWidthOrHeight = 0.5f);
                    break;
            }
        }

        void ApplySafeArea()
        {
            if (safeAreaTransform == null)
                return;
            var safeArea = UnityEngine.Screen.safeArea;
            var anchorMin = safeArea.position;
            var anchorMax = safeArea.position + safeArea.size;
            anchorMin.x /= canvas.pixelRect.width;
            anchorMin.y /= canvas.pixelRect.height;
            anchorMax.x /= canvas.pixelRect.width;
            anchorMax.y /= canvas.pixelRect.height;
            safeAreaTransform.anchorMin = anchorMin;
            safeAreaTransform.anchorMax = anchorMax;
        }

        void OnDestroy()
        {
            OnResolutionOrOrientationChanged.RemoveListener(SetCanvasScalerMatchValue);
            if (helpers != null && helpers.Contains(this))
                helpers.Remove(this);
        }

        private static void OrientationChanged()
        {
            lastOrientation = UnityEngine.Screen.orientation;
            lastResolution.x = UnityEngine.Screen.width;
            lastResolution.y = UnityEngine.Screen.height;
            OnResolutionOrOrientationChanged.Invoke();
        }

        private static void ResolutionChanged()
        {
            lastResolution.x = UnityEngine.Screen.width;
            lastResolution.y = UnityEngine.Screen.height;
            OnResolutionOrOrientationChanged.Invoke();
        }

        private static void SafeAreaChanged()
        {
            lastSafeArea = UnityEngine.Screen.safeArea;
            for (int i = 0; i < helpers.Count; i++)
            {
                helpers[i].ApplySafeArea();
            }
        }
    }
}
