﻿using System;
using System.Globalization;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Zendesk.Common;
using Zendesk.Internal.Models.Common;
using Zendesk.Internal.Models.Core;
using Zendesk.Internal.Models.Support;
using Zendesk.Providers;

namespace Zendesk.UI
{
    public class ZendeskTicketContentOverview : MonoBehaviour
    {
        public GameObject agentImageGO;
        public GameObject closedImage;
        public GameObject pictureImage;
        private int commentCount;
        public GameObject lastCommentGO;
        private Text lastCommentText;
        public GameObject lastUpdateGO;
        private Text lastUpdateText;
        public GameObject newMessagesCountGO;
        private Text newMessagesCountText;
        public GameObject participantsGO;
        private Text participantsText;
        private Request request;
        private bool showNewMessagesCount = false;
        public GameObject subjectGO;
        public Font unreadFont;
        public GameObject unreadIcon;
        private ZendeskErrorUI zendeskErrorUI;
        private ZendeskSupportProvider zendeskSupportProvider;
        private ZendeskSupportUI ZendeskSupportUI;
        private ZendeskLocalizationHandler zendeskLocalizationHandler;
        public Text closedText;
        private void Start()
        {
            participantsText = participantsGO.GetComponent<Text>();
            lastCommentText = lastCommentGO.GetComponent<Text>();
            lastUpdateText = lastUpdateGO.GetComponent<Text>();
        }

        public void Init(Request r, ZendeskSupportProvider zendeskSupportProvider, ZendeskSupportUI zendeskSupportUI,
            ZendeskErrorUI zendeskErrorUI, ZendeskHtmlHandler zendeskHtmlHandler, ZendeskLocalizationHandler zendeskLocalizationHandler)
        {
            try
            {
                this.zendeskLocalizationHandler = zendeskLocalizationHandler;
                this.zendeskErrorUI = zendeskErrorUI;
                participantsText = participantsGO.GetComponent<Text>();
                this.lastCommentText = lastCommentGO.GetComponent<Text>();
                lastUpdateText = lastUpdateGO.GetComponent<Text>();
                ZendeskSupportUI = zendeskSupportUI;
                commentCount = r.CommentCount;
                this.closedText.text = zendeskLocalizationHandler.translationGameObjects["usdk_request_status_closed"]; 
                request = r;
                var participantsString = "";
                if (r.LastCommentingAgents != null)
                {
                    var lastAgent = r.LastCommentingAgents.FirstOrDefault();
                    if (r.LastCommentingAgents.Count > 0 && lastAgent != null && lastAgent.Photo != null &&
                        lastAgent.Photo.ContentUrl != null)
                    {
                        if (zendeskSupportUI.ticketContentImageIds.ContainsKey(lastAgent.Photo.Id))
                        {
                            SetTextureToAgentImageGO(zendeskSupportUI.ticketContentImageIds[lastAgent.Photo.Id]);
                        }
                        else
                        {
                            zendeskSupportProvider.GetImage(textureResponse => { GetImageOfAgentCallback(textureResponse, lastAgent.Photo.Id); },
                                lastAgent.Photo);
                        }
                    }
                    else if (r.LastCommentingAgents.Count > 0 && lastAgent != null &&
                             (lastAgent.Photo == null || lastAgent.Photo.ContentUrl == null))
                    {
                    }

                    foreach (var u in r.LastCommentingAgents)
                    {
                        var userNameSplitted = u.Name.Split(' ');
                        if (!string.IsNullOrEmpty(u.Name) && userNameSplitted.Length >= 1)
                        {
                            string firstName = userNameSplitted[0];
                            if (firstName.Length >= 24)
                            {
                                firstName = firstName.Substring(0, 23);
                            }
                            participantsString += string.Format("{0}", firstName);
                        }
                        else
                        {
                            participantsString += string.Format(", {0}", u.Name);
                        }

                        participantsString += ", ";
                        break;
                    }
                }

                participantsString += zendeskLocalizationHandler.translationGameObjects["usdk_me"];
                var lastCommentText = zendeskHtmlHandler.ClearHtmlText(r.LastComment.PlainBody);
                participantsText.text = zendeskHtmlHandler.ClearHtmlText(participantsString);
                this.lastCommentText.text = lastCommentText;
                lastUpdateText.text = UpdatedAtDateFormat(r.PublicUpdatedAt);
                var localRequest = ZendeskLocalStorage.GetSupportRequest(r.Id);
                if (localRequest == null)
                {
                    ZendeskLocalStorage.SaveSupportRequest(r, false);
                    localRequest = ZendeskLocalStorage.GetSupportRequest(r.Id);
                }
                
                if (localRequest.newComments > 0 || commentCount > localRequest.commentCount)
                {
                    ZendeskLocalStorage.SaveSupportRequest(r, false);
                    unreadIcon.SetActive(true);
                    participantsText.GetComponent<Text>().font = unreadFont;
                    this.lastCommentText.GetComponent<Text>().color = new Color32(47, 57, 65, 255);
                    this.lastCommentText.GetComponent<Text>().font = unreadFont;
                }
                
                if (lastCommentText.StartsWith("[") && lastCommentText.EndsWith("]"))
                {
                    pictureImage.SetActive(true);
                    this.lastCommentText.text = zendeskLocalizationHandler.translationGameObjects["usdk_image"];
                }
                
                if (r.StatusEnum == RequestStatus.Closed)
                {
                    closedImage.SetActive(true);
                    lastCommentGO.SetActive(false);
                    pictureImage.SetActive(false);
                }

                gameObject.SetActive(true);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        
        private string UpdatedAtDateFormat(string date)
        {
            try
            {
                return ZendeskUtils.SetZendeskShortDateFormat(date, zendeskLocalizationHandler.Locale);
            }
            catch
            {
                
            }

            return String.Empty;
        }

        public void OpenFullTicket()
        {
            try
            {
                zendeskErrorUI.DestroyZendeskErrorToast();
                if (ZendeskSupportUI == null)
                {
                    ZendeskSupportUI = GetComponentInParent<ZendeskSupportUI>();
                }
                if (request != null)
                {
                    ZendeskSupportUI.RetrieveTicketResponseData(request, true);
                }
            }
            catch
            {
                zendeskErrorUI.NavigateError(null,true);
            }
        }

        private void GetImageOfAgentCallback(ZendeskResponse<Texture2D> textureResponse, long id)
        {
            try
            {
                if (textureResponse.IsError)
                {
                    zendeskErrorUI.IfAuthError(textureResponse.ErrorResponse, ZendeskSupportUI.zendeskAuthType);
                        return;
                }
                var texture = textureResponse.Result;
                if (texture != null)
                {
                    if (!ZendeskSupportUI.ticketContentImageIds.ContainsKey(id))
                    {
                        ZendeskSupportUI.ticketContentImageIds.Add(id,texture);
                    }
                    SetTextureToAgentImageGO(texture);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void SetTextureToAgentImageGO(Texture2D texture)
        {
            agentImageGO.GetComponent<RawImage>().texture = texture;
            agentImageGO.GetComponent<RawImage>().color = Color.white;
            ((RectTransform)agentImageGO.transform).sizeDelta = new Vector2 (120, 120);
        }
    }
}