﻿using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace Zendesk.UI
{
    public class ZendeskTicketAttachmentThumbnail : MonoBehaviour
    {
        private string Path { get; set; }
        public GameObject agentContentGO;
        public GameObject agentDeleteButtonGO;

        private ZendeskSupportUI zendeskSupportUI;
        private string maxSizeErrorMessage;
        private string maxResolutionErrorMessage;
        
        public void Init(string path, long maxAttachmentSize,
            ZendeskSupportUI zendeskSupportUI, string maxSizeErrorMessage, string maxResolutionErrorMessage)
        {
            try
            {
                this.Path = path;
                this.maxSizeErrorMessage = maxSizeErrorMessage;
                this.maxResolutionErrorMessage = maxResolutionErrorMessage;
                gameObject.SetActive(true);
                var texture = LoadImageFromPath(maxAttachmentSize, 4096);
                this.agentContentGO.GetComponent<RawImage>().texture = texture;
                this.agentContentGO.GetComponent<RectTransform>().sizeDelta = new Vector2(texture.width,texture.height);
                this.agentDeleteButtonGO.GetComponent<Button>().onClick.AddListener(delegate
                    {
                        zendeskSupportUI.AttachmentDeleteButtonClick(this.gameObject);
                    });
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public string GetPath()
        {
            return Path;
        }

        public Texture2D LoadImageFromPath(long maxAttachmentSize, int maxRes)
        {
            Texture2D tex = new Texture2D(1, 1, TextureFormat.ARGB32, true);
            byte[] fileData;
            if (File.Exists(Path))
            {
                fileData = File.ReadAllBytes(Path);
                if (fileData.Length <= 20971520)
                {
                    tex = new Texture2D(2, 2);
                    tex.LoadImage(fileData);
                    if (tex.height > maxRes || tex.width > maxRes)
                    {
                        throw new Exception(maxResolutionErrorMessage);
                    }
                }
                else
                {
                    throw new Exception(maxSizeErrorMessage);
                }
            }
            return tex;
        }
    }
}