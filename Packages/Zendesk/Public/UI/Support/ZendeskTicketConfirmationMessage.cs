﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Zendesk.UI
{
    public class ZendeskTicketConfirmationMessage : MonoBehaviour
    {
        public GameObject confirmationMessageGO;
        private Text confirmationMessageText;
        
        public void Init(string answerBotMessage)
        {
            try
            {
                confirmationMessageText = confirmationMessageGO.GetComponent<Text>();
                confirmationMessageText.text = answerBotMessage;
                gameObject.SetActive(true);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}