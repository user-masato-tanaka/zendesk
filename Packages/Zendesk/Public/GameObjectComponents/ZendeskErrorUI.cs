﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zendesk.Internal.Models.Common;
using Zendesk.Internal.Models.Core;

namespace Zendesk.UI
{
    public class ZendeskErrorUI : MonoBehaviour
    {
        public ZendeskErrorEvent zendeskErrorEvent;
        public GameObject zendeskErrorToastUIGO;
        public GameObject zendeskErrorUIGO;
        public GameObject zendeskFullScreenErrorTitleContainer;
        public GameObject zendeskFullScreenErrorBackButtonContainer;
        public GameObject zendeskFullScreenErrorReloadButtonContainer;
        public GameObject zendeskFullScreenErrorExitButtonContainer;
        public Text zendeskErrorFullScreenTitleText;
        public Text zendeskErrorFullScreenDescriptionText;
        public Text zendeskErrorFullScreenButtonText;
        public Text zendeskErrorFullScreenExitButtonText;
        [HideInInspector]
        public ZendeskUI zendeskUI;
        private static List<GameObject> errorToasts = new List<GameObject>();
        private ZendeskLocalizationHandler zendeskLocalizationHandler;
        private string authScreenErrorTitle;
        private string authScreenErrorBody;
        private string jwtIntegratorError;

        public void Init(ZendeskLocalizationHandler localizationHandler)
        {
            this.zendeskLocalizationHandler = localizationHandler;
            SetStrings();
        }

        private void SetStrings() 
        {
            this.zendeskErrorFullScreenTitleText.text = zendeskLocalizationHandler.translationGameObjects["usdk_generic_error_message_title"];
            this.zendeskErrorFullScreenDescriptionText.text = zendeskLocalizationHandler.translationGameObjects["usdk_generic_error_message_body_content"];
            this.zendeskErrorFullScreenButtonText.text = zendeskLocalizationHandler.translationGameObjects["usdk_generic_error_button"];
            this.zendeskErrorFullScreenExitButtonText.text =
                zendeskLocalizationHandler.translationGameObjects["usdk_back_button_label"];
            authScreenErrorTitle = zendeskLocalizationHandler.translationGameObjects["usdk_jwt_error_message"];
            authScreenErrorBody = zendeskLocalizationHandler.translationGameObjects["usdk_error_page_message_new_design"];
            jwtIntegratorError = zendeskLocalizationHandler.translationGameObjects["usdk_jwt_token_integrator_error"];
        }
        public void NavigateError(string content,bool? isFullScreen = false, bool? isButtonReloads = true, string fullScreenErrorTitle = null)
        {
            zendeskErrorEvent.Invoke(content,isFullScreen.Value,isButtonReloads.Value,fullScreenErrorTitle);
        }

        public void NavigateError<T>(ZendeskResponse<T> response, bool? isFullScreen = false, bool? isButtonReloads = true, string fullScreenErrorTitle = null)
        {
            if (response.IsError)
            {
                if (response.ErrorResponse != null && response.ErrorResponse.Reason != null)
                {
                    zendeskErrorEvent.Invoke(response.ErrorResponse.Reason, isFullScreen.Value, isButtonReloads.Value, fullScreenErrorTitle);
                }
            }
            else
            {
                zendeskErrorEvent.Invoke("", isFullScreen.Value,isButtonReloads.Value,fullScreenErrorTitle);
            }
        }
        
        public void ZendeskError(string content, bool isFullScreen, bool isButtonReloads, string fullScreenErrorTitle = null)
        {
            if (isFullScreen)
            {
                SetStrings();  
                if (!string.IsNullOrEmpty(content))
                {
                    this.zendeskErrorFullScreenDescriptionText.text = content;
                }
                if (fullScreenErrorTitle != null)
                {
                    if (fullScreenErrorTitle.Trim().Equals("hide"))
                    {
                        zendeskFullScreenErrorTitleContainer.SetActive(false);   
                    }
                    else
                    {
                        zendeskFullScreenErrorTitleContainer.SetActive(true);
                        zendeskErrorFullScreenTitleText.text = fullScreenErrorTitle;
                    }
                }
                else
                {
                    zendeskFullScreenErrorTitleContainer.SetActive(true);
                }
                this.zendeskErrorUIGO.SetActive(true);
                if (!isButtonReloads)
                {
                    zendeskFullScreenErrorReloadButtonContainer.SetActive(false);
                    zendeskFullScreenErrorExitButtonContainer.SetActive(true);
                    zendeskFullScreenErrorBackButtonContainer.SetActive(false);
                }
                else
                {
                    zendeskFullScreenErrorReloadButtonContainer.SetActive(true);
                    zendeskFullScreenErrorExitButtonContainer.SetActive(false);
                    zendeskUI.ShowBackButton(zendeskFullScreenErrorBackButtonContainer);
                }
            }
            else
            {
                ShowToast(content);
            }
        }

        private void ShowToast(string content)
        {
            if (zendeskUI.backStateGO.Count > 0)
            {
                var screenGO = zendeskUI.backStateGO[zendeskUI.backStateGO.Count - 1].ScreenGO;
                var g = Instantiate(zendeskErrorToastUIGO, screenGO.transform);
                errorToasts.Add(g);
                g.SetActive(true);
                g.transform.GetComponentInChildren<Text>().text = string.Format(content);
            }
        }

        public static void AfterDestroyToast(GameObject g)
        {
            if (errorToasts.Contains(g))
            {
                errorToasts.Remove(g);
            }
        }

        public void DestroyZendeskErrorToast()
        {
            try
            {
                foreach (var toast in errorToasts)
                {
                    Destroy(toast);
                }
                errorToasts.Clear();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        
        public bool IfAuthError(ErrorResponse zendeskErrorResponse, ZendeskAuthType zendeskAuthType)
        {
            if (zendeskErrorResponse != null && zendeskErrorResponse.IsAuthError)
            {
                if (zendeskAuthType.Value == ZendeskAuthType.Jwt.Value)
                {
                    Debug.Log(jwtIntegratorError);
                }
                NavigateError(authScreenErrorBody, true, false, authScreenErrorTitle);
                return true;
            }

            return false;
        }
    }
}