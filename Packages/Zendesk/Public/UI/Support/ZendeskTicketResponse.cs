﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using Zendesk.Internal.Models.Common;
using Zendesk.Internal.Models.Core;
using Zendesk.Internal.Models.Support;
using Zendesk.Providers;

namespace Zendesk.UI
{
    public class ZendeskTicketResponse : MonoBehaviour
    {
        public GameObject agentNameContainerGO;
        public GameObject agentNameGO;
        public GameObject agentPhotoGO;
        public GameObject agentResponseGO;
        public GameObject imageResponseGO;
        private Text agentResponseText;
        public GameObject endUserResponseGO;
        private Text endUserResponseText;
        public GameObject endUserMessagePanelGO;
        private Text responseOwnerNameText;
        public GameObject agentPhotoContainerGO;
        public Texture placeholderLandscape;
        public Texture placeholderPortrait;
        private bool isAgent = false;
        private ZendeskSupportProvider supportProvider;
        private ZendeskErrorUI zendeskErrorUI;
        private ZendeskSupportUI zendeskSupportUI;
        private ZendeskHtmlHandler zendeskHtmlHandler;
        private ZendeskMain zendeskMain;
        private ZendeskLocalizationHandler zendeskLocalizationHandler;

        private List<string> supportedImageExtensions = new List<string>()
        {
            "image/jpeg",
            "image/png"
        };

        private string requestId;

        public void Init(CommentResponse comment, ZendeskSupportProvider zendeskSupportProvider,
            ZendeskSupportUI zendeskSupportUI, ZendeskErrorUI zendeskErrorUI, User author,
            bool isPreviousAuthor, Attachment agentPhotoAttachment, Attachment attachment,
            ZendeskHtmlHandler zendeskHtmlHandler, ZendeskLocalizationHandler zendeskLocalizationHandler)
        {
            try
            {
                this.zendeskMain = zendeskHtmlHandler.GetComponent<ZendeskMain>();
                this.zendeskSupportUI = zendeskSupportUI;
                this.zendeskLocalizationHandler = zendeskLocalizationHandler;
                this.zendeskHtmlHandler = zendeskHtmlHandler;
                this.zendeskErrorUI = zendeskErrorUI;
                supportProvider = zendeskSupportProvider;
                if (comment != null)
                {
                    requestId = comment.RequestId;
                    if (comment.Agent)
                    {
                        string agentResponseToLoad =
                            zendeskHtmlHandler.HandleHtmlComponents(
                                this.zendeskHtmlHandler.ParseHtmlRequest(comment.HtmlBody));
                        agentResponseGO.GetComponent<ZendeskTextPic>()
                                       .init(agentResponseToLoad, this.zendeskMain.zendeskLinkHander);
                        isAgent = comment.Agent;
                        if (isPreviousAuthor)
                        {
                            agentNameContainerGO.gameObject.SetActive(false);
                        }
                        else
                        {
                            responseOwnerNameText = agentNameGO.GetComponent<Text>();
                            var authorNameWithoutFullSurname = author.Name.Split(' ');
                            if (author.Name != null && authorNameWithoutFullSurname.Length >= 1)
                            {
                                string firstName = authorNameWithoutFullSurname[0];
                                if (firstName.Length >= 36)
                                {
                                    firstName = firstName.Substring(0, 35);
                                }

                                author.Name = string.Format("{0}", firstName);
                            }

                            responseOwnerNameText.text = author.Name;
                        }

                        if (agentPhotoAttachment != null && agentPhotoAttachment.ContentUrl != null)
                        {
                            if (zendeskSupportUI.ticketContentImageIds.ContainsKey(agentPhotoAttachment.Id))
                            {
                                SetAgentImage(zendeskSupportUI.ticketContentImageIds[agentPhotoAttachment.Id]);
                            }
                            else
                            {
                                zendeskSupportProvider.GetImage(
                                    textureResponse =>
                                    {
                                        GetImageOfAgentCallback(textureResponse, agentPhotoAttachment.Id);
                                    },
                                    agentPhotoAttachment);
                            }
                        }
                        else if (agentPhotoAttachment == null || agentPhotoAttachment.ContentUrl == null)
                        {
                        }
                    }
                    else
                    {
                        endUserResponseText = endUserResponseGO.GetComponent<Text>();
                        SetCommentBody(comment.Body);
                    }

                    AddAttachments(comment.Attachments);
                }

                gameObject.SetActive(true);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void Init(Comment comment, ZendeskSupportUI zendeskSupportUI,
            ZendeskSupportProvider zendeskSupportProvider, ZendeskErrorUI zendeskErrorUI,
            ZendeskLocalizationHandler zendeskLocalizationHandler)
        {
            try
            {
                this.zendeskErrorUI = zendeskErrorUI;
                this.zendeskLocalizationHandler = zendeskLocalizationHandler;
                this.zendeskSupportUI = zendeskSupportUI;
                supportProvider = zendeskSupportProvider;
                requestId = comment.RequestId;
                endUserResponseText = endUserResponseGO.GetComponent<Text>();
                SetCommentBody(comment.Body);
                AddAttachments(comment.Attachments);
                gameObject.SetActive(true);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void Init(string comment, ZendeskSupportUI zendeskSupportUI, ZendeskErrorUI zendeskErrorUI,
            ZendeskLocalizationHandler zendeskLocalizationHandler)
        {
            try
            {
                this.zendeskErrorUI = zendeskErrorUI;
                this.zendeskLocalizationHandler = zendeskLocalizationHandler;
                this.zendeskSupportUI = zendeskSupportUI;
                endUserResponseText = endUserResponseGO.GetComponent<Text>();
                endUserResponseText.text = comment;
                gameObject.SetActive(true);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void SetCommentBody(string body)
        {
            try
            {
                if (body.StartsWith("[") && body.EndsWith("]"))
                {
                    if (!isAgent)
                    {
                        endUserMessagePanelGO.SetActive(false);
                        endUserResponseText.transform.parent.gameObject.SetActive(false);
                    }
                    else
                    {
                        agentResponseGO.transform.parent.gameObject.SetActive(false);
                    }
                }
                else
                {
                    endUserResponseText.text = body;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void AddAttachments(List<Attachment> attachments)
        {
            try
            {
                if (attachments != null && attachments.Count > 0)
                {
                    if (zendeskSupportUI.ticketId != requestId)
                    {
                        return;
                    }

                    GameObject newImageObject;
                    for (int i = 1; i <= attachments.Count; i++)
                    {
                        if (!isContentTypeSupported(attachments[i - 1]))
                        {
                            return;
                        }

                        int properWidth = 0;
                        int properHeight = 0;
                        ParseImageDimensionsToSDK((int) attachments[i - 1].Height, (int) attachments[i - 1].Width,
                            out properHeight, out properWidth);
                        imageResponseGO.transform.GetChild(0).GetComponent<RawImage>().texture =
                            GetPlaceholderTexture((int) attachments[i - 1].Height, (int) attachments[i - 1].Width);
                        imageResponseGO.transform.GetChild(0).GetComponent<LayoutElement>().preferredHeight =
                            properHeight;
                        imageResponseGO.transform.GetChild(0).GetComponent<LayoutElement>().preferredWidth =
                            properWidth;
                        if (isAgent)
                        {
                            newImageObject = Instantiate(imageResponseGO,
                                agentResponseGO.transform.parent.parent.transform);
                        }
                        else
                        {
                            newImageObject = Instantiate(imageResponseGO,
                                endUserResponseGO.transform.parent.parent.parent);
                            newImageObject.transform.SetAsLastSibling();
                        }

                        newImageObject.name += "_" + attachments[i - 1].Id;
                    }

                    foreach (var att in attachments)
                    {
                        if (IsImageResolutionAcceptable((int) att.Height,
                            (int) att.Width))
                        {
                            supportProvider.GetImage(
                                textureResponse => { GetAttachmentCallback(textureResponse, att); },
                                att);
                        }
                        else
                        {
                            zendeskErrorUI.NavigateError(
                                zendeskLocalizationHandler.translationGameObjects["usdk_file_exceeds_max_resolution"]);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void GetImageOfAgentCallback(ZendeskResponse<Texture2D> textureResponse, long id)
        {
            try
            {
                if (textureResponse.IsError)
                {
                    zendeskErrorUI.IfAuthError(textureResponse.ErrorResponse, zendeskSupportUI.zendeskAuthType);
                    return;
                }

                if (!textureResponse.IsError && textureResponse.Result != null)
                {
                    if (!zendeskSupportUI.ticketContentImageIds.ContainsKey(id))
                    {
                        zendeskSupportUI.ticketContentImageIds.Add(id, textureResponse.Result);
                    }

                    SetAgentImage(textureResponse.Result);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private bool isContentTypeSupported(Attachment attachment)
        {
            if (attachment != null && !string.IsNullOrEmpty(attachment.ContentType))
            {
                if (supportedImageExtensions.Contains(attachment.ContentType.ToLower()))
                {
                    return true;
                }
            }

            return false;
        }

        private void GetAttachmentCallback(ZendeskResponse<Texture2D> textureResponse, Attachment attachment)
        {
            try
            {
                if (zendeskSupportUI.ticketId != requestId)
                {
                    return;
                }

                Texture texture;
                if (textureResponse.IsError)
                {
                    if (!zendeskErrorUI.IfAuthError(textureResponse.ErrorResponse, zendeskSupportUI.zendeskAuthType))
                    {
                        zendeskErrorUI.NavigateError(
                            zendeskLocalizationHandler.translationGameObjects["usdk_fail_fetch_attachment"]);
                    }
                }

                texture = textureResponse.Result;
                if (texture != null)
                {
                    Transform firstChild;
                    float kMaxResolution;
                    if (!isAgent)
                    {
                        firstChild = endUserResponseGO
                                     .transform.parent.parent.parent
                                     .Find(imageResponseGO.name + "(Clone)_" + attachment.Id).transform;
                        kMaxResolution = endUserResponseGO
                                         .transform.parent.parent.parent.GetComponent<RectTransform>().rect.width -
                                         24;
                    }
                    else
                    {
                        firstChild = agentResponseGO
                                     .transform.parent.parent
                                     .Find(imageResponseGO.name + "(Clone)_" + attachment.Id).transform;
                        firstChild.GetComponent<HorizontalLayoutGroup>().childAlignment = TextAnchor.MiddleLeft;
                        kMaxResolution = agentResponseGO
                                         .transform.parent.parent.parent.GetComponent<RectTransform>().rect.width -
                                         24;
                    }

                    var child = firstChild.transform.GetChild(0);
                    child.GetComponent<RawImage>().texture = texture;
                    child.parent.GetComponent<RawImage>().texture =
                        GetPlaceholderTexture(texture.height, texture.width);
                    int properWidth;
                    int properHeight;
                    ParseImageDimensionsToSDK(texture.height, texture.width, out properHeight, out properWidth);
                    child.GetComponent<LayoutElement>().preferredWidth = properWidth;
                    child.GetComponent<LayoutElement>().preferredHeight = properHeight;
                    if (texture.width > kMaxResolution)
                    {
                        child.GetComponent<LayoutElement>().preferredWidth = kMaxResolution;
                        child.GetComponent<LayoutElement>().preferredHeight =
                            (kMaxResolution * texture.height) / texture.width;
                    }
                    else
                    {
                        child.GetComponent<LayoutElement>().preferredWidth = texture.width;
                        child.GetComponent<LayoutElement>().preferredHeight = texture.height;
                    }

                    child.GetComponent<RawImage>().color = Color.white;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void ParseImageDimensionsToSDK(int height, int width, out int heightUnity, out int widthUnity)
        {
            try
            {
                float kMaxResolution =
                    zendeskSupportUI.ticketResponseScrollView.GetComponent<RectTransform>().rect.width
                    - zendeskSupportUI.endUserResponsePrefab.GetComponent<HorizontalLayoutGroup>().padding.left;
                if (width > kMaxResolution)
                {
                    widthUnity = (int) kMaxResolution;
                    heightUnity = (int) (kMaxResolution * height) / width;
                }
                else
                {
                    widthUnity = width;
                    heightUnity = height;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private Texture GetPlaceholderTexture(int height, int width)
        {
            try
            {
                if (width > height)
                {
                    return placeholderLandscape;
                }
                else
                {
                    return placeholderPortrait;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void MessageSentStatus(bool sent)
        {
            //            if (sent)
            //            {
            //                messageStatusPanel.SetActive(true);
            //                messageErrorStatusPanel.SetActive(false);
            //            }
            //            else
            //            {
            //                messageStatusPanel.SetActive(false);
            //                messageErrorStatusPanel.SetActive(true);
            //            }
        }

        private void SetAgentImage(Texture2D texture)
        {
            agentPhotoGO.gameObject.GetComponent<RawImage>().texture = texture;
            agentPhotoGO.gameObject.GetComponent<RawImage>().color = Color.white;
            ((RectTransform) agentPhotoGO.transform).sizeDelta = new Vector2(96, 96);
        }

        private bool IsImageResolutionAcceptable(int height, int width)
        {
            if (height > 4096 || width > 4096)
            {
                return false;
            }

            return true;
        }
    }
}