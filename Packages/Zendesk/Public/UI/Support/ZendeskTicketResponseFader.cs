﻿using UnityEngine;
using UnityEngine.UI;

namespace Zendesk.UI
{
    public class ZendeskTicketResponseFader : MonoBehaviour
    {
        public float speed = 3;
        private HorizontalLayoutGroup horizontalLayoutGroup;
        private CanvasGroup canvasGroup;
        public int start;
        public int end;
        public bool destroyable;
        public float waitSeconds = 4;
        private bool isRunning;
        private float lerpTime;
        private float timeLeft;

        private void Start()
        {
            timeLeft = waitSeconds;
            lerpTime = 0;
            isRunning = true;
            horizontalLayoutGroup = GetComponent<HorizontalLayoutGroup>();
            canvasGroup = GetComponent<CanvasGroup>();
            canvasGroup.alpha = 0;
            horizontalLayoutGroup.padding.top = start;
        }

        private void Update()
        {
            if(isRunning)
            {
                if (destroyable)
                {
                    timeLeft -= Time.fixedUnscaledDeltaTime;
                    if (timeLeft < 0)
                    {
                        Animate(0, start, true);
                    }
                    else
                    {
                        Animate(1, end, false);
                    }
                }
                else
                {
                    Animate(1, end, true);
                }
            }
            else
            {
                if (destroyable)
                {
                    ZendeskErrorUI.AfterDestroyToast(this.gameObject);
                    Destroy(this.gameObject);
                }
            }
        }

        private void Animate(int endAlphaValue, int endPositionValue, bool stopRunning)
        {
            lerpTime += Time.fixedUnscaledDeltaTime * speed;
            canvasGroup.alpha = Mathf.Lerp(canvasGroup.alpha, endAlphaValue, lerpTime);
                        
            RectOffset tempPadding = new RectOffset(
                horizontalLayoutGroup.padding.left,
                horizontalLayoutGroup.padding.right,
                horizontalLayoutGroup.padding.top,
                horizontalLayoutGroup.padding.bottom);
            tempPadding.top = (int)Mathf.Lerp(horizontalLayoutGroup.padding.top, endPositionValue, lerpTime);
            horizontalLayoutGroup.padding = tempPadding;
                        
            if (lerpTime >= 1)
            {
                lerpTime = 0;
                horizontalLayoutGroup.padding.top = endPositionValue;
                canvasGroup.alpha = endAlphaValue;
                isRunning = !stopRunning;
            }
        }
    }
}