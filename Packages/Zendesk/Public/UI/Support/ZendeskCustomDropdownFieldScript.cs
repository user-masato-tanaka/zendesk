﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Zendesk.Common;
using Zendesk.Internal.Models.Common;
using Zendesk.Internal.Models.Support;
using Zendesk.Providers;

namespace Zendesk.UI
{
    public class ZendeskCustomDropdownFieldScript : ZendeskGenericComponentScript
    {
        private Color redOutline;
        private Color greyOutline;
        public Dropdown dropdown;
        public Text validationMessageComponent;
        public GameObject validationPanel;
        public bool required;
        [HideInInspector]
        public DropdownOptions[] dropdownOptions;
        
        public void init(long id, string displayText, DropdownOptions[] options, string validationMessage, string placeholder, bool requiredField)
        {
            idCustomField = id;
            labelComponent.text = displayText;
            validationMessageComponent.text = validationMessage;
            required = requiredField;
            dropdownOptions = options;
            dropdown.AddOptions(options.Select(a => a.Text).ToList());
        }
    }
}