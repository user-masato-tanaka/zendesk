﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Zendesk.UI.HelpCenter;

namespace Zendesk.UI
{
    public class ZendeskArticleListOverview : MonoBehaviour
    {
        public GameObject categoryGO;
        public Text categoryText;
        public Text sectionText;
        [HideInInspector]
        public long sectionId;
        [HideInInspector]
        private ZendeskHelpCenterUI zendeskHelpCenterUI;
        private ZendeskErrorUI zendeskErrorUI;

        public void Init(string categoryText, string sectionText, long sectionId, ZendeskHelpCenterUI helpcenter, ZendeskErrorUI zendeskErrorUI)
        {
            try
            {
                this.sectionId = sectionId;
                this.zendeskHelpCenterUI = helpcenter;
                this.zendeskErrorUI = zendeskErrorUI;
                
                if (!string.IsNullOrEmpty(categoryText))
                {
                    this.categoryText.text = categoryText;
                    this.categoryGO.SetActive(true);
                }
                
                this.sectionText.text = sectionText;
                gameObject.SetActive(true);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        
        public void button_OpenSection()
        {
            try
            {
                zendeskHelpCenterUI.LoadArticlesFromSection(sectionId);
            }
            catch
            {
                zendeskErrorUI.NavigateError(null,true);
            }
        }
        
    }
}