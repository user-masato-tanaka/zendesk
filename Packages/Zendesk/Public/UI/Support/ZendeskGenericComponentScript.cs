﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Zendesk.Common;
using Zendesk.Internal.Models.Common;
using Zendesk.Internal.Models.Support;
using Zendesk.Providers;

namespace Zendesk.UI
{
    public class ZendeskGenericComponentScript : MonoBehaviour 
    {
        [HideInInspector]
        public long idCustomField;
        public Text labelComponent;
    }
}