using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;

public static class FileBrowser{

    
    private static string _selectedImagePath = null;
    private static string _attachmentsCallbackPath = null;
    private static int counter = 1;
    private static string SelectedImagePath
    {
        get
        {
            _selectedImagePath = Path.Combine( Application.temporaryCachePath, "pickedImg_"  + counter);
            counter++;
            if( _selectedImagePath == null )
            {
                Directory.CreateDirectory( Application.temporaryCachePath );
            }
            return _selectedImagePath;
        }
    }
    private static string AttachmentsCallbackPath
    {
        get
        {
            if( _attachmentsCallbackPath == null )
            {
                var supportGO = GameObject.FindWithTag("ZendeskSupportObject");
                if (supportGO != null)
                {
                    _attachmentsCallbackPath = GetGameObjectPath(supportGO.transform);
                }
            }
            return _attachmentsCallbackPath;
        }
    }
    
    private static string GetGameObjectPath(Transform transform)
    {
        string path = transform.name;
        while (transform.parent != null)
        {
            transform = transform.parent;
            path = transform.name + "/" + path;
        }

        path = "/" + path;
        return path;
    }
    
    #if UNITY_IOS
    [DllImport("__Internal")]
    extern public static void OpenFileBrowser(string attachmentsCallbackPath, string selectedImagePath);
   
    public static void OpenFileBrowser()
    {
        OpenFileBrowser(AttachmentsCallbackPath, SelectedImagePath);
    }
    #elif UNITY_ANDROID
    
    public static void OpenFileBrowser()
    {
        AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject javaClass = new AndroidJavaObject("com.zendesk.gallery.UnityBinder");
        javaClass.CallStatic("OpenGallery", new object[]{ jc.GetStatic<AndroidJavaObject>("currentActivity"), AttachmentsCallbackPath, SelectedImagePath});
    }
    #endif
}