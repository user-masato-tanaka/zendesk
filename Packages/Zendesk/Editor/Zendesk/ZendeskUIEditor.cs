using UnityEditor;
using UnityEngine;
using Zendesk.Internal.Models.Core;
using Zendesk.Providers;
using Zendesk.UI.HelpCenter;

namespace Zendesk.UI
{
    [CustomEditor(typeof(ZendeskUI))]
    public class ZendeskUIEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            ZendeskUI myScript = (ZendeskUI) target;
            if (myScript != null)
            {
                myScript.zendeskCanvas = (GameObject) EditorGUILayout.ObjectField("Zendesk Canvas",
                    myScript.zendeskCanvas, typeof(GameObject), true);
                myScript.zendeskButtonsCanvas = (GameObject) EditorGUILayout.ObjectField("Zendesk Buttons Canvas",
                    myScript.zendeskButtonsCanvas, typeof(GameObject), true);
                myScript.zendeskSupportUIGO = (GameObject) EditorGUILayout.ObjectField("Zendesk Support UI GO",
                    myScript.zendeskSupportUIGO, typeof(GameObject), true);
                myScript.zendeskHelpCenterUIGO = (GameObject) EditorGUILayout.ObjectField("Zendesk Help Center UI GO",
                    myScript.zendeskHelpCenterUIGO, typeof(GameObject), true);
                myScript.useAndroidBackButtonViaZendesk = GUILayout.Toggle(myScript.useAndroidBackButtonViaZendesk,
                    "Use Android Back Button");
                myScript.loadingBar = (GameObject) EditorGUILayout.ObjectField("Loading Bar",
                    myScript.loadingBar, typeof(GameObject), true);
                myScript.useZendeskUI = GUILayout.Toggle(myScript.useZendeskUI, "Use Zendesk UI");
                using (new EditorGUI.DisabledScope(!myScript.useZendeskUI))
                {
                    var useZendeskUIButtons = serializedObject.FindProperty("useZendeskUIButtons");
                    EditorGUILayout.PropertyField(useZendeskUIButtons, true);
                    using (new EditorGUI.DisabledScope(!myScript.useZendeskUIButtons))
                    {
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("openSupportString"), true);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("openHelpCenterString"), true);
                        EditorGUILayout.PropertyField(serializedObject.FindProperty("createRequestString"), true);
                    }

                    serializedObject.ApplyModifiedProperties();
                }

                EditorUtility.SetDirty(myScript);
            }

            if (EditorApplication.isPlaying)
                Repaint();
        }
    }
}