using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Zendesk.UI
{
    [CustomEditor(typeof(ZendeskCustomerSatisfactionRating))]
    public class ZendeskCustomerSatisfactionRatingUIEditor : Editor
    {
        private bool cSatUIComponents = true, cSatGameObjects = true;
        public override void OnInspectorGUI()
        {
            ZendeskCustomerSatisfactionRating myScript = (ZendeskCustomerSatisfactionRating)target;
            if (myScript != null)
            {
                cSatUIComponents = EditorGUILayout.Foldout(cSatUIComponents,"UI Components");
                if(cSatUIComponents)
                {
                    myScript.cSatMessage = (Text) EditorGUILayout.ObjectField("Bot Message", myScript.cSatMessage, typeof(Text), true);
                    myScript.cSatGood = (Text) EditorGUILayout.ObjectField("Good Score Text", myScript.cSatGood, typeof(Text), true);
                    myScript.cSatBad = (Text) EditorGUILayout.ObjectField("Bad Score Text", myScript.cSatBad, typeof(Text), true);
                    myScript.answerBotLabelText = (Text) EditorGUILayout.ObjectField("Answer Bot Label Text", myScript.answerBotLabelText, typeof(Text), true);
                }
                
                cSatGameObjects = EditorGUILayout.Foldout(cSatGameObjects,"Game Objects");
                if(cSatGameObjects)
                {
                    myScript.goodScorePanel = (GameObject) EditorGUILayout.ObjectField("Good Score Panel", myScript.goodScorePanel, typeof(GameObject), true);
                    myScript.badScorePanel = (GameObject) EditorGUILayout.ObjectField("Bad Score Panel", myScript.badScorePanel, typeof(GameObject), true);
                    myScript.ratingResponse = (GameObject) EditorGUILayout.ObjectField("Rating Response", myScript.ratingResponse, typeof(GameObject), true);
                }
                EditorUtility.SetDirty (myScript);
            }
            if (EditorApplication.isPlaying)
                Repaint();
        }
    }
}