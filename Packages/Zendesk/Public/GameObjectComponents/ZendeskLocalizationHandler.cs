using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using Zendesk.Internal.Models.Core;

namespace Zendesk.UI
{
    public class ZendeskLocalizationHandler : MonoBehaviour
    {
        public TextAsset translationsFile;
        [HideInInspector]
        public string defaultLocale = "en-us";
        [HideInInspector]
        public int? defaultLocaleIndex;
        public Dictionary<String, String> translationGameObjects = new Dictionary<String, String>();

        public string Locale { get; set; }
        private readonly char lineSeperator = '\n';
        private List<String> langList = new List<string>();

        public void SetLocaleISO(int locale = (int) SystemLanguage.Unknown)
        {
            var currentLocale = (int)Application.systemLanguage;
            if ((int)locale != (int)SystemLanguage.Unknown)
            {
                currentLocale = locale;
            }

            if (!CheckIfLanguageSupported(currentLocale))
            {
                currentLocale = (int) SystemLanguage.Unknown;
            }
            
            switch (currentLocale)
            {
                case (int)SystemLanguage.Arabic:
                {
                    this.Locale = "ar";
                    break;
                }
                case (int)SystemLanguage.Portuguese:
                {
                    this.Locale = "pt-br";
                    break;
                }
                case (int)SystemLanguage.Bulgarian:
                {
                    this.Locale = "bg";
                    break;
                }
                case (int)SystemLanguage.Czech:
                {
                    this.Locale = "cs";
                    break;
                }
                case (int)SystemLanguage.Hebrew:
                {
                    this.Locale = "he";
                    break;
                }
                case (int)SystemLanguage.Danish:
                {
                    this.Locale = "da";
                    break;
                }
                case (int)SystemLanguage.Dutch:
                {
                    this.Locale = "nl";
                    break;
                }
                case (int)SystemLanguage.English:
                {
                    this.Locale = "en-US";
                    break;
                }
                case (int)SystemLanguage.Finnish:
                {
                    this.Locale = "fi";
                    break;
                }
                case (int)SystemLanguage.French:
                {
                    this.Locale = "fr";
                    break;
                }
                case (int)SystemLanguage.Hungarian:
                {
                    this.Locale = "hu";
                    break;
                }
                case (int)SystemLanguage.German:
                {
                    this.Locale = "de";
                    break;
                }
                case (int)SystemLanguage.Greek:
                {
                    this.Locale = "el";
                    break;
                }
                case (int)SystemLanguage.Indonesian:
                {
                    this.Locale = "id";
                    break;
                }
                case (int)SystemLanguage.Italian:
                {
                    this.Locale = "it";
                    break;
                }
                case (int)SystemLanguage.Japanese:
                {
                    this.Locale = "ja";
                    break;
                }
                case (int)SystemLanguage.Korean:
                {
                    this.Locale = "ko";
                    break;
                }
                case (int)SystemLanguage.Norwegian:
                {
                    this.Locale = "no";
                    break;
                }
                case (int)SystemLanguage.Polish:
                {
                    this.Locale = "pl";
                    break;
                }
                case (int)SystemLanguage.Romanian:
                {
                    this.Locale = "ro";
                    break;
                }
                case (int)SystemLanguage.Russian:
                {
                    this.Locale = "ru";
                    break;
                }
                case (int)SystemLanguage.Chinese:
                {
                    this.Locale = "zh-cn";
                    break;
                }
                case (int)SystemLanguage.Spanish:
                {
                    this.Locale = "es";
                    break;
                }
                case (int)SystemLanguage.Thai:
                {
                    this.Locale = "th";
                    break;
                }
                case (int)SystemLanguage.ChineseSimplified:
                {
                    this.Locale = "zh-cn";
                    break;
                }
                case (int)SystemLanguage.ChineseTraditional:
                {
                    this.Locale = "zh-tw";
                    break;
                }
                case (int)SystemLanguage.Turkish:
                {
                    this.Locale = "tr";
                    break;
                }
                case (int)SystemLanguage.Vietnamese:
                {
                    this.Locale = "vi";
                    break;
                }
                case (int)SystemLanguage.Afrikaans:
                {
                    this.Locale = "af";
                    break;
                }
                case (int)SystemLanguage.Basque:
                {
                    this.Locale = "eu";
                    break;
                }
                case (int)SystemLanguage.Belarusian:
                {
                    this.Locale = "be";
                    break;
                }
                case (int)SystemLanguage.Catalan:
                {
                    this.Locale = "ca";
                    break;
                }
                case (int)SystemLanguage.Estonian:
                {
                    this.Locale = "et";
                    break;
                }
                case (int)SystemLanguage.Faroese:
                {
                    this.Locale = "fo";
                    break;
                }
                case (int)SystemLanguage.Icelandic:
                {
                    this.Locale = "is";
                    break;
                }
                case (int)SystemLanguage.Latvian:
                {
                    this.Locale = "lv";
                    break;
                }
                case (int)SystemLanguage.Lithuanian:
                {
                    this.Locale = "lt";
                    break;
                }
                case (int)SystemLanguage.Slovak:
                {
                    this.Locale = "sk";
                    break;
                }
                case (int)SystemLanguage.Swedish:
                {
                    this.Locale = "sv";
                    break;
                }
                case (int)SystemLanguage.Ukrainian:
                {
                    this.Locale = "uk";
                    break;
                }
                case (int)SystemLanguage.SerboCroatian:
                {
                    this.Locale = "hr";
                    break;
                }
                default:
                {
                    this.Locale = defaultLocale;
                    break;
                }
            }
        }
        
        public string SetLocaleISOForUserAuth()
        {
            var currentLocale = (int)Application.systemLanguage;
           
            switch (currentLocale)
            {
                case (int)SystemLanguage.Arabic:
                {
                    return "ar";
                }
                case (int)SystemLanguage.Portuguese:
                {
                    return "pt-br";
                }
                case (int)SystemLanguage.Bulgarian:
                {
                    return "bg";
                }
                case (int)SystemLanguage.Czech:
                {
                    return "cs";
                }
                case (int)SystemLanguage.Hebrew:
                {
                    return "he";
                }
                case (int)SystemLanguage.Danish:
                {
                    return "da";
                }
                case (int)SystemLanguage.Dutch:
                {
                    return "nl";
                }
                case (int)SystemLanguage.English:
                {
                    return "en-US";
                }
                case (int)SystemLanguage.Finnish:
                {
                    return "fi";
                }
                case (int)SystemLanguage.French:
                {
                    return "fr";
                }
                case (int)SystemLanguage.Hungarian:
                {
                    return "hu";
                }
                case (int)SystemLanguage.German:
                {
                    return "de";
                }
                case (int)SystemLanguage.Greek:
                {
                    return "el";
                }
                case (int)SystemLanguage.Indonesian:
                {
                    return "id";
                }
                case (int)SystemLanguage.Italian:
                {
                    return "it";
                }
                case (int)SystemLanguage.Japanese:
                {
                    return "ja";
                }
                case (int)SystemLanguage.Korean:
                {
                    return "ko";
                }
                case (int)SystemLanguage.Norwegian:
                {
                    return "no";
                }
                case (int)SystemLanguage.Polish:
                {
                    return "pl";
                }
                case (int)SystemLanguage.Romanian:
                {
                    return "ro";
                }
                case (int)SystemLanguage.Russian:
                {
                    return "ru";
                }
                case (int)SystemLanguage.Chinese:
                {
                    return "zh-cn";
                }
                case (int)SystemLanguage.Spanish:
                {
                    return "es";
                }
                case (int)SystemLanguage.Thai:
                {
                    return "th";
                }
                case (int)SystemLanguage.ChineseSimplified:
                {
                    return "zh-cn";
                }
                case (int)SystemLanguage.ChineseTraditional:
                {
                    return "zh-tw";
                }
                case (int)SystemLanguage.Turkish:
                {
                    return "tr";
                }
                case (int)SystemLanguage.Vietnamese:
                {
                    return "vi";
                }
                case (int)SystemLanguage.Afrikaans:
                {
                    return "af";
                }
                case (int)SystemLanguage.Basque:
                {
                    return "eu";
                }
                case (int)SystemLanguage.Belarusian:
                {
                    return "be";
                }
                case (int)SystemLanguage.Catalan:
                {
                    return "ca";
                }
                case (int)SystemLanguage.Estonian:
                {
                    return "et";
                }
                case (int)SystemLanguage.Faroese:
                {
                    return "fo";
                }
                case (int)SystemLanguage.Icelandic:
                {
                    return "is";
                }
                case (int)SystemLanguage.Latvian:
                {
                    return "lv";
                }
                case (int)SystemLanguage.Lithuanian:
                {
                    return "lt";
                }
                case (int)SystemLanguage.Slovak:
                {
                    return "sk";
                }
                case (int)SystemLanguage.Swedish:
                {
                    return "sv";
                }
                case (int)SystemLanguage.Ukrainian:
                {
                    return "uk";
                }
                case (int)SystemLanguage.SerboCroatian:
                {
                    return "hr";
                }
                default:
                {
                    return defaultLocale;
                }
            }
        }

        private bool CheckIfLanguageSupported(int currentLocale)
        {
            foreach (ZendeskLocales zendeskLocale  in Enum.GetValues(typeof(ZendeskLocales)))
            {
                if (currentLocale == (int)zendeskLocale)
                {
                    return true;
                }
            }
            return false;
        }

        // Read data from CSV file
        public void ReadData(string helpCenterLocale = "", bool checkSuccess = true)
        {
            translationGameObjects.Clear();
            langList.Clear();
            string[] records = translationsFile.text.Split(lineSeperator);
            var isFirstLine = true;
            var currentLocaleIndex = -1;
            int? success = null;
            Regex elementParser = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
            foreach (string record in records)
            {
                if (!String.IsNullOrEmpty(record))
                {
                    string[] fields = elementParser.Split(record);
                    if (isFirstLine)
                    {
                        if (!string.IsNullOrEmpty(this.Locale) && fields.Contains(this.Locale))
                        {
                            currentLocaleIndex = Array.FindIndex(fields, t => t.Equals(this.Locale, StringComparison.InvariantCultureIgnoreCase));
                            success = 1;
                        }
                        else if (!string.IsNullOrEmpty(helpCenterLocale) && fields.Contains(helpCenterLocale))
                        {
                            currentLocaleIndex = Array.FindIndex(fields, t => t.Equals(helpCenterLocale, StringComparison.InvariantCultureIgnoreCase));
                            this.Locale = helpCenterLocale;
                        }
                        else if (fields.Any(x => x.ToLower() == this.defaultLocale))
                        {
                            currentLocaleIndex = Array.FindIndex(fields, t => t.Equals(this.defaultLocale, StringComparison.InvariantCultureIgnoreCase));
                            this.Locale = this.defaultLocale;
                            success = 0;
                        }
                        else
                        {
                            throw new Exception(
                                "<color='red'><b>[ZENDESK ERROR]</b>Control the csv file under Zendesk/Localization folder, locale can not be found.</color>");
                        }
                        
                        if (fields.Any(x => x.ToLower() == this.defaultLocale))
                        {
                            defaultLocaleIndex = Array.FindIndex(fields, t => t.Equals(this.defaultLocale, StringComparison.InvariantCultureIgnoreCase));
                        }

                        langList.AddRange(fields);
                        isFirstLine = false;
                    }
                    else
                    { 
                        var translation = "";
                        if (fields.Length > currentLocaleIndex)
                        {
                            translation = GetRidOfCommaEscapers(fields[currentLocaleIndex]);
                            if (string.IsNullOrEmpty(translation))
                            {
                                if (defaultLocaleIndex != null)
                                    translation = GetRidOfCommaEscapers(fields[defaultLocaleIndex.Value]);
                            }
                        }
                        else if (defaultLocaleIndex.HasValue && fields.Length > defaultLocaleIndex.Value)
                        {
                            translation = GetRidOfCommaEscapers(fields[defaultLocaleIndex.Value]);
                        }
                        translationGameObjects.Add(fields[0], translation);

                    }
                }
            }

            if (checkSuccess)
            {
                if (success.HasValue)
                {
                    if (success == 1)
                    {
                        Debug.Log(String.Format(translationGameObjects["usdk_locale_set_success_message"], this.Locale));
                    }else if(success == 0)
                    {
                        Debug.Log(String.Format(translationGameObjects["usdk_locale_set_default_locale_message"], this.Locale));
                    }
                }
                else
                {
                    Debug.Log(String.Format(translationGameObjects["usdk_locale_set_help_center_locale_message"], this.Locale));
                }
            }
        }

        private static string GetRidOfCommaEscapers(string translation)
        {
            if (translation.StartsWith("\"") && translation.EndsWith("\""))
            {
                return translation.Substring(1, translation.Length - 2);
            }

            return translation;
        }
    }
}