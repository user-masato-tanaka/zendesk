using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Zendesk.UI
{
    public class ScrollRectExpanded : ScrollRect
    {
        public override void OnEndDrag(PointerEventData eventData)
        {
            if(content.GetComponent<RectTransform>().localPosition.y <= -250)
            {
                GetComponentInParent<ZendeskUI>().RefreshPage();
            }
            base.OnEndDrag(eventData);
        }
    }
}