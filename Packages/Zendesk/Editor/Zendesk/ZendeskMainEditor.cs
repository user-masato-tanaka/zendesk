using UnityEditor;
using UnityEngine;
using Zendesk.Internal.Models.Core;
using Zendesk.Providers;

namespace Zendesk.UI
{
    [CustomEditor(typeof(ZendeskMain))]
    public class ZendeskMainEditor : Editor
    {
        int tab = 0;
        public override void OnInspectorGUI()
        {
            ZendeskMain myScript = (ZendeskMain)target;
            if (myScript != null)
            {
                tab = GUILayout.Toolbar (tab, new string[] {"Basic Setup", "Advanced", "Pause Control"});
                switch (tab) {
                    case 0:
                    {
                        myScript.zendeskUrl = EditorGUILayout.TextField("Zendesk URL", myScript.zendeskUrl);
                        myScript.appId = EditorGUILayout.TextField("Application ID", myScript.appId);
                        myScript.clientId = EditorGUILayout.TextField("Client ID", myScript.clientId);
                        myScript.setLocaleFromSettings = GUILayout.Toggle(myScript.setLocaleFromSettings, "Set Locale from Device");
                        
                        using (new EditorGUI.DisabledScope(myScript.setLocaleFromSettings))
                        {
                            myScript.locale = (ZendeskLocales) EditorGUILayout.EnumPopup ("Locale", myScript.locale);
                        }
                
                        if (GUILayout.Button("Test Configuration"))
                        {
                            myScript.TestConfiguration();
                        }
                        break;
                    }
                    case 1:
                    {
                        myScript.commonTags = EditorGUILayout.TextField("Common Tags", myScript.commonTags);
                        myScript.supportProvider =
                            (ZendeskSupportProvider) EditorGUILayout.ObjectField("Support Provider",
                                myScript.supportProvider, typeof(ZendeskSupportProvider),false);
                        myScript.helpCenterProvider =
                            (ZendeskHelpCenterProvider) EditorGUILayout.ObjectField("Help Center Provider",
                                myScript.helpCenterProvider, typeof(ZendeskHelpCenterProvider), false);
                        myScript.zendeskLocalizationHandler =
                            (ZendeskLocalizationHandler) EditorGUILayout.ObjectField("Zendesk Localization Handler",
                                myScript.zendeskLocalizationHandler, typeof(ZendeskLocalizationHandler),false);
                        break;
                    }
                    case 2:
                        myScript.autoPause = GUILayout.Toggle(myScript.autoPause, "Auto-Pause");
                        using (new EditorGUI.DisabledScope(!myScript.autoPause))
                        {
                            // Find the property corresponding to the UnityEvent we want to edit.
                            var pauseProp = serializedObject.FindProperty("pauseFunctionality");
                            var unpauseProp = serializedObject.FindProperty("resumeFunctionality");

                            // Draw the Inspector widget for this property.
                            EditorGUILayout.PropertyField(pauseProp, true);
                            EditorGUILayout.PropertyField(unpauseProp, true);

                            // Commit changes to the property back to the component we're editing.
                            serializedObject.ApplyModifiedProperties();
                        }
                        break;
                }
                
                EditorUtility.SetDirty (myScript);
            }
            if (EditorApplication.isPlaying)
                Repaint();

        }

    }
}
