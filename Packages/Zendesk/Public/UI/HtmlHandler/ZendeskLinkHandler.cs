﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zendesk.Common;

namespace Zendesk.UI
{
	public class ZendeskLinkHandler : MonoBehaviour
	{
		private ZendeskMain zendeskMain;
		private ZendeskCore zendeskCore;
		private string knowledgeBaseSuffix = "knowledge/";
		private string hcBaseSuffix = "hc/";
		private ZendeskErrorUI zendeskErrorUI;
		private Dictionary<string, string> linkDictionary;
		void Start ()
		{
			zendeskMain = GetComponent<ZendeskMain>();
			zendeskCore = GetComponent<ZendeskCore>();
			linkDictionary = new Dictionary<string, string>();
		}

		public void Init(ZendeskErrorUI zendeskErrorUI)
		{
			this.zendeskErrorUI = zendeskErrorUI;
			linkDictionary = new Dictionary<string, string>();
		}

		public string AddLink(string href)
		{
			if (linkDictionary == null)
			{
				linkDictionary = new Dictionary<string, string>();
			}
			
			string linkID = Guid.NewGuid().ToString("N").Substring(0, 2);
			while (linkDictionary.Keys.Contains(linkID))
			{
				linkID = Guid.NewGuid().ToString("N").Substring(0, 2);
			}
			
			linkDictionary.Add(linkID, href);
			return linkID;
		}
		
		public void HandleLink(string link)
		{
			var linkIDElement = linkDictionary.Where(a => a.Key == link).FirstOrDefault();
			link = linkIDElement.Value;
			try
			{
				if (IsInternalLink(link))
				{
					zendeskMain.zendeskUI.zendeskHelpCenterUI.LoadIndividualArticle(GetArticleId(link), GetArticleLocale(link));
				}
				else
				{
					Application.OpenURL(link);
				}
			}
			catch
			{
				zendeskErrorUI.NavigateError(null,true);
			}
		}

		public bool IsInternalLink(string link)
		{
			if (string.IsNullOrEmpty(link) || string.IsNullOrEmpty(zendeskCore.ZendeskUrl) ||
			    string.IsNullOrEmpty(knowledgeBaseSuffix) || string.IsNullOrEmpty(hcBaseSuffix))
			{
				return false;
			}
			
			try
			{
				Uri uri = new Uri(this.zendeskCore.ZendeskUrl);
				string subDomain = uri.Host + "/";
				
				if (link.ToUpper().Contains(subDomain.ToUpper() + this.knowledgeBaseSuffix.ToUpper()) ||
				    link.ToUpper().Contains(subDomain.ToUpper() + this.hcBaseSuffix.ToUpper()))
				{
					return true;
				}
			}
			catch(Exception e)
			{
				throw e;
			}
			
			return false;
		}

		public long GetArticleId(string articleURL)
		{
			try
			{
				if (!articleURL.ToUpper().Contains("ARTICLES"))
					return 0;
				
				string articleIdAndName = articleURL.Split('/').Last();
				long articleId = long.Parse(articleIdAndName.Split('-').First());
				return articleId;
			}
			catch
			{
				return 0;
			}
		}
		
		public string GetArticleLocale(string articleURL)
		{
			try
			{
				var splitUrl = articleURL.Split('/');
				return splitUrl[4];
			}
			catch
			{
				return zendeskMain.zendeskLocalizationHandler.Locale;
			}
		}
	}
}

