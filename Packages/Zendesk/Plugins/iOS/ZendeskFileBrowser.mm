#import <Foundation/Foundation.h>
#import <Photos/Photos.h>
#import <ImageIO/ImageIO.h>
#import <MobileCoreServices/UTCoreTypes.h>
//------- start header ----------------
@interface ZendeskFileBrowser : NSObject

+(void) checkPermission:(NSString *)path callPath:(NSString *)callbackPath;
+(void) askPermission;
+(void) askPermissionAccessLevel;
+(void) showImagePicker;
+ (UIImage *)scaleAndRotateImage:(UIImage *) image; 
@end

@implementation ZendeskFileBrowser

static NSString *savePath;
static NSString *resultPath;
static NSString *callbackFuncPath;
UIImagePickerController* imagePicker;

+(void) checkPermission : (NSString*) selectedPath callPath:(NSString*)callbackPath{
    savePath = selectedPath;
    callbackFuncPath = callbackPath;
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    if (@available(iOS 14, *)) 
    {
        #if __IPHONE_OS_VERSION_MAX_ALLOWED >= 140000 // Xcode 12 and iOS 14, or greater
        status = [PHPhotoLibrary authorizationStatusForAccessLevel:PHAccessLevelAddOnly];
        if (status == PHAuthorizationStatusAuthorized) {
            [self showImagePicker];
        } else if (status == PHAuthorizationStatusNotDetermined) {
            [self askPermissionAccessLevel];
        }else if (status == PHAuthorizationStatusLimited) {
            char* callbackFncPath = [self getCString:callbackFuncPath];
            UnitySendMessage(callbackFncPath, "ImageSavedToTemporaryPath", "error");
        }else{
            char* callbackFncPath = [self getCString:callbackFuncPath];
            UnitySendMessage(callbackFncPath, "ImageSavedToTemporaryPath", "error");
        }
        #endif
    }
    else
    {
        if (status == PHAuthorizationStatusAuthorized) {
            [self showImagePicker];
        } else if (status == PHAuthorizationStatusNotDetermined) {
            [self askPermission];
        }else{
            char* callbackFncPath = [self getCString:callbackFuncPath];
            UnitySendMessage(callbackFncPath, "ImageSavedToTemporaryPath", "error");
        }
    }
}

+(void) askPermission {
    __block BOOL isAuthorized = NO;
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    if(status == PHAuthorizationStatusNotDetermined) {
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        isAuthorized = (status == PHAuthorizationStatusAuthorized);
            dispatch_semaphore_signal(sema);
        }];
   
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        if(isAuthorized){
            [self checkPermission : savePath callPath: callbackFuncPath];
        }
    }
}

+(void) askPermissionAccessLevel {
    #if __IPHONE_OS_VERSION_MAX_ALLOWED >= 140000 // Xcode 12 and iOS 14, or greater
    __block BOOL isAuthorized = NO;
    PHAuthorizationStatus prevStatus = [PHPhotoLibrary authorizationStatus];
    if (@available(iOS 14, *)) {
        prevStatus = [PHPhotoLibrary authorizationStatusForAccessLevel:PHAccessLevelAddOnly];
        if (prevStatus == PHAuthorizationStatusNotDetermined) {
            dispatch_semaphore_t sema = dispatch_semaphore_create(0);
            [PHPhotoLibrary requestAuthorizationForAccessLevel:(PHAccessLevelAddOnly) handler:^(PHAuthorizationStatus status) {
                isAuthorized = (status == PHAuthorizationStatusAuthorized);
                dispatch_semaphore_signal(sema);
            }];
            dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
            if(isAuthorized){
                [self checkPermission : savePath callPath: callbackFuncPath];
            }
        }
    }
    #endif
}

+(void) showImagePicker{
    imagePicker = [[UIImagePickerController alloc]init];
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.delegate = self;
        UIViewController *rootViewController = UnityGetGLViewController();
        [rootViewController presentViewController:imagePicker animated:true completion:nil];
    }
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
+ (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {

    UIImage *image = [self scaleAndRotateImage: [info objectForKey:UIImagePickerControllerOriginalImage]];
    
    if (image != nil) {
        NSInteger width = image.size.width * image.scale;
        NSInteger height = image.size.height * image.scale;
        
        if(width > 4096 || height > 4096)
        {
            char* callbackFncPath = [self getCString:callbackFuncPath];
            UnitySendMessage(callbackFncPath, "ImageSavedToTemporaryPath", "error_res");
            resultPath = nil;
        }
        else
        {
            resultPath = [savePath stringByAppendingPathExtension:@"jpeg"];
            if (![UIImageJPEGRepresentation(image, 0.7) writeToFile:resultPath atomically:YES]) {
                NSLog(@"Error creating JPEG image");
                resultPath = nil;
            }
        }
    }
    else
        NSLog(@"Error fetching original image from picker");
	
	imagePicker = nil;
	char* fileName = [self getCString:resultPath];
    char* callbackFncPath = [self getCString:callbackFuncPath];
	UnitySendMessage(callbackFncPath, "ImageSavedToTemporaryPath", fileName);
	
	[picker dismissViewControllerAnimated:NO completion:nil];
}

+ (char *)getCString:(NSString *)source {
	if (source == nil)
		source = @"";
	
	const char *sourceUTF8 = [source UTF8String];
	char *result = (char*) malloc(strlen(sourceUTF8) + 1);
	strcpy(result, sourceUTF8);
	
	return result;
}

+ (UIImage *)scaleAndRotateImage:(UIImage *) image {
    int kMaxResolution = 1280;

    CGImageRef imgRef = image.CGImage;

    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);


    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = bounds.size.width / ratio;
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = bounds.size.height * ratio;
        }
    }

    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {

        case UIImageOrientationUp: 
            transform = CGAffineTransformIdentity;
            break;

        case UIImageOrientationUpMirrored: 
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;

        case UIImageOrientationDown: 
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;

        case UIImageOrientationDownMirrored: 
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;

        case UIImageOrientationLeftMirrored: 
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;

        case UIImageOrientationLeft: 
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;

        case UIImageOrientationRightMirrored: 
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;

        case UIImageOrientationRight: 
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;

        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];

    }

    UIGraphicsBeginImageContext(bounds.size);

    CGContextRef context = UIGraphicsGetCurrentContext();

    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }

    CGContextConcatCTM(context, transform);

    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return imageCopy;
}

+ (void)trySaveSourceImage:(NSData *)imageData withInfo:(NSDictionary *)info {
	NSString *filePath = info[@"PHImageFileURLKey"];
	if (filePath != nil) 
		filePath = [NSString stringWithFormat:@"%@", filePath];
	
	if (filePath == nil || [filePath length] == 0)
	{
		filePath = info[@"PHImageFileUTIKey"];
		if (filePath != nil)
			filePath = [NSString stringWithFormat:@"%@", filePath];
	}
	
	if (filePath == nil || [filePath length] == 0)
		resultPath = savePath;
	else
		resultPath = [savePath stringByAppendingPathExtension:[filePath pathExtension]];
	
	NSError *error;
	if (![[NSFileManager defaultManager] fileExistsAtPath:resultPath] || [[NSFileManager defaultManager] removeItemAtPath:resultPath error:&error]) {
		if (![imageData writeToFile:resultPath atomically:YES]) {
			NSLog(@"Error copying source image to file");
			resultPath = nil;
		}
	}
	else {
		NSLog(@"Error deleting existing image: %@", error);
		resultPath = nil;
	}
}


extern "C" void OpenFileBrowser( const char* attachmentsCallbackPath, const char* path ){
    NSString* selectedPath = [[NSString alloc] initWithUTF8String:path];
    NSString* callbackPath = [[NSString alloc] initWithUTF8String:attachmentsCallbackPath];
    [ZendeskFileBrowser checkPermission:selectedPath callPath:callbackPath];
}

@end
