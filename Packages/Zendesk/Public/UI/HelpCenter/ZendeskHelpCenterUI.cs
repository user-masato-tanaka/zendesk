using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Zendesk.Internal.Models.Common;
using Zendesk.Internal.Models.Core;
using Zendesk.Internal.Models.HelpCenter;

namespace Zendesk.UI.HelpCenter
{
    public class ZendeskHelpCenterUI : MonoBehaviour
    {
        #region Public Properties
        
        public bool showArticleVotes = false;       
        public GameObject myHelpCenterPanel;
        public GameObject articleRenderPanel;
        public GameObject helpCenterView;
        public GameObject sectionPrefab;
        public GameObject backButtonContainer;
        public GameObject helpCenterScrollView;
        public GameObject articleTitle;
        public GameObject footerPanel;
        public Text myHelpCenterFooterContactUsButtonText;
        public GameObject articleListPanel;
        public GameObject articleListContentOverviewPrefab;
        public GameObject articleListView;
        public GameObject headerContainer;
        public Text headerTitle;
        public GameObject headerPanel; 
        
        #endregion
        
        #region Private Properties
        
        private ZendeskErrorUI zendeskErrorUI;
        private ZendeskMain zendeskMain;
        private SectionsResponse sectionsResponse;
        private CategoriesResponse categoriesResponse;
        private ZendeskUI zendeskUI;
        private ZendeskArticleRender zendeskArticleRender;
        private ZendeskSettings zendeskSettings;
        private ZendeskLocalizationHandler zendeskLocalizationHandler;
        private GameObject zendeskCanvas;
        
        #endregion
        
        public void OpenHelpCenter()
        {
            try
            {
                if (!zendeskCanvas.activeSelf)
                {
                    zendeskCanvas.SetActive(true);
                }
                if (zendeskMain != null && zendeskSettings != null)
                {
                    gameObject.SetActive(true);
                    if (zendeskSettings.HelpCenter.Enabled)
                    {
                        InitHelpCenter();
                        ShowArticlesWindow();
                    }
                    else
                    {
                        zendeskUI.AddScreenBackState(myHelpCenterPanel, ZendeskScreen.MyHelpCenter);
                        zendeskErrorUI.NavigateError(
                            zendeskMain.zendeskLocalizationHandler.translationGameObjects
                                ["usdk_help_center_is_not_enabled"], true,false,"hide");
                    }
                }
                else
                {
                    zendeskUI.AddScreenBackState(myHelpCenterPanel, ZendeskScreen.MyHelpCenter);
                    zendeskErrorUI.NavigateError(null, true);
                }
            }
            catch
            {
                zendeskErrorUI.NavigateError(null, true);
            }
        }

        private void OpenMainHelpCenter()
        {
            try
            {
                gameObject.SetActive(true);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void InitWithErrorHandler(ZendeskMain zendeskMain, ZendeskUI zendeskUI, ZendeskErrorUI zendeskErrorUI)
        {
            this.zendeskMain = zendeskMain;
            this.zendeskUI = zendeskUI;
            this.zendeskErrorUI = zendeskErrorUI;
            this.zendeskCanvas = this.zendeskUI.zendeskCanvas;
        }

        public void Init(ZendeskMain zendeskMain, ZendeskUI zendeskUI, ZendeskSettings zendeskSettings,
            ZendeskErrorUI zendeskErrorUI)
        {
            this.zendeskMain = zendeskMain;
            this.zendeskUI = zendeskUI;
            this.zendeskCanvas = this.zendeskUI.zendeskCanvas;
            this.zendeskErrorUI = zendeskErrorUI;
            this.zendeskSettings = zendeskSettings;
            this.zendeskLocalizationHandler = zendeskMain.GetComponent<ZendeskLocalizationHandler>();
            articleRenderPanel.GetComponent<ZendeskArticleRender>().Init(zendeskMain,zendeskErrorUI,zendeskLocalizationHandler);
            SetHelpCenterStrings();
        }

        private void SetHelpCenterStrings()
        {
            myHelpCenterFooterContactUsButtonText.text =
                zendeskMain.zendeskLocalizationHandler.translationGameObjects["usdk_get_in_touch_label_saddle_bar"];
        }

        private void ShowArticlesWindow()
        {
            try
            {
                zendeskUI.AddScreenBackState(myHelpCenterPanel, ZendeskScreen.MyHelpCenter);
                LoadArticles();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void ShowArticlesWindowCached()
        {
            try
            {
                if (helpCenterView.transform.childCount > 0)
                {
                    zendeskUI.AddScreenBackState(myHelpCenterPanel, ZendeskScreen.MyHelpCenter);
                    zendeskUI.ShowBackButton(backButtonContainer);
                    articleRenderPanel.SetActive(false);
                    articleListPanel.SetActive(false);
                    footerPanel.SetActive(true);
                    helpCenterScrollView.SetActive(true);
                }
                else
                {
                    OpenHelpCenter();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void LoadIndividualArticle(Article article)
        {
            try
            {
                zendeskUI.StartLoading(headerPanel);
                zendeskUI.AddScreenBackState(articleRenderPanel, ZendeskScreen.ArticleScreen, article);
                articleRenderPanel.SetActive(true);
                helpCenterScrollView.SetActive(false);
                articleListPanel.SetActive(false);
                footerPanel.SetActive(false);
                articleTitle.GetComponent<Text>().text = ClearHtmlText(article.Title);
                this.articleRenderPanel.GetComponent<ZendeskArticleRender>().RenderArticle(article);
                zendeskUI.ShowBackButton(backButtonContainer);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        
        public void LoadIndividualArticle(long articleId, string locale)
        {
            try
            {
                if (articleId == 0)
                {
                    OpenMainHelpCenter();
                    zendeskMain.zendeskUI.zendeskSupportUI.CloseSupportUI();
                    OpenHelpCenter();
                    zendeskUI.ShowBackButton(backButtonContainer);
                    return;
                }
                
                zendeskUI.StartLoading(headerPanel);
                zendeskMain.helpCenterProvider.GetArticle(GetArticleCallback, articleId, locale);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void GetArticleCallback(ZendeskResponse<ArticleResponse> articleResponse)
        {
            try
            {
                if (articleResponse.IsError)
                {
                    zendeskErrorUI.NavigateError(null,true);
                    return;
                }
                
                OpenMainHelpCenter();
                zendeskMain.zendeskUI.zendeskSupportUI.CloseSupportUI();
                zendeskUI.AddScreenBackState(articleRenderPanel, ZendeskScreen.ArticleScreen, articleResponse.Result.Article);
                articleRenderPanel.SetActive(true);
                helpCenterScrollView.SetActive(false);
                footerPanel.SetActive(false);
                articleTitle.GetComponent<Text>().text = ClearHtmlText(articleResponse.Result.Article.Title);
                this.articleRenderPanel.GetComponent<ZendeskArticleRender>().RenderArticle(articleResponse.Result.Article);
                zendeskUI.ShowBackButton(backButtonContainer);
            }
            catch
            {
                zendeskErrorUI.NavigateError(null, true);
            }
            finally
            {
                zendeskUI.FinishLoading();
            }
        }

        private void LoadArticles()
        {
            try
            {
                zendeskUI.StartLoading(headerPanel);
                zendeskMain.helpCenterProvider.GetCategories(GetCategoriesCallback);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void GetCategoriesCallback(ZendeskResponse<CategoriesResponse> categories)
        {
            try
            {
                if (categories.IsError)
                {
                    if (categories.ErrorResponse.Status == 404)
                    {
                        zendeskErrorUI.NavigateError(
                            zendeskMain.zendeskLocalizationHandler.translationGameObjects
                                ["usdk_help_center_is_not_enabled"], true,false,"hide");
                        return;
                    }
                    
                    zendeskErrorUI.NavigateError(null,true);
                    zendeskUI.FinishLoading();
                    return;
                }

                categoriesResponse = categories.Result;
                zendeskMain.helpCenterProvider.GetSections(GetSectionsCallback);
            }
            catch (Exception e)
            {
                zendeskUI.FinishLoading();
                throw e;
            }
        }

        private void GetSectionsCallback(ZendeskResponse<SectionsResponse> sections)
        {
            try
            {
                if (sections.IsError)
                {
                    zendeskErrorUI.NavigateError(null,true);
                    return;
                }
                
                zendeskUI.ShowBackButton(backButtonContainer);
                articleRenderPanel.SetActive(false);
                articleListPanel.SetActive(false);
                footerPanel.SetActive(true);
                helpCenterScrollView.SetActive(true);
                
                sectionsResponse = sections.Result;
                RenderMyHelpCenter();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                zendeskUI.FinishLoading();
            }
        }

        public void LoadArticlesFromSection(long sectionId)
        {
            try
            {
                zendeskUI.StartLoading(headerPanel);
                zendeskUI.AddScreenBackState(articleListPanel, ZendeskScreen.ArticleListScreen, sectionId);
                zendeskMain.helpCenterProvider.GetArticlesBySectionId(GetArticlesFromSectionCallback, sectionId);
                zendeskUI.ShowBackButton(backButtonContainer);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void RenderMyHelpCenter()
        {
            try
            {
                ClearExistingHelpCenterPage();
                long prevCategoryId = 0;
                var categories = categoriesResponse.Categories;
                var sections = sectionsResponse.Sections;
                foreach (var category in categories)
                {
                    foreach (var section in sections.FindAll(x => x.CategoryId == category.Id))
                    {
                        string categoryText = ClearHtmlText(category.Name);
                        string sectionName = ClearHtmlText(section.Name);
                        var g = Instantiate(sectionPrefab, helpCenterView.transform);
                        if (prevCategoryId == category.Id)
                        {
                            categoryText = null;
                        }

                        prevCategoryId = category.Id;
                        g.GetComponent<ZendeskArticleListOverview>()
                         .Init(categoryText, sectionName, section.Id, this, zendeskErrorUI);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                zendeskUI.FinishLoading();
            }
        }

        private void GetArticlesFromSectionCallback(ZendeskResponse<ArticlesResponse> articles)
        {
            try
            {
                ClearArticleListPanel();
                articleRenderPanel.SetActive(false);
                helpCenterScrollView.SetActive(false);
                articleListPanel.SetActive(true);
                footerPanel.SetActive(true);
                if (articles.IsError)
                {
                    throw new Exception();
                }

                var articlesList = articles.Result.Articles;
                if (articlesList.Count > 0)
                {
                    foreach (var article in articlesList)
                    {
                        var articleGameObject =
                            Instantiate(articleListContentOverviewPrefab, articleListView.transform);
                        article.Author = articles.Result.Users.Where(a => a.Id == article.AuthorId).FirstOrDefault();
                        articleGameObject.GetComponent<ZendeskArticleUIContainer>().Init(article,
                            zendeskErrorUI, this);
                    }
                }
            }
            catch
            {
                zendeskErrorUI.NavigateError(null,true);
            }
            finally
            {
                zendeskUI.FinishLoading();
            }
        }

        private void ClearExistingHelpCenterPage()
        {
            try
            {
                foreach (Transform child in helpCenterView.transform)
                {
                    GameObject.Destroy(child.gameObject);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void ClearArticleListPanel()
        {
            try
            {
                foreach (Transform child in articleListView.transform)
                {
                    Destroy(child.gameObject);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public string ClearHtmlText(string text)
        {
            return zendeskMain.zendeskHtmlHandler.ClearHtmlText(text);
        }
        
        public void OverrideHelpCenterLocale(string locale)
        {
            zendeskSettings.HelpCenter.Locale = locale;
        }

        private void InitHelpCenter()
        {
            headerTitle.text = zendeskMain.zendeskLocalizationHandler.translationGameObjects["usdk_help_center_label"];
            zendeskUI.ShowBackButton(backButtonContainer);
            articleRenderPanel.SetActive(false);
            articleListPanel.SetActive(false);
            helpCenterScrollView.SetActive(false);
        }
    }
}