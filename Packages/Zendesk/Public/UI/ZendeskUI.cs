﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zendesk.Internal.Models.Common;
using Zendesk.Internal.Models.Core;
using Zendesk.Internal.Models.HelpCenter;
using Zendesk.Internal.Models.Support;
using Zendesk.UI.HelpCenter;

namespace Zendesk.UI
{
    public class ZendeskUI : MonoBehaviour
    {
        #region Private Properties

        private ZendeskErrorUI zendeskErrorUI;
        private ZendeskMain zendeskMain;
        [SerializeField] private Text openSupportString;
        [SerializeField] private Text createRequestString;
        [SerializeField] private Text openHelpCenterString;
        private GameObject loadingBarInstantiated;

        #endregion Private Properties

        #region Public Properties


        [SerializeField]
        public GameObject zendeskCanvas;

        [SerializeField]
        public GameObject zendeskButtonsCanvas;

        [SerializeField]
        public GameObject zendeskSupportUIGO;

        [SerializeField]
        public GameObject zendeskHelpCenterUIGO;

        [SerializeField]
        public bool useAndroidBackButtonViaZendesk;

        [SerializeField]
        public GameObject loadingBar;

        [SerializeField]
        public bool useZendeskUI = true;
        
        [SerializeField]
        public bool useZendeskUIButtons = true;
        
        public List<ZendeskScreenRecord> backStateGO = new List<ZendeskScreenRecord>();

        [HideInInspector]
        public ZendeskSupportUI zendeskSupportUI;

        [HideInInspector]
        public ZendeskHelpCenterUI zendeskHelpCenterUI;

        [HideInInspector]
        public string embededVideoString;

        #endregion Public Properties

        public void InitWithErrorHandler(ZendeskMain zendeskMain, ZendeskErrorUI zendeskErrorUI)
        {
            if (useZendeskUI)
            {
                this.zendeskMain = zendeskMain;
                this.zendeskErrorUI = zendeskErrorUI;
                zendeskSupportUI = zendeskSupportUIGO.GetComponent<ZendeskSupportUI>();
                zendeskSupportUI.InitWithErrorHandler(zendeskMain, this, zendeskErrorUI);
                zendeskHelpCenterUI = zendeskHelpCenterUIGO.GetComponent<ZendeskHelpCenterUI>();
                zendeskHelpCenterUI.InitWithErrorHandler(zendeskMain, this, zendeskErrorUI);
                SetCommonStrings();
                if (!useZendeskUIButtons)
                {
                    Destroy(zendeskButtonsCanvas);
                }
                else
                {
                    SetSampleButtonStrings();
                    zendeskButtonsCanvas.SetActive(true);
                }
            }
            else
            {
                Destroy(zendeskButtonsCanvas);
                Destroy(zendeskCanvas);
                Destroy(this.gameObject.GetComponent<ZendeskErrorUI>());
                Destroy(this);
            }
        }

        public void Init(ZendeskMain zendeskMain, ZendeskSettings zendeskSettings,
            ZendeskErrorUI zendeskErrorUI, string commonTags = "")
        {
            try
            {
                if (useZendeskUI)
                {
                    this.zendeskErrorUI = zendeskErrorUI;
                    zendeskSupportUI = zendeskSupportUIGO.GetComponent<ZendeskSupportUI>();
                    zendeskSupportUI.Init(zendeskMain, this, zendeskSettings, zendeskErrorUI, commonTags);
                    zendeskHelpCenterUI = zendeskHelpCenterUIGO.GetComponent<ZendeskHelpCenterUI>();
                    zendeskHelpCenterUI.Init(zendeskMain, this, zendeskSettings, zendeskErrorUI);
                    SetCommonStrings();
                    if (!useZendeskUIButtons)
                    {
                        Destroy(zendeskButtonsCanvas);
                    }
                    else
                    {
                        SetSampleButtonStrings();
                        zendeskButtonsCanvas.SetActive(true);
                    }
                }
                else
                {
                    Destroy(zendeskCanvas);
                    Destroy(zendeskButtonsCanvas);
                    Destroy(this.gameObject.GetComponent<ZendeskErrorUI>());
                    Destroy(this);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void BackButton()
        {
            try
            {
                if (backStateGO.Count <= 1)
                {
                    ZendeskInitCheck();
                    backStateGO.Clear();
                    zendeskErrorUI.zendeskErrorUIGO.SetActive(false);
                    return;
                }

                var itemHide = backStateGO[backStateGO.Count - 1];
                ScreenControlsBeforeClose(itemHide);
                if (!ZendeskInitCheck())
                {
                    backStateGO.Remove(itemHide);
                    return;
                }

                var itemShow = backStateGO[backStateGO.Count - 2];
                zendeskErrorUI.DestroyZendeskErrorToast();
                backStateGO.Remove(itemHide);
                backStateGO.Remove(itemShow);
                zendeskErrorUI.zendeskErrorUIGO.SetActive(false);
                if (itemHide.ZendeskScreen.ScreenType != itemShow.ZendeskScreen.ScreenType)
                {
                    if (itemHide.ZendeskScreen.ScreenType == ScreenType.Support)
                    {
                        zendeskSupportUIGO.SetActive(false);
                    }
                    else if (itemHide.ZendeskScreen.ScreenType == ScreenType.HelpCenter)
                    {
                        zendeskHelpCenterUIGO.SetActive(false);
                    }
                }

                string parameterString = "";
                if (itemShow.Parameter != null)
                {
                    parameterString = itemShow.Parameter.ToString();
                }

                switch (itemShow.ZendeskScreen.Screen)
                {
                    case Screen.CreateTicket:
                        zendeskSupportUIGO.SetActive(true);
                        zendeskSupportUI.SubmitNewRequest(parameterString);
                        break;
                    case Screen.TicketResponse:
                        zendeskSupportUIGO.SetActive(true);
                        Request req = null;
                        if (itemShow.Parameter != null)
                        {
                            if (!string.IsNullOrEmpty(parameterString))
                            {
                                req = new Request();
                                req.Id = parameterString;
                            }
                        }

                        zendeskSupportUI.RetrieveTicketResponseData((Request) itemShow.Parameter, true, false);
                        break;
                    case Screen.MyTickets:
                        zendeskSupportUIGO.SetActive(true);
                        zendeskSupportUI.BackToMyTicketsScreen();
                        break;
                    case Screen.MyHelpCenter:
                        zendeskHelpCenterUIGO.SetActive(true);
                        zendeskHelpCenterUI.ShowArticlesWindowCached();
                        break;
                    case Screen.Article:
                        zendeskHelpCenterUIGO.SetActive(true);
                        Article article = null;
                        if (itemShow != null && itemShow.Parameter != null)
                        {
                            article = (Article) itemShow.Parameter;
                        }

                        zendeskHelpCenterUI.LoadIndividualArticle(article);
                        break;
                    case Screen.ArticleList:
                        zendeskHelpCenterUIGO.SetActive(true);
                        long sectionId = (long) itemShow.Parameter;
                        zendeskHelpCenterUI.LoadArticlesFromSection(sectionId);
                        break;
                }
            }
            catch
            {
                zendeskErrorUI.NavigateError(null, true);
            }
        }

        public void AddScreenBackState(GameObject goScreen, ZendeskScreen screenEnum, object parameter = null)
        {
            try
            {
                backStateGO.Add(new ZendeskScreenRecord()
                    {ScreenGO = goScreen, ZendeskScreen = screenEnum, Parameter = parameter});
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void RefreshPage()
        {
            try
            {
                if (Application.internetReachability == NetworkReachability.NotReachable)
                {
                    zendeskErrorUI.NavigateError(null, true);
                    return;
                }

                AddScreenBackState(null, ZendeskScreen.Empty);
                BackButton();
            }
            catch
            {
                zendeskErrorUI.NavigateError(null, true);
            }
        }

        private void ScreenControlsBeforeClose(ZendeskScreenRecord zendeskScreenRecord)
        {
            zendeskSupportUI.ticketConfirmationMessageCoroutines.Clear();
            if (zendeskScreenRecord != null && zendeskScreenRecord.ZendeskScreen != null)
            {
                switch (zendeskScreenRecord.ZendeskScreen.Screen)
                {
                    case Screen.CreateTicket:
                        HideAllValidationsInCreateRequestForm();
                        break;
                }
            }
        }

        public void CloseButton()
        {
            try
            {
                zendeskErrorUI.DestroyZendeskErrorToast();
                foreach (var zendeskScreenRecord in backStateGO)
                {
                    
                    
                    ScreenControlsBeforeClose(zendeskScreenRecord);
                }

                backStateGO.Clear();
                zendeskHelpCenterUIGO.SetActive(false);
                zendeskSupportUIGO.SetActive(false);
                zendeskErrorUI.zendeskErrorUIGO.SetActive(false);
                if (zendeskCanvas.activeSelf)
                {
                    zendeskCanvas.SetActive(false);
                }

                if (useZendeskUI && useZendeskUIButtons)
                {
                    zendeskButtonsCanvas.SetActive(true);
                }
            }
            catch
            {
                zendeskErrorUI.NavigateError(null, true);
            }
        }

        private void Update()
        {
            if (useAndroidBackButtonViaZendesk && Application.platform == RuntimePlatform.Android)
            {
                if (Input.GetKeyDown(KeyCode.Escape) && backStateGO.Count > 1)
                {
                    BackButton();
                }
                else if (Input.GetKeyDown(KeyCode.Escape) && backStateGO.Count == 1)
                {
                    CloseButton();
                }
            }
        }

        public void ShowBackButton(GameObject backButtonContainer)
        {
            try
            {
                if (backStateGO.Count > 1)
                {
                    backButtonContainer.SetActive(true);
                }
                else
                {
                    backButtonContainer.SetActive(false);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void SetCommonStrings()
        {
            embededVideoString =
                zendeskMain.zendeskLocalizationHandler.translationGameObjects["usdk_help_center_article_view_media"];
        }

        private void SetSampleButtonStrings()
        {
            if (openSupportString != null)
                openSupportString.text =
                    zendeskMain.zendeskLocalizationHandler.translationGameObjects["usdk_sample_button_support"];
            if (createRequestString != null)
                createRequestString.text =
                    zendeskMain.zendeskLocalizationHandler.translationGameObjects["usdk_get_in_touch_label_saddle_bar"];
            if (openHelpCenterString != null)
                openHelpCenterString.text =
                    zendeskMain.zendeskLocalizationHandler.translationGameObjects["usdk_sample_button_helpcenter"];
        }

        public void StartLoading(GameObject target)
        {
            try
            {
                if (loadingBarInstantiated != null)
                {
                    Destroy(loadingBarInstantiated);
                }

                loadingBarInstantiated = Instantiate(loadingBar, zendeskCanvas.transform);
                loadingBarInstantiated.SetActive(true);
            }
            catch
            {
            }
        }

        public void FinishLoading()
        {
            try
            {
                if (loadingBarInstantiated != null)
                {
                    if (loadingBarInstantiated.GetComponent<Animator>() != null &&
                        loadingBarInstantiated.GetComponent<Animator>().isActiveAndEnabled)
                    {
                        loadingBarInstantiated.GetComponent<Animator>().SetBool("isLoading", false);
                        if (loadingBarInstantiated
                            .GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("FinishLoading"))
                        {
                            Destroy(loadingBarInstantiated);
                        }
                    }
                    else
                    {
                        Destroy(loadingBarInstantiated);
                    }
                }
            }
            catch
            {
                Destroy(loadingBarInstantiated);
            }
        }

        private void HideAllValidationsInCreateRequestForm()
        {
            zendeskSupportUI.createRequestPanelEmailValidation.SetActive(false);
            zendeskSupportUI.createRequestPanelNameValidation.SetActive(false);
            zendeskSupportUI.createRequestPanelMessageValidation.SetActive(false);
        }

        public void OpenHelpCenter()
        {
            zendeskHelpCenterUI.OpenHelpCenter();
        }

        public void OpenSupport(string tags = "")
        {
            zendeskSupportUI.OpenSupportPanel(tags);   
        }

        public void OpenCreateRequest(string tags = "")
        {
            zendeskSupportUI.SubmitNewRequest(tags);
        }
        
        private bool ZendeskInitCheck()
        {
            if (zendeskMain.InitialisationStatus != InitialisationStatus.Initialised)
            {
                zendeskErrorUI.zendeskFullScreenErrorBackButtonContainer.GetComponent<Button>().interactable = false;
                zendeskMain.InitImmediate();
                return false;
            }
        
            return true;
        }
    }
}