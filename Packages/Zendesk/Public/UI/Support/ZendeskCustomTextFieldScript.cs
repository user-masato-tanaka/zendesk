﻿using System;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;
using Zendesk.Common;
using Zendesk.Internal.Models.Common;
using Zendesk.Internal.Models.Support;
using Zendesk.Providers;

namespace Zendesk.UI
{
    public class ZendeskCustomTextFieldScript : ZendeskGenericComponentScript
    {
        private Color redOutline;
        private Color greyOutline;
        public InputField inputField;
        public Text validationMessageComponent;
        public GameObject validationPanel;
//        public Text placeholderComponent;
        public bool required;
        private void Start()
        {
            if(inputField != null)
                inputField.onEndEdit.AddListener(delegate{ ValidateCustomField(inputField); });
            ColorUtility.TryParseHtmlString("#D8DCDE", out greyOutline);
            ColorUtility.TryParseHtmlString("#CC3340", out redOutline);
        }
        
        public void init(long id, string displayText, string validationMessage, string placeholder, bool requiredField, string defaultValidationMessage)
        {
            idCustomField = id;
            labelComponent.text = displayText;
            if (string.IsNullOrEmpty(validationMessage))
            {
                validationMessage = defaultValidationMessage;
            }
            validationMessageComponent.text = validationMessage;
//            placeholderComponent.text = placeholder;
            required = requiredField;
        }
        
        // Checks if there is anything entered into the input field.
        public void ValidateCustomField(InputField selectedCustomField)
        {
            if (this.required)
            {
                var customFieldValue = selectedCustomField.text.Trim();

                if (string.IsNullOrEmpty(customFieldValue) && customFieldValue.Length == 0)
                {
                    selectedCustomField.Select();
                    selectedCustomField.GetComponentInParent<Image>().color = redOutline;
                    if (!string.IsNullOrEmpty(validationMessageComponent.text))
                    {
                        this.validationPanel.SetActive(true);    
                    }
                }
                else
                {
                    selectedCustomField.GetComponentInParent<Image>().color = greyOutline;
                    this.validationPanel.SetActive(false);
                }
            }
        }
    }
}