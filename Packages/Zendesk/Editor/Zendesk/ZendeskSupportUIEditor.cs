using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Zendesk.Internal.Models.Support;

namespace Zendesk.UI
{
    [CustomEditor(typeof(ZendeskSupportUI))]
    public class ZendeskSupportUIEditor : Editor
    {
        int tab = 0;
        private bool myTicketsUIComponents = true, myTicketsGameObjects = true, newTicketUIComponents = true, newTicketGameObjects = true, conversationalUIComponents = true, conversationalTicketGameObjects = true, customFieldsGameObjects = false;
        public override void OnInspectorGUI()
        {
            ZendeskSupportUI myScript = (ZendeskSupportUI)target;
            if (myScript != null)
            {
                tab = GUILayout.Toolbar (tab, new string[] {"Common","My Tickets", "Conversational", "New Request Form", "Custom Fields"});
                switch (tab) {
                    case 0:
                    {
                        myScript.headerTitle = (Text) EditorGUILayout.ObjectField("Header Title Text", myScript.headerTitle, typeof(Text), true);
                        myScript.backButtonContainer = (GameObject) EditorGUILayout.ObjectField("Back Button Container", myScript.backButtonContainer, typeof(GameObject), true);
                        myScript.keyboardBackground = (GameObject) EditorGUILayout.ObjectField("Keyboard Background", myScript.keyboardBackground, typeof(GameObject), true);
                        myScript.keyboardBackgroundColor = EditorGUILayout.ColorField("Keyboard Background Color", new Color32(47, 57, 65, 206)); 
                        myScript.headerPanel = (GameObject) EditorGUILayout.ObjectField("Header Panel", myScript.headerPanel, typeof(GameObject), true); 
                        break;
                    }
                    case 1:
                    {
                        myTicketsUIComponents = EditorGUILayout.Foldout(myTicketsUIComponents,"UI Components");
                        if(myTicketsUIComponents)
                        {
                            myScript.noTicketsPanelGeneralInfoText = (Text) EditorGUILayout.ObjectField("No Tickets - General Info", myScript.noTicketsPanelGeneralInfoText, typeof(Text), true);
                            myScript.noTicketsPanelStartConversationText = (Text) EditorGUILayout.ObjectField("No Tickets - Start Conversation", myScript.noTicketsPanelStartConversationText, typeof(Text), true);
                            myScript.myTicketsFooterNeedHelpText = (Text) EditorGUILayout.ObjectField("My Tickets - Need Help", myScript.myTicketsFooterNeedHelpText, typeof(Text), true);
                            myScript.myTicketsFooterContactUsButtonText = (Text) EditorGUILayout.ObjectField("My Tickets - Contact Us", myScript.myTicketsFooterContactUsButtonText, typeof(Text), true);
                        }
                        myTicketsGameObjects = EditorGUILayout.Foldout(myTicketsGameObjects,"Game Objects");
                        if(myTicketsGameObjects)
                        {
                            myScript.noTicketsPanel = (GameObject) EditorGUILayout.ObjectField("No Tickets Panel", myScript.noTicketsPanel, typeof(GameObject), true);
                            myScript.noTicketsFooter = (GameObject) EditorGUILayout.ObjectField("No Tickets Footer Panel", myScript.noTicketsFooter, typeof(GameObject), true);
                            myScript.myTicketsPanel = (GameObject) EditorGUILayout.ObjectField("My Tickets Panel", myScript.myTicketsPanel, typeof(GameObject), true);
                            myScript.myTicketsFooter = (GameObject) EditorGUILayout.ObjectField("My Tickets Footer Panel", myScript.myTicketsFooter, typeof(GameObject), true);
                            myScript.ticketListView = (GameObject) EditorGUILayout.ObjectField("Ticket List View", myScript.ticketListView, typeof(GameObject), true);
                            myScript.solvedConversationPrefab = (GameObject) EditorGUILayout.ObjectField("Solved Conversation Prefab", myScript.solvedConversationPrefab, typeof(GameObject), true);
                            myScript.ticketContainerPrefab = (GameObject) EditorGUILayout.ObjectField("Ticket Container Prefab", myScript.ticketContainerPrefab, typeof(GameObject), true);
                        }
                        break;
                    }
                    case 2:
                    {
                        conversationalUIComponents = EditorGUILayout.Foldout(conversationalUIComponents,"UIComponents");
                        if(conversationalUIComponents)
                        {
                            myScript.typeMessageText = (Text) EditorGUILayout.ObjectField("Type Message", myScript.typeMessageText, typeof(Text), true);
                            myScript.conversationalFooterNeedHelpText = (Text) EditorGUILayout.ObjectField("Conversational - Contact Us", myScript.conversationalFooterNeedHelpText, typeof(Text), true);
                            myScript.ticketResponseAttachmentCountText = (Text) EditorGUILayout.ObjectField("Attachment Count Text", myScript.ticketResponseAttachmentCountText, typeof(Text), true);
                            myScript.ticketResponseFooterContactUsButtonText = (Text) EditorGUILayout.ObjectField("Send Request Text", myScript.ticketResponseFooterContactUsButtonText, typeof(Text), true);

                        }
                        conversationalTicketGameObjects = EditorGUILayout.Foldout(conversationalTicketGameObjects,"Game Objects");
                        if(conversationalTicketGameObjects)
                        {
                            myScript.ticketResponsePanel = (GameObject) EditorGUILayout.ObjectField("Ticket Response Panel", myScript.ticketResponsePanel, typeof(GameObject), true);
                            myScript.attachmentsView = (GameObject) EditorGUILayout.ObjectField("Attachments View", myScript.attachmentsView, typeof(GameObject), true);
                            myScript.attachmentButton = (GameObject) EditorGUILayout.ObjectField("Attachment Button", myScript.attachmentButton, typeof(GameObject), true);
                            myScript.sendCommentButton = (GameObject) EditorGUILayout.ObjectField("Send Comment Button", myScript.sendCommentButton, typeof(GameObject), true);
                            myScript.addCommentButtonImage = (GameObject) EditorGUILayout.ObjectField("Add Comment Button Image", myScript.addCommentButtonImage, typeof(GameObject), true);
                            myScript.newCommentEndUser = (GameObject) EditorGUILayout.ObjectField("New Comment End User", myScript.newCommentEndUser, typeof(GameObject), true);
                            myScript.createRequestAttachmentLine = (GameObject) EditorGUILayout.ObjectField("Attachment Line", myScript.createRequestAttachmentLine, typeof(GameObject), true);
                            myScript.footerLineMain = (GameObject) EditorGUILayout.ObjectField("Footer Panel", myScript.footerLineMain, typeof(GameObject), true);
                            myScript.footerClosedStatus = (GameObject) EditorGUILayout.ObjectField("Footer for Closed Ticket", myScript.footerClosedStatus, typeof(GameObject), true);
                            myScript.ticketResponseListView = (GameObject) EditorGUILayout.ObjectField("Ticket Response List View", myScript.ticketResponseListView, typeof(GameObject), true);
                            myScript.ticketResponseScrollView = (GameObject) EditorGUILayout.ObjectField("Ticket Response Scroll View", myScript.ticketResponseScrollView, typeof(GameObject), true);
                            myScript.attachmentPrefab = (GameObject) EditorGUILayout.ObjectField("Attachment Prefab", myScript.attachmentPrefab, typeof(GameObject), true);
                            myScript.agentResponsePrefab = (GameObject) EditorGUILayout.ObjectField("Agent Response Prefab", myScript.agentResponsePrefab, typeof(GameObject), true);
                            myScript.endUserResponsePrefab = (GameObject) EditorGUILayout.ObjectField("End User Response Prefab", myScript.endUserResponsePrefab, typeof(GameObject), true);
                            myScript.messageDatePrefab = (GameObject) EditorGUILayout.ObjectField("Message Date Prefab", myScript.messageDatePrefab, typeof(GameObject), true);
                            myScript.endOfConversationPrefab = (GameObject) EditorGUILayout.ObjectField("End Of Conversation Prefab", myScript.endOfConversationPrefab, typeof(GameObject), true);
                            myScript.satisfactionRatingPrefab = (GameObject) EditorGUILayout.ObjectField("Satisfaction Rating Prefab", myScript.satisfactionRatingPrefab, typeof(GameObject), true);
                            myScript.answerBotResponsePrefab = (GameObject) EditorGUILayout.ObjectField("Anwser Bot Prefab", myScript.answerBotResponsePrefab, typeof(GameObject), true);
                            myScript.ticketConfirmationMessagePrefab = (GameObject) EditorGUILayout.ObjectField("Confirmation Message Prefab", myScript.ticketConfirmationMessagePrefab, typeof(GameObject), true);
                            myScript.ticketResponseAttachmentCountContainer = (GameObject) EditorGUILayout.ObjectField("Attachment Count Container", myScript.ticketResponseAttachmentCountContainer, typeof(GameObject), true);
                        }
                        break;
                    }
                    case 3:
                    {
                        newTicketUIComponents = EditorGUILayout.Foldout(newTicketUIComponents,"UI Components");
                        if(newTicketUIComponents)
                        {
                            myScript.createRequestPanelNameText = (Text) EditorGUILayout.ObjectField("Name Text", myScript.createRequestPanelNameText, typeof(Text), true);
                            myScript.createRequestPanelEmailText = (Text) EditorGUILayout.ObjectField("Email Text", myScript.createRequestPanelEmailText, typeof(Text), true);
                            myScript.createRequestPanelMessageText = (Text) EditorGUILayout.ObjectField("Message Text", myScript.createRequestPanelMessageText, typeof(Text), true);
                            myScript.createRequestPanelInvalidNameText = (Text) EditorGUILayout.ObjectField("Invalid Name Text", myScript.createRequestPanelInvalidNameText, typeof(Text), true);
                            myScript.createRequestPanelInvalidEmailText = (Text) EditorGUILayout.ObjectField("Invalid Email Text", myScript.createRequestPanelInvalidEmailText, typeof(Text), true);
                            myScript.createRequestPanelInvalidMessageText = (Text) EditorGUILayout.ObjectField("Invalid Message Text", myScript.createRequestPanelInvalidMessageText, typeof(Text), true);
                            myScript.createRequestPanelInputName = (InputField) EditorGUILayout.ObjectField("Name Input Field", myScript.createRequestPanelInputName, typeof(InputField), true);
                            myScript.createRequestPanelInputEmail = (InputField) EditorGUILayout.ObjectField("Email Input Field", myScript.createRequestPanelInputEmail, typeof(InputField), true);
                            myScript.createRequestPanelInputMessage = (InputField) EditorGUILayout.ObjectField("Message Input Field", myScript.createRequestPanelInputMessage, typeof(InputField), true);
                            myScript.createRequestFooterPanelSendButton = (Button) EditorGUILayout.ObjectField("Message Send Button", myScript.createRequestFooterPanelSendButton, typeof(Button), true);
                            myScript.createRequestFooterPanelAttachmentButton = (Button) EditorGUILayout.ObjectField("Message Attachment Button", myScript.createRequestFooterPanelAttachmentButton, typeof(Button), true);
                            myScript.createRequestPanelFeedbackPanelTitle = (Text) EditorGUILayout.ObjectField("Feedback Panel Title", myScript.createRequestPanelFeedbackPanelTitle, typeof(Text), true);
                            myScript.createRequestPanelFeedbackPanelDescription = (Text) EditorGUILayout.ObjectField("Feedback Panel Description", myScript.createRequestPanelFeedbackPanelDescription, typeof(Text), true);
                            myScript.createRequestPanelFeedbackPanelButtonText = (Text) EditorGUILayout.ObjectField("Feedback Panel Button Text", myScript.createRequestPanelFeedbackPanelButtonText, typeof(Text), true);
                            myScript.createRequestAttachmentCountText = (Text) EditorGUILayout.ObjectField("Attachment Count Text", myScript.createRequestAttachmentCountText, typeof(Text), true);
                        }
                        
                        newTicketGameObjects = EditorGUILayout.Foldout(newTicketGameObjects,"Game Objects");
                        if(newTicketGameObjects)
                        {
                            myScript.createRequestPanel = (GameObject) EditorGUILayout.ObjectField("Create Request Panel", myScript.createRequestPanel, typeof(GameObject), true);
                            myScript.createRequestScrollView = (GameObject) EditorGUILayout.ObjectField("Create Request Panel Scroll View", myScript.createRequestScrollView, typeof(GameObject), true);
                            myScript.createRequestPanelFeedbackToast = (GameObject) EditorGUILayout.ObjectField("Create Request Panel Feedback Toast", myScript.createRequestPanelFeedbackToast, typeof(GameObject), true);
                            myScript.createRequestPanelFeedbackPanel = (GameObject) EditorGUILayout.ObjectField("Create Request Panel Feedback Panel", myScript.createRequestPanelFeedbackPanel, typeof(GameObject), true);
                            myScript.createRequestPanelFieldContainer = (GameObject) EditorGUILayout.ObjectField("Create Request Panel Field Container", myScript.createRequestPanelFieldContainer, typeof(GameObject), true);
                            myScript.createRequestPanelNameContainer = (GameObject) EditorGUILayout.ObjectField("Create Request Name Container", myScript.createRequestPanelNameContainer, typeof(GameObject), true);
                            myScript.createRequestPanelEmailContainer = (GameObject) EditorGUILayout.ObjectField("Create Request Email Container", myScript.createRequestPanelEmailContainer, typeof(GameObject), true);
                            myScript.createRequestPanelMessageContainer = (GameObject) EditorGUILayout.ObjectField("Create Request Message Container", myScript.createRequestPanelMessageContainer, typeof(GameObject), true);
                            myScript.createRequestPanelNameValidation = (GameObject) EditorGUILayout.ObjectField("Name Validation", myScript.createRequestPanelNameValidation, typeof(GameObject), true);
                            myScript.createRequestPanelEmailValidation = (GameObject) EditorGUILayout.ObjectField("Email Validation", myScript.createRequestPanelEmailValidation, typeof(GameObject), true);
                            myScript.createRequestPanelMessageValidation = (GameObject) EditorGUILayout.ObjectField("Message Validation", myScript.createRequestPanelMessageValidation, typeof(GameObject), true);
                            myScript.createRequestPanelAttachmentLine = (GameObject) EditorGUILayout.ObjectField("Attachment Line", myScript.createRequestPanelAttachmentLine, typeof(GameObject), true);
                            myScript.attachmentsViewNonConversational = (GameObject) EditorGUILayout.ObjectField("Attachments View", myScript.attachmentsViewNonConversational, typeof(GameObject), true);
                            myScript.createRequestAttachmentCountContainer = (GameObject) EditorGUILayout.ObjectField("Attachment Count Container", myScript.createRequestAttachmentCountContainer, typeof(GameObject), true);
                        }
                     
                        break;
                    }

                    case 4:
                    {
                        var style = new GUIStyle(GUI.skin.button);
                        style.fixedWidth = 200;

                        var customFieldTranslations = serializedObject.FindProperty("customFieldTranslations");
                        var customFieldsFile = serializedObject.FindProperty("customFieldsFile");
                        var requestForm = serializedObject.FindProperty("requestForm");
                        EditorGUILayout.PropertyField(customFieldTranslations, true);
                        EditorGUILayout.PropertyField(customFieldsFile, true);

                        GUILayout.Space(10);
                        GUILayout.BeginHorizontal();
                        GUILayout.FlexibleSpace();
                        if (GUILayout.Button("Load Custom Fields from CSV File", style))
                        {
                            myScript.LoadCustomFieldsFromCSV();
                        }

                        if (GUILayout.Button("Clear Custom Fields", style))
                        {
                            myScript.requestForm = new RequestForm();
                        }

                        if (GUILayout.Button("Add Custom Field", style))
                        {
                            if (myScript.requestForm == null)
                            {
                                myScript.requestForm = new RequestForm();
                            }

                            if (myScript.requestForm.Fields == null)
                            {
                                myScript.requestForm.Fields = new List<RequestFormField>();
                            }

                            myScript.requestForm.Fields.Add(new RequestFormField());
                        }

                        GUILayout.FlexibleSpace();
                        GUILayout.EndHorizontal();
                        GUILayout.Space(10);

                        EditorGUILayout.HelpBox(
                            myScript.customFieldsWarning,
                            MessageType.Info);
                        GUILayout.Space(10);

                        EditorGUILayout.PropertyField(requestForm, true);
                        serializedObject.ApplyModifiedProperties();

                        customFieldsGameObjects =
                            EditorGUILayout.Foldout(customFieldsGameObjects, "Custom Fields Prefabs");
                        if (customFieldsGameObjects)
                        {
                            myScript.CustomTextFieldPrefab = (GameObject) EditorGUILayout.ObjectField("Text Field Prefab", myScript.CustomTextFieldPrefab, typeof(GameObject), true);
                            myScript.CustomTextMultilineFieldPrefab = (GameObject) EditorGUILayout.ObjectField("Text Multiline Field Prefab", myScript.CustomTextMultilineFieldPrefab, typeof(GameObject), true);
                            myScript.CustomCheckboxFieldPrefab = (GameObject) EditorGUILayout.ObjectField("Checkbox Field Prefab", myScript.CustomCheckboxFieldPrefab, typeof(GameObject), true);
                            myScript.CustomNumericFieldPrefab = (GameObject) EditorGUILayout.ObjectField("Numeric Field Prefab", myScript.CustomNumericFieldPrefab, typeof(GameObject), true);
                            myScript.CustomDecimalFieldPrefab = (GameObject) EditorGUILayout.ObjectField("Decimal Field Prefab", myScript.CustomDecimalFieldPrefab, typeof(GameObject), true);
                        }

                        break;
                    }
                }
                EditorUtility.SetDirty (myScript);
            }
            if (EditorApplication.isPlaying)
                Repaint();

        }

    }
}