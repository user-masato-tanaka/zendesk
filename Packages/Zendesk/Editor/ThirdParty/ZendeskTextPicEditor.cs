using UnityEditor;

namespace UnityEngine.UI.Extensions
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(ZendeskTextPic))]
    public class ZendeskTextPicEditor : UnityEditor.UI.TextEditor
    {

        private SerializedProperty iconList;

        protected override void OnEnable()
        {
            base.OnEnable();
            iconList = serializedObject.FindProperty("inspectorIconList");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            serializedObject.Update();
            EditorGUILayout.PropertyField(iconList, new GUIContent("Icon List"), true);
            serializedObject.ApplyModifiedProperties();
        }
    }
}