﻿using UnityEditor;
using Zendesk.Common;

public class ZendeskMenu : EditorWindow 
{
	[MenuItem ("Zendesk/Clear Local Support Storage")]
	public static void ClearLocalSupportStorage() {
		ZendeskLocalStorage.ClearLocalSupportStorage();
	}
	
	[MenuItem ("Zendesk/Clear Local User Info")]
	public static void ClearLocalUserInfo() {
		ZendeskLocalStorage.ClearLocalUserInfo(true);
	}
	
	[MenuItem ("Zendesk/Clear SDK Settings")]
	public static void ClearSDKSettings() {
		ZendeskLocalStorage.ClearSDKSettings();
	}
}

