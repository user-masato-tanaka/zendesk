## Overview

The Zendesk SDK for Unity provides a way to integrate Zendesk support capabilities natively into your Unity project.
For more information, see the [full documentation](https://developer.zendesk.com/embeddables/docs/unity-native-sdk/overview).

The SDK lets users submit support requests to your customer support team within your mobile app. See [Support requests](https://developer.zendesk.com/embeddables/docs/unity-native-sdk/support). It also lets users look for answers on their own in your Help Center within the app. See [Help Center](https://developer.zendesk.com/embeddables/docs/unity-native-sdk/help_center).

### Prerequisites

* An active Zendesk account with permission to add and edit Mobile SDKs in the Support admin interface. If you're not a Zendesk admin, you can ask one to help you.
* Unity version 2018.4 or later.

### Getting started

See [Getting started](https://developer.zendesk.com/embeddables/docs/unity-native-sdk/getting_started).

### Known issues

See [Known issues](https://developer.zendesk.com/embeddables/docs/unity-native-sdk/known_issues).
