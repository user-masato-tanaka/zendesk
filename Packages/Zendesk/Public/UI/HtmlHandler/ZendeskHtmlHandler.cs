﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using UnityEngine;
using Zendesk.UI;

public class ZendeskHtmlHandler : MonoBehaviour
{
    #region Properties

    private ZendeskLinkHandler linkHandler;
    private string lastText = "";
    private List<ZendeskHtmlComponent> zendeskHtmlComponents = new List<ZendeskHtmlComponent>();
    private ZendeskUI zendeskUI;
    private HtmlDocument htmlDoc;
    private int line = 0;

    #endregion

    private void Start()
    {
        linkHandler = GetComponent<ZendeskLinkHandler>();
        zendeskUI = GetComponent<ZendeskUI>();
    }

    private void Init(string html)
    {
        zendeskHtmlComponents = new List<ZendeskHtmlComponent>();
        htmlDoc = new HtmlDocument();
        htmlDoc.LoadHtml(ParseHtmlInitial(html));
        lastText = "";
    }

    #region Parsers

    public List<ZendeskHtmlComponent> ParseHtml(string html)
    {
        try
        {
            Init(html);
            foreach (HtmlNode item in htmlDoc.DocumentNode.Descendants())
            {
                switch (item.NodeType.ToString().ToUpper())
                {
                    case "TEXT":
                        if (item.InnerText.ToLower() == "\n")
                        {
                            AddLineBreak();
                            break;
                        }

                        if (!string.IsNullOrEmpty(item.InnerText) && !lastText.Equals(item.InnerText) &&
                            (!PreviousTextIsSameLinkText(lastText, ClearHtmlText(item.InnerText)) &&
                             !PreviousTextContains(lastText, ClearHtmlText(item.InnerText), item.ParentNode.Name)))
                        {
                            if (item.ParentNode != null)
                            {
                                if (item.ParentNode.Name.ToUpper().Equals("LI"))
                                {
                                    break;
                                }
                            }
                            if (item.ParentNode.ParentNode != null)
                            {
                                if (item.ParentNode.ParentNode.Name.ToUpper().Equals("LI"))
                                {
                                    break;
                                }
                            }
                            AddText(item.InnerText);
                        }

                        break;
                    case "ELEMENT":
                        switch (item.Name.ToUpper())
                        {
                            case "IFRAME":
                                foreach (var attribute in item.Attributes)
                                {
                                    if (attribute.Name.ToUpper().Equals("SRC"))
                                    {
                                        string videoURL = GetVideoURL(attribute.Value);
                                        string linkText = zendeskUI.embededVideoString + " (extlink) ";
                                        AddLink(linkText, videoURL);
                                    }
                                }

                                break;
                            case "A":
                                foreach (var attribute in item.Attributes)
                                {
                                    if (attribute.Name.ToUpper().Equals("HREF"))
                                    {
                                        string linkText = "";
                                        if (item.ParentNode != null)
                                        {
                                            if (!item.ParentNode.Name.ToUpper().Equals("LI"))
                                            {
                                                if (!PreviousTextIsSameLinkText(lastText,
                                                    ClearHtmlText(item.InnerText)))
                                                {
                                                    linkText += ClearHtmlText(item.InnerText);
                                                    if (!linkHandler.IsInternalLink(attribute.Value))
                                                    {
                                                        linkText += " (extlink) ";
                                                    }

                                                    AddLink(linkText, attribute.Value);
                                                }
                                                else
                                                {
                                                    try
                                                    {
                                                        zendeskHtmlComponents.RemoveAll(a =>
                                                            a.text == ClearHtmlText(item.InnerText));
                                                        linkText += ClearHtmlText(item.InnerText);
                                                        if (!linkHandler.IsInternalLink(attribute.Value))
                                                        {
                                                            linkText += " (extlink) ";
                                                        }

                                                        AddLink(linkText, attribute.Value);
                                                    }
                                                    catch
                                                    {
                                                        break;
                                                    }
                                                }

                                                break;
                                            }
                                        }
                                        else
                                        {
                                            if (!PreviousTextIsSameLinkText(lastText, ClearHtmlText(item.InnerText)))
                                            {
                                                linkText += ClearHtmlText(item.InnerText);
                                                if (!linkHandler.IsInternalLink(attribute.Value))
                                                {
                                                    linkText += " (extlink) ";
                                                }

                                                AddLink(linkText, attribute.Value);
                                            }

                                            break;
                                        }
                                    }
                                }

                                break;
                            case "STRONG":
                            case "B":
                                string newTextBold = item.InnerHtml;
                                if (PreviousTextContains(lastText, ClearHtmlText(newTextBold), "b"))
                                {
                                }
                                else if (!PreviousTextIsSameLinkText(lastText, ClearHtmlText(newTextBold)) ||
                                         (PreviousTextIsSameLinkText(lastText, ClearHtmlText(newTextBold)) &&
                                          line != item.Line))
                                {
                                    if (!item.ParentNode.Name.ToUpper().Equals("LI") && !item.ParentNode.Name.ToUpper().Equals("H1") && !item.ParentNode.Name.ToUpper().Equals("H2") && !item.ParentNode.Name.ToUpper().Equals("H3") && !item.ParentNode.Name.ToUpper().Equals("H4") && !item.ParentNode.Name.ToUpper().Equals("H5"))
                                    {
                                        AddText("<b>" + ClearHtmlText(newTextBold) + "</b>");
                                    }
                                }
                                else
                                {
                                    var zHtmlComponent = zendeskHtmlComponents
                                                         .Where(a => !string.IsNullOrEmpty(a.text) &&
                                                                     a.text.Contains(ClearHtmlText(newTextBold)))
                                                         .FirstOrDefault();
                                    zHtmlComponent.text = "<b>" + ClearHtmlText(zHtmlComponent.text) + "</b>";
                                    lastText = zHtmlComponent.text;
                                }

                                this.line = item.Line;
                                break;
                            case "I":
                                string newTextItalics = item.InnerHtml; 
                                if (PreviousTextContains(lastText, ClearHtmlText(newTextItalics), "i"))
                                {
                                }

                                if (!PreviousTextIsSameLinkText(lastText, ClearHtmlText(newTextItalics)) ||
                                    (PreviousTextIsSameLinkText(lastText, ClearHtmlText(newTextItalics)) &&
                                     line != item.Line))
                                {
                                    if (!item.ParentNode.Name.ToUpper().Equals("LI") && !item.ParentNode.Name.ToUpper().Equals("H1") && !item.ParentNode.Name.ToUpper().Equals("H2") && !item.ParentNode.Name.ToUpper().Equals("H3") && !item.ParentNode.Name.ToUpper().Equals("H4") && !item.ParentNode.Name.ToUpper().Equals("H5"))
                                    {
                                        AddText("<i>" + ClearHtmlText(newTextItalics) + "</i>");
                                    }
                                }
                                else
                                {
                                    var zHtmlComponent = zendeskHtmlComponents
                                                         .Where(a => !string.IsNullOrEmpty(a.text) &&
                                                                     a.text.Contains(ClearHtmlText(newTextItalics)))
                                                         .FirstOrDefault();
                                    zendeskHtmlComponents.Remove(zHtmlComponent);
                                    zHtmlComponent.text = "<i>" + ClearHtmlText(zHtmlComponent.text) + "</i>";
                                    zendeskHtmlComponents.Add(zHtmlComponent);
                                    lastText = zHtmlComponent.text;
                                }

                                this.line = item.Line;
                                break;
                            case "EM":
                                break;
                            case "BR":
                                AddLineBreak();
                                break;
                            case "IMG":
                                AddImage(item);
                                break;
                            case "LI":
                                HandleHtmlList(item.ChildNodes);
                                break;
                            case "H1":
                                if (item.InnerText.ToLower() == "\n")
                                {
                                    AddLineBreak();
                                    break;
                                }

                                if (!string.IsNullOrEmpty(item.InnerText) && !lastText.Equals(item.InnerText) &&
                                    !PreviousTextIsSameLinkText(lastText, ClearHtmlText(item.InnerText)))
                                {
                                    AddHeader(item.InnerText, "h1");
                                }

                                break;
                            case "H2":
                                if (item.InnerText.ToLower() == "\n")
                                {
                                    AddLineBreak();
                                    break;
                                }
                                if (!string.IsNullOrEmpty(item.InnerText) && !lastText.Equals(item.InnerText) &&
                                    !PreviousTextIsSameLinkText(lastText, ClearHtmlText(item.InnerText)))
                                {
                                    AddHeader(item.InnerText, "h2");
                                }

                                break;
                            case "H3":
                                if (item.InnerText.ToLower() == "\n")
                                {
                                    AddLineBreak();
                                    break;
                                }

                                if (!string.IsNullOrEmpty(item.InnerText) && !lastText.Equals(item.InnerText) &&
                                    !PreviousTextIsSameLinkText(lastText, ClearHtmlText(item.InnerText)))
                                {
                                    AddHeader(item.InnerText, "h3");
                                }

                                break;
                            case "H4":
                                if (item.InnerText.ToLower() == "\n")
                                {
                                    AddLineBreak();
                                    break;
                                }

                                if (!string.IsNullOrEmpty(item.InnerText) && !lastText.Equals(item.InnerText) &&
                                    !PreviousTextIsSameLinkText(lastText, ClearHtmlText(item.InnerText)))
                                {
                                    AddHeader(item.InnerText, "h4");
                                }

                                break;
                        }

                        break;
                }
            }

            return zendeskHtmlComponents;
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    public List<ZendeskHtmlComponent> ParseHtmlRequest(string html)
    {
        try
        {
            Init(html);
            foreach (HtmlNode item in htmlDoc.DocumentNode.Descendants())
            {
                switch (item.NodeType.ToString().ToUpper())
                {
                    case "TEXT":
                        if (item.InnerText.ToLower() == "\n")
                        {
                            AddLineBreak();
                        }

                        if (((item.ParentNode != null && !item.ParentNode.Name.ToUpper().Equals("LI")) ||
                             (item.ParentNode.ParentNode != null &&
                              !item.ParentNode.ParentNode.Name.ToUpper().Equals("LI"))))
                        {
                            if (zendeskHtmlComponents != null && !zendeskHtmlComponents.Any())
                            {
                                AddText(ClearHtmlText(item.InnerText));
                            }
                            else
                            {
                                if (zendeskHtmlComponents.LastOrDefault().zendeskHtmlComponentType ==
                                    ZendeskHtmlComponentType.LINK)
                                {
                                    if (!string.IsNullOrEmpty(item.InnerText) &&
                                        zendeskHtmlComponents.LastOrDefault() != null &&
                                        !string.IsNullOrEmpty(zendeskHtmlComponents.LastOrDefault().text) &&
                                        !zendeskHtmlComponents.LastOrDefault().text.Equals(item.InnerText))
                                    {
                                        AddText(ClearHtmlText(item.InnerText));
                                    }
                                }
                                else if (zendeskHtmlComponents.LastOrDefault().zendeskHtmlComponentType !=
                                         ZendeskHtmlComponentType.HEADER)
                                {
                                    AddText(ClearHtmlText(item.InnerText));
                                }
                                else
                                {
                                    AddLineBreak();
                                }
                            }
                        }

                        break;
                    case "ELEMENT":
                        switch (item.Name.ToUpper())
                        {
                            case "DIV":
                                if (item != null && item.Attributes != null &&
                                    item.Attributes.Count > 0 && item.Attributes.Contains("class"))
                                {
                                    var divClass =
                                        item.Attributes.FirstOrDefault(a => a.Name.ToUpper().Equals("CLASS"));
                                    if (divClass.Value.ToUpper().Equals("SIGNATURE"))
                                    {
                                        // AddText(RemoveLineBreaks(item.InnerText));
                                        AddText(item.InnerText);
                                        return zendeskHtmlComponents;
                                    }
                                }

                                break;
                            case "A":
                                foreach (var attribute in item.Attributes)
                                {
                                    if (attribute.Name.ToUpper().Equals("HREF"))
                                    {
                                        string linkText = "";
                                        if (item.ParentNode != null)
                                        {
                                            if (!item.ParentNode.Name.ToUpper().Equals("LI"))
                                            {
                                                if (!PreviousTextIsSameLinkText(lastText,
                                                    ClearHtmlText(item.InnerText)))
                                                {
                                                    linkText += ClearHtmlText(item.InnerText);
                                                    if (!linkHandler.IsInternalLink(attribute.Value))
                                                    {
                                                        linkText += " (extlink) ";
                                                    }

                                                    AddLink(linkText, attribute.Value);
                                                }
                                                else
                                                {
                                                    try
                                                    {
                                                        zendeskHtmlComponents.RemoveAll(a =>
                                                            a.text == ClearHtmlText(item.InnerText));
                                                        linkText += ClearHtmlText(item.InnerText);
                                                        if (!linkHandler.IsInternalLink(attribute.Value))
                                                        {
                                                            linkText += " (extlink) ";
                                                        }

                                                        AddLink(linkText, attribute.Value);
                                                    }
                                                    catch
                                                    {
                                                        break;
                                                    }
                                                }

                                                break;
                                            }
                                        }
                                        else
                                        {
                                            if (!PreviousTextIsSameLinkText(lastText, ClearHtmlText(item.InnerText)))
                                            {
                                                linkText += ClearHtmlText(item.InnerText);
                                                if (!linkHandler.IsInternalLink(attribute.Value))
                                                {
                                                    linkText += " (extlink) ";
                                                }

                                                AddLink(linkText, attribute.Value);
                                            }

                                            break;
                                        }

                                        if ((item.ParentNode != null && !item.ParentNode.Name.ToUpper().Equals("LI")) ||
                                            (item.ParentNode.ParentNode != null &&
                                             !item.ParentNode.ParentNode.Name.ToUpper().Equals("LI")))
                                        {
                                        }
                                    }
                                }

                                break;
                            case "STRONG":
                            case "B":
                                string newTextBold = RemoveFontTags(item.InnerHtml);
                                if (!PreviousTextIsSameLinkText(lastText, ClearHtmlText(newTextBold)))
                                {
                                    if (!item.ParentNode.Name.ToUpper().Equals("LI"))
                                    {
                                        AddText("<b>" + ClearHtmlText(newTextBold) + "</b>");
                                    }
                                }
                                else
                                {
                                    var zHtmlComponent = zendeskHtmlComponents
                                                         .Where(a => !string.IsNullOrEmpty(a.text) &&
                                                                     a.text.Contains(ClearHtmlText(newTextBold)))
                                                         .FirstOrDefault();
                                    zendeskHtmlComponents.Remove(zHtmlComponent);
                                    zHtmlComponent.text = "<b>" + ClearHtmlText(zHtmlComponent.text) + "</b>";
                                    zendeskHtmlComponents.Add(zHtmlComponent);
                                    lastText = zHtmlComponent.text;
                                }

                                break;
                            case "I":
                            case "EM":
                                string newTextItalics = RemoveFontTags(item.InnerHtml);
                                if (!PreviousTextIsSameLinkText(lastText, ClearHtmlText(newTextItalics)))
                                {
                                    if (!item.ParentNode.Name.ToUpper().Equals("LI"))
                                    {
                                        AddText("<i>" + ClearHtmlText(newTextItalics) + "</i>");
                                    }
                                }
                                else
                                {
                                    var zHtmlComponent = zendeskHtmlComponents
                                                         .Where(a => !string.IsNullOrEmpty(a.text) &&
                                                                     a.text.Contains(ClearHtmlText(newTextItalics)))
                                                         .FirstOrDefault();
                                    zendeskHtmlComponents.Remove(zHtmlComponent);
                                    zHtmlComponent.text = "<i>" + ClearHtmlText(zHtmlComponent.text) + "</i>";
                                    zendeskHtmlComponents.Add(zHtmlComponent);
                                    lastText = zHtmlComponent.text;
                                }

                                break;
                            case "BR":
                                AddLineBreak();
                                break;
                            case "LI":
                                HandleHtmlList(item.ChildNodes);
                                break;
                            case "H1":
                                if (!PreviousTextIsSameLinkText(lastText, ClearHtmlText(item.InnerText)))
                                {
                                    if (!item.ParentNode.Name.ToUpper().Equals("LI"))
                                    {
                                        AddHeader("<b>" + ClearHtmlText(item.InnerText) + "</b>", "h1");
                                    }
                                }
                                else
                                {
                                    var zHtmlComponent = zendeskHtmlComponents
                                                         .Where(a => !string.IsNullOrEmpty(a.text) &&
                                                                     a.text.Contains(ClearHtmlText(item.InnerText)))
                                                         .FirstOrDefault();
                                    zendeskHtmlComponents.Remove(zHtmlComponent);
                                    zHtmlComponent.text = "<b>" + ClearHtmlText(zHtmlComponent.text) + "</b>";
                                    zendeskHtmlComponents.Add(zHtmlComponent);
                                    lastText = zHtmlComponent.text;
                                }

                                break;
                            case "H2":
                                if (!PreviousTextIsSameLinkText(lastText, ClearHtmlText(item.InnerText)))
                                {
                                    if (!item.ParentNode.Name.ToUpper().Equals("LI"))
                                    {
                                        AddHeader("<b>" + ClearHtmlText(item.InnerText) + "</b>", "h2");
                                    }
                                }
                                else
                                {
                                    var zHtmlComponent = zendeskHtmlComponents
                                                         .Where(a => !string.IsNullOrEmpty(a.text) &&
                                                                     a.text.Contains(ClearHtmlText(item.InnerText)))
                                                         .FirstOrDefault();
                                    zendeskHtmlComponents.Remove(zHtmlComponent);
                                    zHtmlComponent.text = "<b>" + ClearHtmlText(zHtmlComponent.text) + "</b>";
                                    zendeskHtmlComponents.Add(zHtmlComponent);
                                    lastText = zHtmlComponent.text;
                                }

                                break;
                            case "H3":
                                if (!PreviousTextIsSameLinkText(lastText, ClearHtmlText(item.InnerText)))
                                {
                                    if (!item.ParentNode.Name.ToUpper().Equals("LI"))
                                    {
                                        AddHeader("<b>" + ClearHtmlText(item.InnerText) + "</b>", "h3");
                                    }
                                }
                                else
                                {
                                    var zHtmlComponent = zendeskHtmlComponents
                                                         .Where(a => !string.IsNullOrEmpty(a.text) &&
                                                                     a.text.Contains(ClearHtmlText(item.InnerText)))
                                                         .FirstOrDefault();
                                    zendeskHtmlComponents.Remove(zHtmlComponent);
                                    zHtmlComponent.text = "<b>" + ClearHtmlText(zHtmlComponent.text) + "</b>";
                                    zendeskHtmlComponents.Add(zHtmlComponent);
                                    lastText = zHtmlComponent.text;
                                }

                                break;
                            case "H4":
                                if (!PreviousTextIsSameLinkText(lastText, ClearHtmlText(item.InnerText)))
                                {
                                    if (!item.ParentNode.Name.ToUpper().Equals("LI"))
                                    {
                                        AddHeader("<b>" + ClearHtmlText(item.InnerText) + "</b>", "h4");
                                    }
                                }
                                else
                                {
                                    var zHtmlComponent = zendeskHtmlComponents
                                                         .Where(a => !string.IsNullOrEmpty(a.text) &&
                                                                     a.text.Contains(ClearHtmlText(item.InnerText)))
                                                         .FirstOrDefault();
                                    zendeskHtmlComponents.Remove(zHtmlComponent);
                                    zHtmlComponent.text = "<b>" + ClearHtmlText(zHtmlComponent.text) + "</b>";
                                    zendeskHtmlComponents.Add(zHtmlComponent);
                                    lastText = zHtmlComponent.text;
                                }

                                break;
                            case "H5":
                                if (!PreviousTextIsSameLinkText(lastText, ClearHtmlText(item.InnerText)))
                                {
                                    if (!item.ParentNode.Name.ToUpper().Equals("LI"))
                                    {
                                        AddHeader("<b>" + ClearHtmlText(item.InnerText) + "</b>", "h5");
                                    }
                                }
                                else
                                {
                                    var zHtmlComponent = zendeskHtmlComponents
                                                         .Where(a => !string.IsNullOrEmpty(a.text) &&
                                                                     a.text.Contains(ClearHtmlText(item.InnerText)))
                                                         .FirstOrDefault();
                                    zendeskHtmlComponents.Remove(zHtmlComponent);
                                    zHtmlComponent.text = "<b>" + ClearHtmlText(zHtmlComponent.text) + "</b>";
                                    zendeskHtmlComponents.Add(zHtmlComponent);
                                    lastText = zHtmlComponent.text;
                                }

                                break;
                            default:
                                break;
                        }

                        break;
                    default:
                        break;
                }
            }

            return zendeskHtmlComponents;
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    #endregion

    #region Helpers

    private string RemoveFontTags(string input)
    {
        string output = input;
        if (output.ToUpper().Contains("<B>"))
        {
            int start = output.ToUpper().IndexOf("<B>");
            int end = output.ToUpper().IndexOf("</B>", start);
            output = output.Substring(start + 3, end - start - 3);
        }

        if (output.ToUpper().Contains("<I>"))
        {
            int start = output.ToUpper().IndexOf("<I>");
            int end = output.ToUpper().IndexOf("</I>", start);
            output = output.Substring(start + 3, end - start - 3);
        }

        return output;
    }

    public string ParseHtmlInitial(string html)
    {
        try
        {
            if (!string.IsNullOrEmpty(html))
            {
                //Removing emojis from string
                html = Regex.Replace(html, @"\p{Cs}", "");
                html = Regex.Replace(html, @"<em .*?>", "<i>");
                html = Regex.Replace(html, @"<strong .*?>", "<b>");
                html = Regex.Replace(html, @"<span .*?>", "");
                html = Regex.Replace(html, @"<p .*?>", "");
                html = html.Replace("&amp;", "&");
                html = html.Replace("&nbsp;", " ");
                html = html.Replace("<strong>", "<b>");
                html = html.Replace("</strong>", "</b>");
                html = html.Replace("&lt;", "<");
                html = html.Replace("&gt;", ">");
                html = html.Replace("<em>", "<i>");
                html = html.Replace("</em>", "</i>");
                html = html.Replace("<p>", "");
                html = html.Replace("</p>", "<br>");
                html = html.Replace("<span>", "");
                html = html.Replace("</span>", "");
            }
        }
        catch (Exception e)
        {
            throw e;
        }

        return html;
    }

    public string ClearHtmlText(string text)
    {
        try
        {
            if (!string.IsNullOrEmpty(text))
            {
                //Removing emojis from string
                text = Regex.Replace(text, @"\p{Cs}", "");
                text = text.Replace("&amp;", "&");
                text = text.Replace("&nbsp;", " ");
                text = text.Replace("<strong>", "<b>");
                text = text.Replace("</strong>", "</b>");
                text = text.Replace("<em>", "<i>");
                text = text.Replace("</em>", "</i>");
                text = text.Replace("&lt;", "<");
                text = text.Replace("&gt;", ">");
                text = text.Replace("<span>", "");
                text = text.Replace("</span>", "");
            }
        }
        catch (Exception e)
        {
            throw e;
        }

        return text;
    }

    public string RemoveLineBreaks(string text)
    {
        try
        {
            if (!string.IsNullOrEmpty(text))
            {
                text = text.Replace("\n", "");
            }
        }
        catch (Exception e)
        {
            throw e;
        }

        return text;
    }

    private bool PreviousTextIsSameLinkText(string lastText, string currentText)
    {
        try
        {
            if (!string.IsNullOrEmpty(lastText) && !string.IsNullOrEmpty(currentText))
            {
                bool equal = lastText.EndsWith(currentText + "</a>") ||
                             lastText.EndsWith(currentText + "</b>") ||
                             lastText.EndsWith(currentText + "</i>") ||
                             lastText.EndsWith(currentText + "</b></i>") ||
                             lastText.EndsWith(currentText + "</i></b>") ||
                             lastText.EndsWith(currentText + " (extlink) ") ||
                             lastText.StartsWith("  - " + currentText) ||
                             lastText.StartsWith("      - " + currentText);
                return equal;
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    private bool PreviousTextContains(string lastText, string currentText, string tag)
    {
        try
        {
            if (!string.IsNullOrEmpty(lastText) && !string.IsNullOrEmpty(currentText))
            {
                return lastText.Contains(currentText + "</" + tag + ">");
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    private void AddLineBreak()
    {
        zendeskHtmlComponents.Add(
            new ZendeskHtmlComponent(
                ZendeskHtmlComponentType.LINEBREAK,
                null,
                null
            )
        );
    }

    private void AddText(string text)
    {
        if (!string.IsNullOrEmpty(text.Trim()))
        {
            zendeskHtmlComponents.Add(
                new ZendeskHtmlComponent(
                    ZendeskHtmlComponentType.TEXT,
                    ClearHtmlText(text),
                    null
                )
            );
            lastText = ClearHtmlText(text);
        }
    }

    private void AddHeader(string text, string headerType)
    {
        zendeskHtmlComponents.Add(
            new ZendeskHtmlComponent(
                ZendeskHtmlComponentType.HEADER,
                ClearHtmlText(text),
                null, new Dictionary<ZendeskHtmlAttributeType, string>()
                {
                    {ZendeskHtmlAttributeType.HEADER, headerType}
                }
            )
        );
        lastText = ClearHtmlText(text);
    }

    private void AddLink(string text, string link)
    {
        zendeskHtmlComponents.Add(
            new ZendeskHtmlComponent(
                ZendeskHtmlComponentType.LINK,
                text,
                link
            )
        );
        lastText = ClearHtmlText(text);
    }

    private string GetVideoURL(string rawURL)
    {
        string videoURL = rawURL;
        while (videoURL.StartsWith("/"))
        {
            videoURL = videoURL.Remove(0, 1);
        }

        if (!videoURL.StartsWith("http"))
        {
            videoURL = "https://" + videoURL;
        }

        return videoURL;
    }

    private void AddImage(HtmlNode item)
    {
        string imgLink = "";
        string imgText = "";
        foreach (var attribute in item.Attributes)
        {
            if (attribute.Name.ToUpper().Equals("SRC"))
            {
                imgLink = attribute.Value;
            }

            if (attribute.Name.ToUpper().Equals("ALT"))
            {
                imgText = attribute.Value;
            }
        }

        zendeskHtmlComponents.Add(
            new ZendeskHtmlComponent(ZendeskHtmlComponentType.IMAGE, imgText, imgLink)
        );
    }

    #endregion

    #region Handlers

    private void HandleHtmlList(HtmlNodeCollection htmlList)
    {
        foreach (HtmlNode item in htmlList)
        {
            switch (item.NodeType.ToString().ToUpper())
            {
                case "TEXT":
                    if (item.InnerText.ToLower() == "\n")
                    {
                        AddLineBreak();
                        break;
                    }
                    else 
                    {
                       AddBullet(item);
                    }

                    if (!PreviousTextIsSameLinkText(lastText, ClearHtmlText(item.InnerText)) &&
                        ((item.ParentNode != null && !item.ParentNode.Name.ToUpper().Equals("LI")) ||
                         (item.ParentNode.ParentNode != null &&
                          !item.ParentNode.ParentNode.Name.ToUpper().Equals("LI"))))
                    {
                        AddText(ClearHtmlText(item.InnerText));
                        AddLineBreak();
                    }

                    break;
                case "ELEMENT":
                    AddBullet(item);
                    switch (item.Name.ToUpper())
                    {
                        case "A":
                            foreach (var attribute in item.Attributes)
                            {
                                if (attribute.Name.ToUpper().Equals("HREF"))
                                {
                                    string linkText = "";

                                    if (!PreviousTextIsSameLinkText(lastText, item.InnerText))
                                    {
                                        linkText += ClearHtmlText(item.InnerText);
                                        if (!linkHandler.IsInternalLink(attribute.Value))
                                        {
                                            linkText += " (extlink) ";
                                        }

                                        AddLink(linkText, attribute.Value);
                                        AddLineBreak();
                                    }
                                }
                            }

                            break;
                        case "B":
                            if (!PreviousTextIsSameLinkText(lastText, item.InnerText))
                            {
                                if (item.ParentNode.Name.ToUpper().Equals("LI"))
                                {
                                    AddText("<b>" + ClearHtmlText(item.InnerText) + "</b>");
                                }
                            }
                            else
                            {
                                var zHtmlComponent = zendeskHtmlComponents
                                                     .Where(a => !string.IsNullOrEmpty(a.text) &&
                                                                 a.text.Contains(item.InnerText)).FirstOrDefault();
                                zendeskHtmlComponents.Remove(zHtmlComponent);
                                zHtmlComponent.text = "<b>" + ClearHtmlText(zHtmlComponent.text) + "</b>";
                                zendeskHtmlComponents.Add(zHtmlComponent);
                                lastText = zHtmlComponent.text;
                            }

                            break;
                        case "I":
                            if (!PreviousTextIsSameLinkText(lastText, item.InnerText))
                            {
                                if (!item.ParentNode.Name.ToUpper().Equals("LI"))
                                {
                                    AddText("<i>" + ClearHtmlText(item.InnerText) + "</i>");
                                }
                            }
                            else
                            {
                                var zHtmlComponent = zendeskHtmlComponents
                                                     .Where(a => !string.IsNullOrEmpty(a.text) &&
                                                                 a.text.Contains(item.InnerText)).FirstOrDefault();
                                zendeskHtmlComponents.Remove(zHtmlComponent);
                                zHtmlComponent.text = "<i>" + ClearHtmlText(zHtmlComponent.text) + "</i>";
                                zendeskHtmlComponents.Add(zHtmlComponent);
                                lastText = zHtmlComponent.text;
                            }

                            break;
                        case "EM":
                            if (!PreviousTextIsSameLinkText(lastText, item.InnerText))
                            {
                                if (!item.ParentNode.Name.ToUpper().Equals("LI"))
                                {
                                    AddText("<i>" + ClearHtmlText(item.InnerText) + "</i>");
                                }
                            }
                            else
                            {
                                var zHtmlComponent = zendeskHtmlComponents
                                                     .Where(a => !string.IsNullOrEmpty(a.text) &&
                                                                 a.text.Contains(item.InnerText)).FirstOrDefault();
                                zendeskHtmlComponents.Remove(zHtmlComponent);
                                zHtmlComponent.text = "<i>" + ClearHtmlText(zHtmlComponent.text) + "</i>";
                                zendeskHtmlComponents.Add(zHtmlComponent);
                                lastText = zHtmlComponent.text;
                            }

                            break;
                        case "BR":
                            AddLineBreak();
                            break;
                        case "IMG":
                            AddImage(item);
                            break;
                        case "H1":
                            if (item.InnerText.ToLower() == "\n")
                            {
                                AddLineBreak();
                                break;
                            }

                            if (!PreviousTextIsSameLinkText(lastText, ClearHtmlText(item.InnerText)) &&
                                ((item.ParentNode != null && !item.ParentNode.Name.ToUpper().Equals("LI")) ||
                                 (item.ParentNode.ParentNode != null &&
                                  !item.ParentNode.ParentNode.Name.ToUpper().Equals("LI"))))
                            {
                                AddHeader("  - " + ClearHtmlText(item.InnerText), "H1");
                                AddLineBreak();
                            }

                            break;
                        case "H2":
                            if (item.InnerText.ToLower() == "\n")
                            {
                                AddLineBreak();
                                break;
                            }

                            if (!PreviousTextIsSameLinkText(lastText, ClearHtmlText(item.InnerText)) &&
                                ((item.ParentNode != null && !item.ParentNode.Name.ToUpper().Equals("LI")) ||
                                 (item.ParentNode.ParentNode != null &&
                                  !item.ParentNode.ParentNode.Name.ToUpper().Equals("LI"))))
                            {
                                AddHeader("  - " + ClearHtmlText(item.InnerText), "H2");
                                AddLineBreak();
                            }

                            break;
                        case "H3":
                            if (item.InnerText.ToLower() == "\n")
                            {
                                AddLineBreak();
                                break;
                            }

                            if (!PreviousTextIsSameLinkText(lastText, ClearHtmlText(item.InnerText)) &&
                                ((item.ParentNode != null && !item.ParentNode.Name.ToUpper().Equals("LI")) ||
                                 (item.ParentNode.ParentNode != null &&
                                  !item.ParentNode.ParentNode.Name.ToUpper().Equals("LI"))))
                            {
                                AddHeader("  - " + ClearHtmlText(item.InnerText), "H3");
                                AddLineBreak();
                            }

                            break;
                        case "H4":
                            if (item.InnerText.ToLower() == "\n")
                            {
                                AddLineBreak();
                                break;
                            }

                            if (!PreviousTextIsSameLinkText(lastText, ClearHtmlText(item.InnerText)) &&
                                ((item.ParentNode != null && !item.ParentNode.Name.ToUpper().Equals("LI")) ||
                                 (item.ParentNode.ParentNode != null &&
                                  !item.ParentNode.ParentNode.Name.ToUpper().Equals("LI"))))
                            {
                                AddHeader("  - " + ClearHtmlText(item.InnerText), "H4");
                                AddLineBreak();
                            }

                            break;
                    }

                    break;
            }
        }
    }

    private void AddBullet(HtmlNode item)
    {
        if ((item.LinePosition == 0 || (item.ParentNode != null && item.ParentNode.FirstChild == item)) && !item.Name.ToUpper().Equals("BR") && !item.Name.ToUpper().Equals("UL"))
        {
            if (item.ParentNode != null &&
                item.ParentNode.ParentNode != null &&
                item.ParentNode.ParentNode.ParentNode != null &&
                item.ParentNode.ParentNode.ParentNode.ParentNode != null &&
                item.ParentNode.ParentNode.ParentNode.ParentNode.Name.ToUpper()
                    .Equals("UL"))
            {
                AddText("      - ");
            }
            else
            {
                AddText("  - ");
            }
        }
    }

    public string HandleHtmlComponents(List<ZendeskHtmlComponent> zendeskHtmlComponents)
    {
        try
        {
            string agentResponseToLoad = string.Empty;
            string lastText = string.Empty;
            foreach (ZendeskHtmlComponent zendeskHtmlComponent in zendeskHtmlComponents)
            {
                switch (zendeskHtmlComponent.zendeskHtmlComponentType)
                {
                    case ZendeskHtmlComponentType.HEADER:
                    case ZendeskHtmlComponentType.TEXT:
                        if (!lastText.Equals(zendeskHtmlComponent) &&
                            !PreviousTextIsSameLinkText(lastText, ClearHtmlText(zendeskHtmlComponent.text)))
                        {
                            agentResponseToLoad += zendeskHtmlComponent.text;
                            lastText = zendeskHtmlComponent.text;
                        }

                        break;
                    case ZendeskHtmlComponentType.IMAGE:
                        break;
                    case ZendeskHtmlComponentType.LINK:
                        if (!lastText.Equals(zendeskHtmlComponent))
                        {
                            agentResponseToLoad += "<a href=" + linkHandler.AddLink(zendeskHtmlComponent.link) + ">";
                            agentResponseToLoad += zendeskHtmlComponent.text + "</a>";
                        }

                        lastText = zendeskHtmlComponent.text;
                        break;
                    case ZendeskHtmlComponentType.LINEBREAK:
                        agentResponseToLoad += "\n";
                        break;
                }
            }

            return ClearHtmlText(agentResponseToLoad);
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    #endregion
}