﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Zendesk.Internal.Models.HelpCenter;
using Zendesk.UI.HelpCenter;

namespace Zendesk.UI
{
    public class ZendeskArticleUIContainer : MonoBehaviour
    {
        public GameObject articleDetailsPanel;
        public Text articleTitleText;
        private ZendeskErrorUI zendeskErrorUI;
        public Text articleAuthorText;
        public Text articleVotingText;
        private Article article;
        private ZendeskHelpCenterUI zendeskHelpCenterUI;

        public void Init(Article article, ZendeskErrorUI zendeskErrorUI, ZendeskHelpCenterUI zendeskHelpCenterUi)
        {
            try
            {
                this.gameObject.name = String.Format("Article_{0}", article.Id.ToString());
                this.zendeskErrorUI = zendeskErrorUI;
                this.zendeskHelpCenterUI = zendeskHelpCenterUi;
                this.articleTitleText.text = zendeskHelpCenterUi.ClearHtmlText(article.Title);
                if (this.articleAuthorText != null)
                {
                    this.articleAuthorText.text = zendeskHelpCenterUi.ClearHtmlText(article.Author.Name);
                }
                if(zendeskHelpCenterUI.showArticleVotes && article.VoteSum > 0)
                {
                    if (this.articleVotingText != null)
                    {
                        this.articleVotingText.text = article.VoteSum.ToString();
                    }
                }
                else
                {
                    this.articleDetailsPanel.SetActive(false);
                }
                    
                this.article = article;
                gameObject.SetActive(true);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void button_OpenArticle()
        {
            try
            {
                zendeskHelpCenterUI.LoadIndividualArticle(article);
            }
            catch
            {
                zendeskErrorUI.NavigateError(null,true);
            }
        }

    }
}