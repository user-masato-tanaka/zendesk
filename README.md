# Zendesk SDK
Zendesk SDKをUnity Package Managerで追加するためのプロジェクトです。

追加方法はPackage ManagerからAdd package from git urlを選んで
`https://atlassian.ponos.co.jp/bitbucket/scm/~masato.tanaka/zendesk.git?path=Packages/Zendesk#1.2.4`
を入力するか、`manifest.json`の`dependencies`に`"com.zendesk.support": "https://atlassian.ponos.co.jp/bitbucket/scm/~masato.tanaka/zendesk.git?path=Packages/Zendesk#1.2.4"`を追加します。
