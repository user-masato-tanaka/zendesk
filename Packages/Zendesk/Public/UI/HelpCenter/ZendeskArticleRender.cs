﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using Zendesk.Internal.Models.HelpCenter;
using Zendesk.Internal.Models.Support;
using Zendesk.UI;

public class ZendeskArticleRender : MonoBehaviour
{
	private ZendeskMain zendeskMain;
	public GameObject articleRenderTargetGO;
	public GameObject textPrefabGO;
	public GameObject imagePrefabGO;
	public GameObject headerPrefabGO;
	private GameObject textPrefabInstatiated = null;
	private string articleContent = "";
	private string lastText = "";
	public Texture imagePlaceholder;
	private ZendeskErrorUI zendeskErrorUI;
	private ZendeskLocalizationHandler zendeskLocalizationHandler;
	private ZendeskLinkHandler linkHandler;
	private List<GameObject> articleComponents;
	public void Init(ZendeskMain zendeskMain, ZendeskErrorUI zendeskErrorUI, ZendeskLocalizationHandler zendeskLocalizationHandler)
	{
		this.zendeskMain = zendeskMain;
	 	this.zendeskErrorUI = zendeskErrorUI;
	    this.zendeskLocalizationHandler = zendeskLocalizationHandler;
		imagePrefabGO.GetComponentInChildren<RawImage>().texture = imagePlaceholder;
		linkHandler = zendeskMain.zendeskLinkHander.GetComponent<ZendeskLinkHandler>();
		articleComponents = new List<GameObject>();
	}

	private void Start()
	{
		linkHandler = zendeskMain.zendeskLinkHander.GetComponent<ZendeskLinkHandler>();
	}

	public void RenderArticle(Article article)
	{
		ClearArticleView();
		
		var zendeskHtmlComponents = zendeskMain.zendeskHtmlHandler.ParseHtml(article.Body);
		int orderNumber = 0;
            
		foreach (ZendeskHtmlComponent zendeskHtmlComponent in zendeskHtmlComponents)
		{
			switch (zendeskHtmlComponent.zendeskHtmlComponentType)
			{
				case ZendeskHtmlComponentType.HEADER:
					if (!lastText.Equals(zendeskHtmlComponent) && !PreviousTextIsSameLinkText(lastText, zendeskHtmlComponent.text))
					{
						AddHeaderComponent(zendeskHtmlComponent.text, zendeskHtmlComponent.attributes);
					}
					break;
				case ZendeskHtmlComponentType.TEXT:
					if (!lastText.Equals(zendeskHtmlComponent) && !PreviousTextIsSameLinkText(lastText, zendeskHtmlComponent.text))
					{
						articleContent += zendeskHtmlComponent.text;
						lastText = zendeskHtmlComponent.text;
					}
					break;
				case ZendeskHtmlComponentType.IMAGE:
					AddTextComponent();
					AddImage(zendeskHtmlComponent.link, orderNumber);
					orderNumber++;
					articleContent = string.Empty;
					break;
				case ZendeskHtmlComponentType.LINK:
					if (!lastText.Equals(zendeskHtmlComponent))
					{
						articleContent += "<a href=" + linkHandler.AddLink(zendeskHtmlComponent.link) + ">";
						articleContent += zendeskHtmlComponent.text + "</a>";
					}
					lastText = zendeskHtmlComponent.text;
					break;
				case ZendeskHtmlComponentType.LINEBREAK:
					AddTextComponent();
					articleContent = string.Empty;
					break;
			}
		}

		if (!string.IsNullOrEmpty(articleContent))
		{
			AddTextComponent();
		}
		
		zendeskMain.zendeskUI.FinishLoading();
	}

	private void AddImage(string imageURL, int orderNumber)
	{
		var gameObjectImg = Instantiate(imagePrefabGO, articleRenderTargetGO.transform);
		gameObjectImg.GetComponentInChildren<LayoutElement>().preferredHeight = 512;
		gameObjectImg.GetComponentInChildren<LayoutElement>().preferredWidth = 512;
		gameObjectImg.name += "_" + orderNumber;
		articleComponents.Add(gameObjectImg);
		
		this.zendeskMain.supportProvider.GetImage(textureResponse =>
		{
			try
			{
				if (textureResponse.IsError)
				{
					zendeskErrorUI.NavigateError(
						zendeskLocalizationHandler.translationGameObjects["usdk_fail_fetch_attachment"]);
					return;
				}

				int properWidth = 0;
				int properHeight = 0;
				var texture = textureResponse.Result;
				if (texture != null)
				{
					if (IsImageResolutionAcceptable(texture.height, texture.width))
					{
						ParseImageDimensionsToSDK((int) texture.height, (int) texture.width, out properHeight,
							out properWidth);

						gameObjectImg.GetComponentInChildren<LayoutElement>().preferredHeight = properHeight;
						gameObjectImg.GetComponentInChildren<LayoutElement>().preferredWidth = properWidth;
						gameObjectImg.GetComponentInChildren<RawImage>().texture = texture;
						gameObjectImg.GetComponentInChildren<RawImage>().color = Color.white;
					}
					else
					{
						zendeskErrorUI.NavigateError(zendeskLocalizationHandler.translationGameObjects["usdk_fail_fetch_attachment"]);
					}
				}
			}
			catch
			{
				zendeskErrorUI.NavigateError(zendeskLocalizationHandler.translationGameObjects["usdk_fail_fetch_attachment"]);
			}
			finally
			{
				gameObjectImg = null;
			}
		}, new Attachment(){ ContentUrl = imageURL}, false);
	}

	private void AddTextComponent()
	{
		if (string.IsNullOrEmpty(articleContent))
		{
			return;
		}
		
		if (textPrefabInstatiated == null && !string.IsNullOrEmpty(articleContent))
		{
			textPrefabInstatiated = Instantiate(textPrefabGO, articleRenderTargetGO.transform);
		}
		
		textPrefabInstatiated.GetComponent<ZendeskTextPic>().init(articleContent, this.zendeskMain.zendeskLinkHander);
		articleComponents.Add(textPrefabInstatiated);
		textPrefabInstatiated = null;
		articleContent = string.Empty;
		lastText = string.Empty;
	}

	private void AddHeaderComponent(string headerContent, Dictionary<ZendeskHtmlAttributeType, string> attributes)
	{
		if (string.IsNullOrEmpty(headerContent))
		{
			return;
		}
		
		if (textPrefabInstatiated == null && !string.IsNullOrEmpty(headerContent))
		{
			textPrefabInstatiated = Instantiate(headerPrefabGO, articleRenderTargetGO.transform);
		}

		var headerTextGameObject = textPrefabInstatiated.transform.GetComponentInChildren<ZendeskTextPic>();
		headerTextGameObject.init(headerContent, this.zendeskMain.zendeskLinkHander);
		if (attributes.ContainsKey(ZendeskHtmlAttributeType.HEADER))
		{
			var headerAttribute = attributes[ZendeskHtmlAttributeType.HEADER].ToUpper();
			
			if (headerAttribute.Equals("H1"))
			{
				headerTextGameObject.fontSize = 78;
			}
			else if (headerAttribute.Equals("H2"))
			{
				headerTextGameObject.fontSize = 60;
			}
			else if (headerAttribute.Equals("H3"))
			{
				headerTextGameObject.font = Resources.Load<Font>("ZendeskFonts/Roboto/Roboto-Regular");
				headerTextGameObject.fontSize = 54;
			}
			else if (headerAttribute.Equals("H4"))
			{
				headerTextGameObject.fontSize = 48;
			}
		}
		articleComponents.Add(textPrefabInstatiated);
		textPrefabInstatiated = null;
	}


	private void ClearArticleView()
	{
		try
		{
			lastText = "";
			foreach (var component in articleComponents)
			{
				Destroy(component);
			}
		}
		catch (Exception e)
		{
			throw e;
		}
	}
	
	private void ParseImageDimensionsToSDK(int height, int width, out int heightUnity, out int widthUnity)
	{
		float kMaxResolution = articleRenderTargetGO.GetComponent<RectTransform>().rect.width
		                       - (articleRenderTargetGO.GetComponent<VerticalLayoutGroup>().padding.left + articleRenderTargetGO.GetComponent<VerticalLayoutGroup>().padding.right) ; 
		if (width > kMaxResolution)
		{
			widthUnity = (int)kMaxResolution;
			heightUnity = (int)(kMaxResolution * height) / width;
		}
		else
		{
			widthUnity = width;
			heightUnity = height;
		}
	}

	private bool PreviousTextIsSameLinkText(string lastText, string currentText)
	{
		try
		{
			bool response = false;

			if (lastText.Equals(currentText))
			{
				return true;
			}
			else if (lastText.EndsWith(currentText +  "</a>"))
			{
				return true;
			}

			return response;
		}
		catch (Exception e)
		{
			throw e;
		}
	}
	
	private bool IsImageResolutionAcceptable(int height, int width)
	{
		if (height > 4096 || width > 4096)
		{
			return false;
		}

		return true;
	}
}
