using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Zendesk.Internal.Models.Common;
using Zendesk.UI;

namespace Scenes
{
    public class CustomButtonsSceneScript : MonoBehaviour
    {
        [SerializeField] private GameObject zendesk;
        private GameObject zendeskGO;
        [SerializeField] private RectTransform pauseMenu;
        [SerializeField] private RectTransform wholeUI;
        [SerializeField] private Button pauseButton;
        [SerializeField] private GameObject newCommentCounterGO;
        [SerializeField] private Text newCommentCounterText;

        public void OpenZendeskHelpCenter()
        {
            zendeskGO.GetComponent<ZendeskMain>().zendeskUI.OpenHelpCenter();
        }

        public void OpenZendeskRequests()
        {
            zendeskGO.GetComponent<ZendeskMain>().zendeskUI.OpenSupport();
        }

        public void Pause()
        {
            newCommentCounterGO.SetActive(false);
            // Check if zendesk failed to initialise
            if (zendeskGO == null || zendeskGO.GetComponent<ZendeskMain>().InitialisationStatus ==
                InitialisationStatus.Failed)
            {
                zendesk.GetComponent<ZendeskUI>().useZendeskUIButtons = false;
                zendesk.GetComponent<ZendeskMain>().autoPause = false;
                zendeskGO = Instantiate(zendesk);
            }

            if (zendeskGO != null && zendeskGO.GetComponent<ZendeskMain>().InitialisationStatus ==
                InitialisationStatus.Initialised)
            {
                zendeskGO.GetComponent<ZendeskMain>().supportProvider.GetRequestUpdates(GetRequestUpdatesCallback);
            }

            Time.timeScale = 0;
            SetPauseMenuVisibility(true);
        }

        private void SetPauseMenuVisibility(bool isVisible)
        {
            pauseButton.gameObject.SetActive(!isVisible);
            pauseMenu.gameObject.SetActive(isVisible);
            wholeUI.gameObject.SetActive(!isVisible);
        }

        private void GetRequestUpdatesCallback(ZendeskResponse<ZendeskRequestUpdates> result)
        {
            if (result.IsError)
            {
                newCommentCounterGO.SetActive(false);
                return;
            }

            var requestUpdates = result.Result.requestUpdates;
            if (requestUpdates == null || requestUpdates.Count <= 0)
            {
                newCommentCounterGO.SetActive(false);
                return;
            }

            var newCommentCount = requestUpdates.Where(item => item.hasUpdates).Sum(item => item.newComments);
            newCommentCounterGO.SetActive(true);
            newCommentCounterText.text = newCommentCount.ToString();
            Canvas.ForceUpdateCanvases();
        }

        public void Resume()
        {
            Time.timeScale = 1.0f;
            SetPauseMenuVisibility(false);
        }
    }
}
